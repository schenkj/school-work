-------- PROJECT GENERATOR --------
PROJECT NAME :	Schenk_A18B0311P
PROJECT DIRECTORY :	C:\WorkSpace\Schenk_A18B0311P\Schenk_A18B0311P
CPU SERIES :	2600
CPU TYPE :	Other
TOOLCHAIN NAME :	KPIT GNUH8 [ELF] Toolchain
TOOLCHAIN VERSION :	v11.02
GENERATION FILES :
    C:\WorkSpace\Schenk_A18B0311P\Schenk_A18B0311P\start.asm
        Reset Program
    C:\WorkSpace\Schenk_A18B0311P\Schenk_A18B0311P\inthandler.c
        Interrupt Handler
    C:\WorkSpace\Schenk_A18B0311P\Schenk_A18B0311P\vects.c
        Vector Table
    C:\WorkSpace\Schenk_A18B0311P\Schenk_A18B0311P\iodefine.h
        Definition of I/O Register
    C:\WorkSpace\Schenk_A18B0311P\Schenk_A18B0311P\inthandler.h
        Interrupt Handler Declarations
    C:\WorkSpace\Schenk_A18B0311P\Schenk_A18B0311P\hwinit.c
        Hardware Setup file
    C:\WorkSpace\Schenk_A18B0311P\Schenk_A18B0311P\typedefine.h
        Aliases of Integer Type
    C:\WorkSpace\Schenk_A18B0311P\Schenk_A18B0311P\intrinsic.h
        Intrinsic header file
    C:\WorkSpace\Schenk_A18B0311P\Schenk_A18B0311P\Schenk_A18B0311P.c
        Main Program
START ADDRESS OF SECTION :
    0x000000800	.text,.rodata
    0x000FFEC00	.data,.bss
    0x00FFC000	.stack

SELECT TARGET :
    H8S/2600A Simulator
DATE & TIME : 29.06.2019 16:34:43
