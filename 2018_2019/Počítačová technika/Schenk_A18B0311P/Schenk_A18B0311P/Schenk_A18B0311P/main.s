;
; Prevod cisla z desitkove do hex. soustavy
;

		.h8300s

		.section	.vects,"a",@progbits
rs:		.long		_start		

; ----------- symboly ------------- 

		.equ	syscall,0x1FF00	; simulated IO area
		.equ	PUTS,0x0114		; kod PUTS
		.equ	GETS,0x0113		; kod GETS
		
; ----------- data ---------------- 

		.data
		.align	2			; zarovnani par. bloku
par_i:	.long	vstup		; parametricky blok pro vstup
par_i2:	.long	vstup2		; parametricky blok pro vstup
par_o:	.long	text1		; parametricky blok pro vystup
par_o2:	.long	text2		; parametricky blok pro vystup
par_p:	.long	prn			; parametricky blok pro vystup

prn:	.space	20			; vystupni buffer
vstup:	.space	20			; vstupni buffer
vstup2:	.space	20			; vstupni buffer
text1:	.asciz	"Zadej cislo: "
text2:	.asciz	"Zadej soustavu: "

		.align	2			; zarovnani adresy
		.space	100			; stack
stck:						; konec stacku + 1

; ----------- program ------------- 

		.text
		.global	_start

_start:
		mov.l	#stck,ER7
		
; ---   prompt		
		mov.w	#PUTS,R0	; 24bitovy PUTS
		mov.l	#par_o,ER1	; adr. param. bloku do ER1
		jsr		@syscall
		
; ---   cteni cisla		
		mov.w	#GETS,R0	; 24bitovy GETS
		mov.l	#par_i,ER1	; adr. param. bloku do ER1
		jsr		@syscall
		mov.l	ER1,ER3
; ---   prompt2		
		mov.w	#PUTS,R0	; 24bitovy PUTS
		mov.l	#par_o2,ER1	; adr. param. bloku do ER2
		jsr		@syscall
		
; ---   cteni cisla2		
		mov.w	#GETS,R0	; 24bitovy GETS
		mov.l	#par_i2,ER1	; adr. param. bloku do ER2
		jsr		@syscall
		mov.l	ER1,ER2
		mov.l	ER3,ER1
; ---	volani funkce
		mov.l	#vstup,ER6	; adresa cisla (string) do ER6
		jsr		@prevod		; cislo v ER0
		mov.l	#vstup2,ER6	; adresa soustavy (string) do ER6	
		jsr		@prevod2	; soustava v ER3
		jsr		@print		; vystup cisla
		
lab1:	jmp	@lab1			; dynamicky stop

; ---	funkce prevod  ----------
prevod:
		push.l	ER1			; ulozeni registru
		xor.w	R0,R0
		xor.w	R1,R1
		mov.w	#10,E1
		
lab2:	mov.b	@ER6,R1L
		cmp.b	#0x0A,R1L	; test na konec (CR)
		beq		lab3
		add.b	#-'0',R1L
		mulxu.w	E1,ER0
		add.w	R1,R0
		inc.l	#1,ER6
		jmp		@lab2
		
lab3:	pop.l	ER1			; obnoveni registru
		rts					; navrat


; ---	funkce prevod2  ----------
prevod2:
		push.l	ER2			; ulozeni registru
		xor.w	R3,R3
		xor.w	R4,R4
		mov.w	#10,E4
		
lab21:	mov.b	@ER6,R4L
		cmp.b	#0x0A,R4L	; test na konec (CR)
		beq		lab31
		add.b	#-'0',R4L
		mulxu.w	E4,ER3
		add.w	R4,R3
		inc.l	#1,ER6
		jmp		@lab21
		
lab31:	pop.l	ER4			; obnoveni registru
		rts					; navrat
		
; ---	vystup  ----------
print:
		mov.l	#prn,ER1	; adresa vyst. bufferu
		mov.b	#16,R2H		;pocet hex. cislic
		mov.l	ER0,ER4		; ER0 - cislo,  ER3 - soustava
		pop.w	R6
		
lab5:	divxu.w	R3,ER0		;deleni cislo soustavou - vysledek v R3 a zbytek v E3
		cmp.l	#0x00000000,ER0
		beq		lab4
		push.w	E0
		xor.w 	E0,E0
		
		jmp		@lab5
		
lab4:	xor.w	R6,R6
		pop.w	R6
		add.w	#0x30,R6	
		
		cmp.l	#0x10,ER3	; jestli je hexa. - prevadej s A...F
		beq		oprava
		
po_oprave:	cmp.l	#stck,ER7
			beq		lab6
		
		mov.l	#prn,ER1
		mov.b	R6L,@ER1
		mov.l	@prn,ER2
		mov.w	#PUTS,R0	; 24bitovy PUTS
		mov.l	#par_p,ER1	; adr. param. bloku do ER1
		jsr		@syscall
		
		inc.l	#1,ER1
		
		jmp		@lab4		
		
lab6:	jmp @lab6

oprava:	cmp.b	#'9',R6L
		ble		po_oprave
		add.b	#(-'0'+'A'-0x0A),R6L	; oprava pro A..F
		jmp 	@po_oprave

; ---	------- ----------
		 
		.end

; ---	------  ----------
		
