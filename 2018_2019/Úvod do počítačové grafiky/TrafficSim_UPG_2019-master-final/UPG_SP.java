import java.awt.Dimension;

import javax.swing.JFrame;

public class UPG_SP {

	 public static void main(String[] args) {
	        JFrame frame = new JFrame();
	        frame.setTitle("UPG_SP");
	        frame.setSize(800, 600);
	        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	        frame.setLocationRelativeTo(null);
	        frame.setVisible(true); 
	        
	     // Vlastni graficky obsah
	        DrawingPanel drawingPanel = new DrawingPanel();
	        drawingPanel.setPreferredSize(new Dimension(640, 480));
	        frame.add(drawingPanel);
	    }
}
