import java.awt.Button;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.Timer;

import TrafficSim.Simulator;

public class ControlPanel extends JPanel {
	private DrawingPanel p;
	private Timer t;
	//private Simulator sim = new Simulator();
	public Color c;
	public static boolean speedOrFull=false;
	public ControlPanel(DrawingPanel p, Timer t, Simulator  sim) {
		this.p = p;
		this.t = t;
		}
	@Override
	public void paint(Graphics g1) {
		setVisible(false);
		setVisible(true);
		if(UPG_SP.btset==false) {
			Button pause = new Button();
				pause.setLabel("Stop");
				pause.addActionListener(new ActionListener() {@Override
					public void actionPerformed(ActionEvent e) {
					if(t.isRunning()) {t.stop();
						pause.setLabel("Start");}
					else {t.start();
						pause.setLabel("Stop");}
				}});
		
		Button slower = new Button();
		slower.setLabel("<<");
		slower.addActionListener(new ActionListener() {@Override
			public void actionPerformed(ActionEvent e) {UPG_SP.speed=(int)(UPG_SP.speed*1.5)+1;
			//System.out.println(UPG_SP.speed);
			t.setDelay(UPG_SP.speed);
			t.restart();
			if(t.isRunning()) pause.setLabel("Stop");
			}});
		
		Button reset = new Button();
		reset.setLabel("reset speed");
		reset.addActionListener(new ActionListener() {@Override
			public void actionPerformed(ActionEvent e) {UPG_SP.speed=40;
			//System.out.println(UPG_SP.speed);
			t.setDelay(UPG_SP.speed);
			t.restart();
			if(t.isRunning()) pause.setLabel("Stop");
			}});
		
		
		Button faster = new Button();
		faster.setLabel(">>");
		faster.addActionListener(new ActionListener() {@Override
			public void actionPerformed(ActionEvent e) {UPG_SP.speed*=0.66;
			//System.out.println(UPG_SP.speed);
			t.setDelay(UPG_SP.speed);
			t.restart();
			if(t.isRunning()) pause.setLabel("Stop");
			}});
		
		Button full = new Button();
		full.setLabel("full");
		full.addActionListener(new ActionListener() {@Override
			public void actionPerformed(ActionEvent e) {
			speedOrFull=false;
			}});
		
		Button speed = new Button();
		speed.setLabel("speed");
		speed.addActionListener(new ActionListener() {@Override
			public void actionPerformed(ActionEvent e) {
			speedOrFull=true;
			
			}});
		
		p.add(speed);
		p.add(full);
		p.add(slower);
		p.add(reset);
		p.add(pause);
		p.add(faster);
		UPG_SP.btset=true;
	}}
	
}
