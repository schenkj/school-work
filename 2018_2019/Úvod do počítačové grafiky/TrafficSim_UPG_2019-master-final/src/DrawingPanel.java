import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import TrafficSim.Car;
import TrafficSim.CrossRoad;
import TrafficSim.EndPoint;
import TrafficSim.Lane;
import TrafficSim.RoadSegment;
import TrafficSim.Simulator;
/**
 * trida ma na starost vykresleni grafickych prvku v   okne
 * */
public class DrawingPanel extends JPanel{
	public GP gp;
	//metoda nacita simulator pro tuto metodu
	
	private double mouseX;
	private double mouseY;
    private static final long serialVersionUID = 1L;
    private Simulator sim;
    private static double maxx=-Double.MAX_VALUE;
    private static double maxy=-Double.MAX_VALUE;
    private static double minx=Double.MAX_VALUE;
    private static double miny=Double.MAX_VALUE;
    private static double odsazeni=0;
    public static int numberOfCarsCurrent=0;
    public static double averageSpeed=0;
    public static double speed=0;
	public Color c;
	private double scl;
	private int roadNumber=0;
	public List <Line2D> lineRoadReal;
	public double roadWidth;
	public boolean roadPaintedOnce=false;
	public Graphics2D g;
	public double height = getHeight(); 
	//private List <Line2D> lineRoadScaled;
	public Point2D mousePoint= new Point2D.Double(0,0);
	public List <LaneInfo> laneInfoList = new ArrayList<>();
	public LaneInfo laneInfo;
	private long startTime = System.currentTimeMillis();
    @Override
    //metoda vykreslujici veskere graficke prvky do okna
    public void paint(Graphics g1) {
    	//implementace Graphics2D - potrebne k samotnemu kresleni
        super.paint(g1);
        g = (Graphics2D)g1;
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, getWidth(), getHeight());
        //nastaveni velikosti obraazu v zavislosti na velikosti okna a posunuti  jej do leveho spodniho rohu
        double width = getWidth();				
        double height = getHeight(); 
        double widthFrame=maxx-minx;
        double heightFrame = maxy-miny;
        double pomer = widthFrame/heightFrame;
        if(height*pomer<=width) scl=height/(maxy-miny);
        else scl=width/(maxx-minx);
         
        g.translate(0, height); 
        g.scale(scl, -scl);
        //nastaveni informaci o silnicich
        RoadSegment[] roads = sim.getRoadSegments();
        g.translate(-minx+odsazeni,-miny+odsazeni);//(maxy-miny)
        //nastaveni parametru nasledneho kresleni silnic
         g.setStroke(new BasicStroke(Math.round(roads[0].getLaneWidth())));
        g.setColor(Color.GRAY);
        
        for (RoadSegment roadSegment : roads) {
        	Stroke stroke = new BasicStroke(((int)roads[0].getLaneWidth()-1), BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER);
            g.setStroke(stroke);
            //g.setColor(setColorRo());
            //System.out.println(c.toString());
        	drawRoad(roadSegment, g, scl);
        	roadWidth=roads[0].getLaneWidth();
        	
		}
        
        for (CrossRoad cross : sim.getCrossroads()) {
        	Stroke stroke = new BasicStroke(((int)roads[0].getLaneWidth()-1), BasicStroke.CAP_ROUND, BasicStroke.JOIN_MITER);
            g.setStroke(stroke);
            //g.setColor(setColorRo());
        	 drawCrossRoad(cross,g);
        	 //System.out.println("----------------------------------------------------------");
		}
        
        //nčteni aut ze simulatoru
        Car [] auta = sim.getCars();
        //uprava parametru pro vykreslovani
        
        Stroke stroke = new BasicStroke(((int)roads[0].getLaneWidth()-1), BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER);
        g.setStroke(stroke);
        //cyklus ktery vykresly jednotliva vozidla
        for(int k=0;k<auta.length;k++) {
        	//ziskej souradnice predniho narazniku
        	double startX=auta[k].getPosition().getX();
        	double startY = auta[k].getPosition().getY();
        	double endX;
        	double endY;
        	
        	//ziskej souradnice zadniho narazniku
        	endX=auta[k].getPosition().getX()+auta[k].getLength()*Math.cos(auta[k].getOrientation());
        	endY=auta[k].getPosition().getY()+auta[k].getLength()*Math.sin(auta[k].getOrientation());
        	//vykresli automobil
        	speed=auta[k].getCurrentSpeed();
        	g.setColor(setColorC());
        	Line2D car = new Line2D.Double(startX, startY, endX, endY);
        	Point2D carStart = new Point2D.Double(startX, startY);
        	Point2D carEnd = new Point2D.Double(endX, endY);
        	if(carStart.distance(carEnd)<5.5) {
        		stroke = new BasicStroke(((int)roads[0].getLaneWidth()-1), BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER);
        		g.setStroke(stroke);}
        	else if(carStart.distance(carEnd)<7.5) {
        		stroke = new BasicStroke(((int)roads[0].getLaneWidth()), BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER);
        		g.setStroke(stroke);}
        	else{
        		stroke = new BasicStroke(((int)roads[0].getLaneWidth()+1), BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER);
        		g.setStroke(stroke);}
        	g.draw(car);
        }
    roadPaintedOnce=true;
    update();
    }
    
	private void update() {
		for(int k=0;k<laneInfoList.size();k++) {
			laneInfoList.get(k).update(System.currentTimeMillis()-startTime);
		}
	}
	/**
     * metoda vykreslujici krizovatku
     * */
    public void drawCrossRoad(CrossRoad cross, Graphics2D g) {
    	//ziskani pruhu silnic ve krizovatce a bodu kde se maji vykreslit
    	Lane[] lanes =cross.getLanes();
    	EndPoint[] endpointy = cross.getEndPoints();
    	RoadSegment[] getroads =cross.getRoads();
    	Point2D [] pointy = new Point2D[getroads.length];
    	//pro kazdu silnici v krizovatce si nacti jeji dulezite body
    	for (int i=0;i<getroads.length;i++) {
    		if(getroads[i]==null) continue;
			pointy [i] = getroads[i].getEndPointPosition(endpointy[i]);
		}
    	Point2D start=null;
    	Point2D end=null;
    	Point2D inverzeStart = null;
    	Point2D inverzeEnd=null;
    	
    	for (Lane lane : lanes) {
			RoadSegment ls=lane.getStartRoad();
			RoadSegment le=lane.getEndRoad();
			for (Point2D point2d : pointy) {
				if(ls.getStartPosition().equals(point2d)){
					start = point2d;
					inverzeStart = ls.getEndPosition();
					break;
				}	
				else if(ls.getEndPosition().equals(point2d)) {	
					start = point2d;
					inverzeStart = ls.getStartPosition();
					break;
				}
			}
			for (Point2D point2d : pointy) {
				if(le.getStartPosition().equals(point2d)){
					end = point2d;
					inverzeEnd = le.getEndPosition();
					
					break;
				}	
				else if(le.getEndPosition().equals(point2d)) {	
					end = point2d;
					inverzeEnd = le.getStartPosition();
					break;
				}
			}
			numberOfCarsCurrent=lane.getNumberOfCarsCurrent();
			averageSpeed=lane.getSpeedAverage();
			
			//ziskane vektory
			double vektorx =start.getX() - inverzeStart.getX() ;
	    	double vektory = start.getY() - inverzeStart.getY();
	    	//prevedene na jednotkovou delku
	    	double jvektorx = vektorx/Math.sqrt(vektorx*vektorx+vektory*vektory);
	    	double jvektory = vektory/Math.sqrt(vektorx*vektorx+vektory*vektory);
	    	//ziskani normalovych vektru
	    	double jnvektorx = -jvektory;
	    	double jnvektory = jvektorx;
	    	
	    	double sx = start.getX();
	         double sy =start.getY();
	         double ex = end.getX();
	         double ey =end.getY(); 
	         int i = lane.getStartLaneNumber();
	         int j = lane.getEndLaneNumber();
	         
	        	 //separator pouze pro viceproude silnice
	        	 double separator=0;
	        	 if(i<0) {separator = ls.getLaneSeparatorWidth();}
	        	 else {
	        		 i--;
	        		 separator = 0;
	        	 }
	        	 
	        	 //vypocet vychozich souranic vozovky
	        	 double vychozix =sx+(i*ls.getLaneWidth()-separator)*jnvektorx;
	        	 double vychoziy =sy+(i*ls.getLaneWidth()-separator)*jnvektory;
	        	 if(i>=0) {
	        		 vychozix =sx+(-i*ls.getLaneWidth()-separator)*jnvektorx;
		        	 vychoziy =sy+(-i*ls.getLaneWidth()-separator)*jnvektory;
	        		 }
	        	 
	        	 separator=0;
	        	//ziskane vektory
	 			 vektorx = end.getX() - inverzeEnd.getX();
	 	    	 vektory = end.getY() - inverzeEnd.getY();
	 	    	//pprevedene na jednotkovou delku
	 	    	 jvektorx = vektorx/Math.sqrt(vektorx*vektorx+vektory*vektory);
	 	    	 jvektory = vektory/Math.sqrt(vektorx*vektorx+vektory*vektory);
	 	    	//ziskani normalovych vektoru
	 	    	 jnvektorx = jvektory;
	 	    	 jnvektory = -jvektorx;
	 	    	//opet separator pro viceproude silince
	        	 if(j<0) {separator = le.getLaneSeparatorWidth();}
	        	 else {
	        		 separator = 0;
	        		 j--;
	        	 }
	        	 //vypocet koncovich souradnic vozovky
	        	 
	        	 double koncovyx =ex+(j*le.getLaneWidth()-separator)*jnvektorx;
	        	 double koncovyy =ey+(j*le.getLaneWidth()-separator)*jnvektory;
	        	 if(j>=0) {
	        		 koncovyx =ex+(-j*le.getLaneWidth()-separator)*jnvektorx;
	        		 koncovyy =ey+(-j*le.getLaneWidth()-separator)*jnvektory;
	        		 }
	        	 if(maxx<vychozix)maxx=vychozix;
	        	 else if(maxx<koncovyx)maxx=koncovyx;
	        	 if(maxy<vychoziy)maxy=vychoziy;
	        	 else if(maxy<koncovyy)maxy=koncovyy;
	        	 if(minx>vychozix)minx=vychozix;
	        	 else if(minx>koncovyx)minx=koncovyx;
	        	 if(miny>vychoziy)miny=vychoziy;
	        	 else if(miny>koncovyy)miny=koncovyy;
	        	 
	        	 Line2D line = new Line2D.Double(vychozix, vychoziy, koncovyx, koncovyy);
	        	 
	        	 //vykresleni silnice
	        	 g.setColor(setColorRo());
	        	 
	        	 g.draw(line);
	        	 if(!roadPaintedOnce) {
	        		 roadNumber++;
	        		 //System.out.println("scale: "+scl);
	        		 lineRoadReal.add(line);
	        		 laneInfo = new LaneInfo(laneInfoList.size(), lane);
	        		 laneInfoList.add(laneInfo);
	        		 }
    	}
				 
    }
    public Color setColorRo() {
		if(ControlPanel.speedOrFull) {
			if(averageSpeed<45)c=Color.GREEN;
			else if(averageSpeed>=45)c=Color.YELLOW;
			else if(averageSpeed>70)c=Color.RED;
			else c=Color.MAGENTA;
			//System.out.println(averageSpeed);
		}
		else if(!ControlPanel.speedOrFull){
			if(numberOfCarsCurrent<2)c=Color.LIGHT_GRAY;
			else if(numberOfCarsCurrent==2)c=Color.GRAY;
			else if(numberOfCarsCurrent>2)c=Color.DARK_GRAY;
			else c=Color.MAGENTA;
			//if(numberOfCarsCurrent>0)System.out.println(numberOfCarsCurrent);
		}
		return c;
	}
    public Color setColorC() {
			if(speed<45)c=Color.CYAN;
			else if(speed>=45)c=Color.blue;
			else if(speed>60)c=Color.MAGENTA;
			else c=Color.WHITE;
		return c;
	}
    /**
     * metoda vykresluje silnice mimo krizovatku*/
    public void drawRoad(RoadSegment road, Graphics2D g1, double scl) {
    	
    	//ziskani vektoru
    	double vektorx = road.getStartPosition().getX() - road.getEndPosition().getX();
    	double vektory = road.getStartPosition().getY() - road.getEndPosition().getY();
    	//prevedeni na jednotkovou delku
    	double jvektorx = vektorx/Math.sqrt(vektorx*vektorx+vektory*vektory);
    	double jvektory = vektory/Math.sqrt(vektorx*vektorx+vektory*vektory);
    	//jejich normaly
    	double jnvektorx = -jvektory;
    	double jnvektory = jvektorx;
    	//nastaeni kresleni
         Graphics2D g = (Graphics2D)g1;
         RoadSegment[] roads = sim.getRoadSegments();
         g.setStroke(new BasicStroke(Math.round(roads[0].getLaneWidth())-1));
         //averageSpeed=road.getLane(0).getSpeedAverage();
         //numberOfCarsCurrent=road.getLane(0).getNumberOfCarsCurrent();
         g.setColor(setColorRo());
         g.setColor(Color.GRAY);
         //ulozeni souradnic do promennych
         double sx = road.getStartPosition().getX();
         double sy =road.getStartPosition().getY();
         double ex = road.getEndPosition().getX();
         double ey =road.getEndPosition().getY(); 
         for(int i=-road.getBackwardLanesCount();i<=road.getForwardLanesCount()-1;i++) {
        	 //separator pouze pro viceproude silnice
        	 //System.out.println("cislo"+(i));
        	 Lane lane;
        	 if (i<0) {
        	 lane =  road.getLane(i);}
        	 else {lane =  road.getLane(i+1);}
        	 numberOfCarsCurrent=lane.getNumberOfCarsCurrent();
        	 averageSpeed = lane.getSpeedAverage();
        	 
        	 double separator=0;
        	 if(i<0) {separator = road.getLaneSeparatorWidth();}
        	 else separator = 0;
        	 //nacteni souradnic silnic
        	 double vychozix =sx+(i*road.getLaneWidth()-separator)*jnvektorx;
        	 double vychoziy =sy+(i*road.getLaneWidth()-separator)*jnvektory;
        	 double koncovyx =ex+(i*road.getLaneWidth()-separator)*jnvektorx;
        	 double koncovyy =ey+(i*road.getLaneWidth()-separator)*jnvektory;
        	 if(maxx<vychozix)maxx=vychozix;
        	 else if(maxx<koncovyx)maxx=koncovyx;
        	 if(maxy<vychoziy)maxy=vychoziy;
        	 else if(maxy<koncovyy)maxy=koncovyy;
        	 if(minx>vychozix)minx=vychozix;
        	 else if(minx>koncovyx)minx=koncovyx;
        	 if(miny>vychoziy)miny=vychoziy;
        	 else if(miny>koncovyy)miny=koncovyy;
        	 //vykresleni silnice
        	 g.setColor(setColorRo());
        	 Line2D line = new Line2D.Double(vychozix, vychoziy, koncovyx, koncovyy);      
        	 
        	 g.draw(line);
        	 odsazeni=road.getLaneWidth(); 
        	 if(!roadPaintedOnce) {
        		 roadNumber++;
        		 //System.out.println("scale: "+scl);
        		 lineRoadReal.add(new Line2D.Double(vychozix, vychoziy, koncovyx, koncovyy));
        		 laneInfo = new LaneInfo(laneInfoList.size(), lane);
        		 laneInfoList.add(laneInfo);
        		 }
        	 //laneInfo.add(numberOfCarsCurrent, averageSpeed);
         }
    }
    public DrawingPanel(Simulator sim) {this.sim= sim;
    lineRoadReal = new ArrayList<Line2D>();
	addMouseListener(new MouseListener() {
		@Override public void mouseReleased(MouseEvent e) {}
		@Override public void mousePressed(MouseEvent e) {
			mouseX=e.getX();
			mouseY=e.getY();
			mousePoint = new Point2D.Double(minx+mouseX/scl-odsazeni, miny+(getHeight()-mouseY)/scl-odsazeni);
			for(int i=0;i<lineRoadReal.size();i++) {				
				if(lineRoadReal.get(i).ptSegDist(mousePoint)<=roadWidth/2) {
					//System.out.println(lineRoadReal.get(i).toString());
					if(!UPG_SP.graphPainted) {
					gp = new GP(sim, laneInfoList.get(i));
					System.out.println(UPG_SP.graphPainted);
					gp.setVisible(true);
					e.consume();}else e.consume();
				}
				}
			}
		@Override	public void mouseExited(MouseEvent e) {}
		@Override	public void mouseEntered(MouseEvent e) {}		
		@Override	public void mouseClicked(MouseEvent e) {}
	});}
  }

