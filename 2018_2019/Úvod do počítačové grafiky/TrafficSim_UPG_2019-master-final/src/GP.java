import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import TrafficSim.Simulator;

public class GP extends JFrame {
	public LaneInfo laneInfo;
	public GP(Simulator  sim, LaneInfo laneInfo1) {
		this.laneInfo=laneInfo1;
		setVisible(false);
		setVisible(true);
 		setSize(800,600);
 		setTitle("Graph frame");
 		//System.out.println("graph painting");
			}
	public void paint(Graphics g1) {
    	if(!UPG_SP.graphPainted) {
    		super.paint(g1);
    		UPG_SP.graphPainted=true;
    		ChartPanel drawingArea1;
            ChartPanel drawingArea2;
            JFreeChart chart1;
            JFreeChart chart2;
            int WIDTH = getWidth();
            int HEIGHT = getHeight();
            chart1 = makeLineChart(true);
            drawingArea1 = new ChartPanel(chart1);
            drawingArea1.setPreferredSize(new Dimension(WIDTH, (int)(HEIGHT/2.5)));
            chart2 = makeLineChart(false);
            drawingArea2 = new ChartPanel(chart2);
            drawingArea2.setPreferredSize(new Dimension(WIDTH, (int)(HEIGHT/2.5)));
            addWindowListener(new WindowEventHandler());
            setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
            setLayout(new GridLayout(2,1));
            add(drawingArea1);
            add(drawingArea2);
            setSize(WIDTH,HEIGHT);
            setVisible(true);
            }
        }
	class WindowEventHandler extends WindowAdapter {
		  public void windowClosing(WindowEvent evt) {
			  UPG_SP.graphPainted=false;
		    //System.out.println("Call your method here"+UPG_SP.graphPainted);
		  }
		}
    public JFreeChart makeLineChart(boolean kind) {
        XYSeriesCollection dataSet = new XYSeriesCollection();
        String header="";
        if(kind) {dataSet.addSeries(laneInfo.numberOfCars);header="Number of cars current";}
        else {dataSet.addSeries(laneInfo.numberofCarsTotal);header="Number of cars sum";}
        JFreeChart chart = ChartFactory.createXYLineChart(header, "time [ms]", "number of cars", dataSet);
        return chart;
    
    }
			}