import org.jfree.data.xy.XYSeries;

import TrafficSim.Lane;

public class LaneInfo {
	public XYSeries numberOfCars;
	public XYSeries numberofCarsTotal;
	public int number;
	public Lane lane;
	public int i=0;
	public LaneInfo(int number, Lane lane) {this.number = number;this.lane=lane;
	numberOfCars= new XYSeries("numberOfCars");
	numberofCarsTotal= new XYSeries("numberOfCarsTotal");
	}	
	public void update(long l) {
		numberOfCars.add(l, lane.getNumberOfCarsCurrent());
		numberofCarsTotal.add(l, lane.getNumberOfCarsTotal());
	}
}
