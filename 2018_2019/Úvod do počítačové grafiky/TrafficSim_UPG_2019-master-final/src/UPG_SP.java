import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.Timer; 
import TrafficSim.Simulator;
/** Trida UPG_SP je vstupnim bodem programu
*/

public class UPG_SP extends JFrame{
	/**	metoda main jako parametr přijme číslo scénáře, následne iniciuje simulátor a načte daný scénář
	 * nakonec zavolá metodu,která má na starosti samotné okno */
		public static int speed = 40;
		public static boolean btset=false;
		//public static boolean graphBT = false;
		public static int cisloSC;
		public static boolean graphPainted=false;
		public static void main(String[] args) {
			
			if(args.length<0)return;	
			//cisloSC je číslo načtené jako argument přetypovaný na int použitý jako parametr pro načtení scénáře
			
			if(args.length==0)cisloSC=0;
			else cisloSC = Integer.parseInt(args[0]);
			//inicializace simulátoru
			Simulator sim = new Simulator();
			//načtení požadovaného scénáře
			//System.out.println(cisloSC);
			
	        sim.runScenario(sim.getScenarios()[cisloSC]);
	        //volání metody, který má na starosti samotné grafické okno
	        
		 	new UPG_SP(sim).setVisible(true);
		 	
	 	}
		/**metoda nastavující parametry okna, volá třídu která do okna zanese grafické prvky
		 * implementuje časovač 
		 * */
	 	public UPG_SP(Simulator sim) {
	 		setVisible(false);
	 		setVisible(true);
	 		/*GP grPan = new GP(sim);
		 	grPan.setVisible(false);*/
	 		//možnost ukončení programu zavřením okna
	 		setDefaultCloseOperation(EXIT_ON_CLOSE);
	 		//velikost okna
	 		setSize(800, 600);
	 		//časovač, který neustále překresluje okno
	 		Timer t = new Timer(speed, e -> {repaint();
	 		sim.nextStep(0.2);
	 		});
	 		t.start();
	 		//inicializace simulatoru
	 		//spuštění scénáře simulátorem
	        //zavolání  metody která do okna kreslí
	        //jako parametr  přijme silmulátor
	 		getContentPane().setLayout(new BorderLayout());
	 			DrawingPanel p = new DrawingPanel(sim);
	 			ControlPanel sp = new ControlPanel(p,t,sim);
	 			//GraphPanel gp = new GraphPanel(sim);
	 			getContentPane().add(p, BorderLayout.CENTER);
	 			getContentPane().add(sp, BorderLayout.NORTH);}	 	 
	 	}