package uur;

import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyCode;
import javafx.util.converter.DefaultStringConverter;


public class ConsumingTextFieldTableCell<S> extends TextFieldTableCell<S, String> {

	public ConsumingTextFieldTableCell() {
		super(new DefaultStringConverter());
		setOnKeyReleased(e -> {
			if (e.getCode() == KeyCode.DELETE || e.getCode() == KeyCode.N) {
				e.consume();
			}
		});
	}
	
}