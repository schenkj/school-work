package uur;

import java.text.NumberFormat;
import java.util.Locale;

import javafx.scene.control.Alert;
import javafx.scene.control.TableCell;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;


public class NumberTableCell<S> extends TableCell<S, Double> {
	private TextField editor;

	
	@Override
	public void startEdit() {
		super.startEdit();
		if (editor == null) {
			editor = new TextField(getItem().toString().replaceAll("\\.", ","));
			editor.setEditable(true);
			editor.setOnAction(e -> {
				double number;
				if (editor.getText().equals("")) {
					commitEdit(0.0); 
					getTableColumn().setVisible(false);
					getTableColumn().setVisible(true);
				}
				else if (editor.getText().matches("\\d*,{0,1}\\d*") && (number = Double.parseDouble(editor.getText().replaceAll(",", "."))) > 0) {
					commitEdit(number); 
					getTableColumn().setVisible(false);
					getTableColumn().setVisible(true);
				}
				else {
					Alert a = new Alert(AlertType.ERROR);
					a.setHeaderText("Invalid input!");
					a.setContentText("Please enter a number greater than or equal 0 and follow czech rules for number format.");
					a.showAndWait();
					cancelEdit();
				}	
			});
			editor.setOnKeyReleased(e -> {
				if (e.getCode() == KeyCode.DELETE || e.getCode() == KeyCode.N) {
					e.consume();
				}
			});
		}
		setText(null);
		setGraphic(editor);
		editor.selectAll();
		editor.requestFocus();
	}
	
	@Override
	public void cancelEdit() {
		NumberFormat nf = NumberFormat.getNumberInstance();
		setGraphic(null);
		setText(nf.format(getItem()));
		editor.setText(getItem().toString().replaceAll("\\.", ","));
		super.cancelEdit();
	}
	
	@Override
	public void updateItem(Double item, boolean empty) {
		super.updateItem(item, empty);
		if (empty) {
			setGraphic(null);
			setText("");
		} else {
			if (item != null) {
				if (isEditing()) {
					setText(null);
					setGraphic(editor);
					editor.setText(item.toString().replaceAll("\\.", ","));
				} else {
					NumberFormat nf = NumberFormat.getNumberInstance();
					setGraphic(null);
					setText(nf.format(item));
				}
			}
		}
	}
	
}
