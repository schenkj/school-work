package uur;

import java.text.NumberFormat;
import java.time.LocalDate;

import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.input.KeyCode;
import javafx.util.StringConverter;

public class OutputTableCell<S> extends TextFieldTableCell<S, Person> {

	public OutputTableCell() {
		super(new StringConverter<Person>() {

			@Override
			public String toString(Person object) {
				int lines = object.getLine();
				return getEff(object.getHours(), lines);
			}

			@Override
			public Person fromString(String string) {
				return null; //nevyu��v� se
			}
		});
		setOnKeyReleased(e -> {
			if (e.getCode() == KeyCode.DELETE) {
				e.consume();
				System.out.println("Consumed DELETE");
			}
		});
		
	}
	
	private static String getEff(double hours, int lines) {
		if(lines==0||hours==0)return null;
		NumberFormat nf = NumberFormat.getNumberInstance();
		int h = (int)(hours/lines);
		int m=(int)(hours/lines*60-60*h);
		int s=(int)(hours/lines*3600-3600*h-60*m);
		String eff=h+":"+m+":"+s;
		//return nf.format(eff);
		return eff;
	}

	@Override
	public void updateItem(Person item, boolean empty) {
		super.updateItem(item, empty);
		if (!empty) {
			if (item != null) {
				setText(getEff(item.getHours(), item.getLine()));
			}
		}
	}
}

