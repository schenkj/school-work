package uur;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Locale;

import javafx.scene.control.DatePicker;
import javafx.scene.control.TableCell;
import javafx.scene.input.KeyCode;
import javafx.scene.text.Font;

public class DatePickerCell extends TableCell<Person, LocalDate> {
	private DatePicker dateDP;

	public void startEdit() {
		super.startEdit();		
		if (dateDP == null) {
			createDatePicker();
		}
		setText(null);		
		setGraphic(dateDP);
		dateDP.show();
	}
	
	public void cancelEdit() {
		super.cancelEdit();
		
		dateDP.setValue(getItem());
		
		setText(getItem().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)));

		setGraphic(null);
	}

	private void createDatePicker() {
		dateDP = new DatePicker(getItem());
		dateDP.setOnKeyReleased(event -> {
			if (event.getCode() == KeyCode.DELETE) {
				event.consume();
			}
		});
		
		dateDP.setOnAction(event -> {			
			if (dateDP.getValue() == null) {
				event.consume();
				cancelEdit();
			} else if (dateDP.getValue().isAfter(LocalDate.now())) {
				event.consume();
				cancelEdit();
			} else {
				commitEdit(dateDP.getValue());
			}
		});
	}
	
	public void updateItem(LocalDate item, boolean empty) {
		
		super.updateItem(item, empty);
		

		setFont(new Font("Arial", 15));
		
		if (empty) {
			setText(null);
			setGraphic(null);
		} else {
			if (isEditing()) {
				if (dateDP != null) {
					dateDP.setValue(item);
				}
				setText(null);
				setGraphic(dateDP);
			} else {
				setText(item.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)));
				setGraphic(null);
			}
		}
	}
}