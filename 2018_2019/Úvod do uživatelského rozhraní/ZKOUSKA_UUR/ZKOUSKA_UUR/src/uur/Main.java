package uur;

import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.Locale;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.binding.ObjectBinding;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.event.EventType;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

	private TableView<Person> myTable;
	private ObservableList<Person> data;
	private Button insert;
	private double hours1=0,hours2=0,hours3=0,hours4=0,hours5=0,hours6=0,hours7=0;
	private GridPane grid;

	@Override
	public void start(Stage primaryStage) throws Exception {
		Scene mainScene = new Scene(createPane());
		primaryStage.setScene(mainScene);
		primaryStage.setTitle("ZKOUSKA_Schenk");
		primaryStage.show();
	}
	
	private Parent createPane() {
		BorderPane pane = new BorderPane();
		pane.setCenter(centerPane());
		pane.setRight(createGrid());
		return pane;
	}
	
	private Node createButtons() {
		String css = "-fx-alignment: CENTER;";
		HBox h = new HBox(25);
		h.setStyle(css);
		Button del = new Button("Delete");
		del.setOnAction(e -> deleteSelected());
		Button exit = new Button("Exit");
		exit.setOnAction(e -> exit());
		h.getChildren().addAll(del, exit);
		return h;
	}
	
	private void exit() {
		Platform.exit();
	}

	private void deleteSelected() {
		ObservableList<Person> sel = FXCollections.observableArrayList(myTable.getSelectionModel().getSelectedItems());
		
		if (sel.size() != 0) {
			Alert a = new Alert(AlertType.CONFIRMATION);
			a.setHeaderText("Delete ALL selected?");
			ListView<Person> list = new ListView<Person>();
			list.setItems(sel);
			a.setGraphic(list);
			a.showAndWait();
			if (a.getResult() == ButtonType.OK) {
				myTable.getItems().removeAll(sel);
				myTable.getSelectionModel().clearSelection();
			}
		}
		else {
			Alert a = new Alert(AlertType.INFORMATION);
			a.setHeaderText("Nothing is selected");
			a.showAndWait();
		}
		
	}

	private Node createGrid() {
		grid = new GridPane();
		grid.setPadding(new Insets(20));
		grid.setVgap(10);
		grid.setHgap(10);
		grid.setAlignment(Pos.CENTER);
		grid.add(new Label("Coder:"), 2, 0);
		grid.add(new Label("Project:"), 2, 2);
		grid.add(new Label("Date:"), 2, 4);
		grid.add(new Label("Lines:"), 2, 6);
		grid.add(new Label("Hours:"), 2, 8);
		
		TextField nameTF = new TextField();
		DatePicker dateDP = new DatePicker();
		TextField lineTF = new TextField();	
		ComboBox<Coder> coder = new ComboBox<Coder>(FXCollections.observableArrayList(Coder.values()));
		coder.getSelectionModel().select(Coder.OTHER);
		TextField hoursTF = new TextField();
		grid.add(coder, 4, 0);
		grid.add(nameTF, 4, 2);
		grid.add(dateDP, 4, 4);
		grid.add(lineTF, 4, 6);
		grid.add(hoursTF, 4, 8);
		
		insert = new Button("Insert");
		insert.setOnAction(e -> {
			double number;
			if (nameTF.getText().isEmpty()) {
				alert("Empty name");
			} else if (dateDP.getValue() == null || dateDP.getValue().isAfter(LocalDate.now())) {
				alert("Invalid date");
			} else if(lineTF.getText().equals("")||lineTF.getText().isEmpty()) {
				alert("Invalid lines value");
			} else if(lineTF.getText().matches("\\d*,{0,1}\\d*") && (Double.parseDouble(lineTF.getText().replaceAll(",", "."))) <= 0){
				alert("Invalid lines value");
			} else if(!lineTF.getText().matches("\\d*,{0,1}\\d*")){
				alert("Invalid lines value");
			}  else if(hoursTF.getText().equals("")||hoursTF.getText().isEmpty()) {	
				myTable.getItems().add(new Person(coder.getSelectionModel().getSelectedItem(), nameTF.getText(), dateDP.getValue(), Integer.parseInt(lineTF.getText()), 0));
			} else if (hoursTF.getText().matches("\\d*,{0,1}\\d*") && (number = Double.parseDouble(hoursTF.getText().replaceAll(",", "."))) > 0.5 && (number = Double.parseDouble(hoursTF.getText().replaceAll(",", "."))) <= 8){
				myTable.getItems().add(new Person(coder.getSelectionModel().getSelectedItem(), nameTF.getText(), dateDP.getValue(), Integer.parseInt(lineTF.getText()), number));
				if(coder.getSelectionModel().getSelectedItem()==Coder.JINDRA)hours1+=number;
				else if(coder.getSelectionModel().getSelectedItem()==Coder.JIRKA)hours2+=number;
				else if(coder.getSelectionModel().getSelectedItem()==Coder.MAREK)hours3+=number;
				else if(coder.getSelectionModel().getSelectedItem()==Coder.OLGA)hours4+=number;
				else if(coder.getSelectionModel().getSelectedItem()==Coder.ONDRA)hours5+=number;
				else if(coder.getSelectionModel().getSelectedItem()==Coder.PAVLINA)hours6+=number;
				else hours7+=number;
				grid.getChildren().remove(12);
				grid.add(listCoders(), 2, 14);
			} else {
				alert("Invalid hours (use ',' instead of '.')");
			}
			
		});
		
		grid.add(insert, 3, 10);
		grid.add(createButtons(), 2, 16);
		grid.add(listCoders(), 2, 14);
		return grid;
	}
	private static String getTimeFormat(double hours) {
		String eff="";
		if(hours==0)return 0+":"+0+":"+0;
		int h = (int)(hours-hours%1);
		int m=(int)(hours%1*60);
		int s=(int)((hours%1)%100*3600);
		while(s>100)s/=10;
		if(s>60)s-=60;
		if(h<10)eff+="0"+h+":";
		else eff+=h+":";
		if(m<10)eff+="0"+m+":";
		else eff+=m+":";
		if(s<10)eff+="0"+s;
		else eff+=s;
		return eff;
	}

	private Node listCoders() {
		VBox list = new VBox();
		Label name1 = new Label(Coder.JINDRA.toString()+"\t"+getTimeFormat(hours1));
		Label name2 = new Label(Coder.JIRKA.toString()+"\t"+getTimeFormat(hours2));
		Label name3 = new Label(Coder.MAREK.toString()+"\t"+getTimeFormat(hours3));
		Label name4 = new Label(Coder.OLGA.toString()+"\t"+getTimeFormat(hours4));
		Label name5 = new Label(Coder.ONDRA.toString()+"\t"+getTimeFormat(hours5));
		Label name6 = new Label(Coder.PAVLINA.toString()+"\t"+getTimeFormat(hours6));
		Label name7 = new Label(Coder.OTHER.toString()+"\t"+getTimeFormat(hours7));
		list.getChildren().addAll(name1,name2, name3, name4, name5, name6, name7);
		return list;
	}

	private void alert(String string) {
		Alert a = new Alert(AlertType.ERROR);
		a.setTitle("Error");
		a.setHeaderText(string);
		a.showAndWait();
	}

	private Parent centerPane() {
		HBox pane = new HBox();
		String css = "-fx-alignment: CENTER;";
		
		data = FXCollections.observableArrayList();
		fill(data);
		myTable = new TableView<Person>();
		myTable.setItems(data);
		myTable.setEditable(true);
		myTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
		myTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		myTable.setPrefSize(800, 600);
		
		TableColumn<Person, String> nameCol = new TableColumn<Person, String>("Project");
		nameCol.setCellValueFactory(new PropertyValueFactory<Person, String>("name"));
		nameCol.setCellFactory(col -> new ConsumingTextFieldTableCell<Person>());
		nameCol.setStyle(css);
		
		TableColumn<Person, LocalDate> dateCol = new TableColumn<Person, LocalDate>("Date");
		dateCol.setCellValueFactory(new PropertyValueFactory<Person, LocalDate>("date"));
		dateCol.setStyle(css);
		dateCol.setCellFactory(f -> new DatePickerCell());
		
		TableColumn<Person, Integer> lineCol = new TableColumn<Person, Integer>("Lines written");
		lineCol.setCellValueFactory(new PropertyValueFactory<Person, Integer>("line"));
		//lineCol.setCellFactory(col -> new ConsumingTextFieldTableCell<Person>());
		lineCol.setStyle(css);
		
		TableColumn<Person, Coder> coderCol = new TableColumn<Person, Coder>("Coder");
		coderCol.setCellValueFactory(new PropertyValueFactory<Person, Coder>("coder"));
		coderCol.setCellFactory(ComboBoxTableCell.forTableColumn(Coder.values()));
		coderCol.setStyle(css);
		
		TableColumn<Person, Double> hoursCol = new TableColumn<Person, Double>("Hours spend");
		hoursCol.setCellValueFactory(new PropertyValueFactory<Person, Double>("hours"));
		hoursCol.setCellFactory(col -> new NumberTableCell<Person>());
		hoursCol.setStyle(css);
		
		TableColumn<Person, Person> outCol = new TableColumn<Person, Person>("Effectivity");
		outCol.setCellValueFactory(value -> new ObjectBinding<Person>() {

			{
				Person person = value.getValue();
				super.bind(person.dateProperty(), person.hoursProperty());
			}
			
			@Override
			protected Person computeValue() {
				Person person = value.getValue();
				return new Person(person.coderProperty().get(), person.nameProperty().get(), person.dateProperty().get(), person.lineProperty().get(), person.hoursProperty().get());
			}
		});
		outCol.setCellFactory(e -> new OutputTableCell<>());
		outCol.setStyle(css);
		outCol.setEditable(false);
		
		myTable.setOnKeyReleased(e -> {
			if (e.getCode() == KeyCode.DELETE) {
				deleteSelected();
			} else if (e.getCode() == KeyCode.N) {
				insert.fire();
			}
		});
		
		myTable.getColumns().addAll(coderCol, nameCol, dateCol, lineCol, hoursCol, outCol);
		pane.getChildren().addAll(myTable);
		return pane;
	}
	private void fill(ObservableList<Person> data) {
		data.add(new Person(Coder.JINDRA, "Menhaton", LocalDate.of(2019, 6, 7), 258, 1.2));
		hours1+=1.2;
		data.add(new Person(Coder.OLGA, "Menhaton", LocalDate.of(2019, 5, 17), 355, 3.0));
		hours4+=3;
		data.add(new Person(Coder.JIRKA, "DefinitelyNotMenhaton", LocalDate.of(2019, 4, 27), 120, 2.5));
		hours2+=2.5;
		data.add(new Person(Coder.MAREK, "Menhaton", LocalDate.of(2018, 12, 16), 110, 1.0));
		hours3+=1;
		data.add(new Person(Coder.JINDRA, "Menhaton", LocalDate.of(2019, 4, 16), 115, 1.25));
		hours1+=1.25;
		data.add(new Person(Coder.ONDRA, "Menhaton", LocalDate.of(2019, 3, 5), 12, 0.5));
		hours5+=0.5;
	}

	public static void main(String[] args) {
		Locale.setDefault(new Locale("cs", "CZ"));
		launch(args);
	}

}
