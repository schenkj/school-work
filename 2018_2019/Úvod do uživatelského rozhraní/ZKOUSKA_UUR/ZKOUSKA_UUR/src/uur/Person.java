package uur;

import java.time.LocalDate;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Person {
	
	private StringProperty name;
	private ObjectProperty<LocalDate> date;
	private IntegerProperty line;
	private ObjectProperty<Coder> coder;
	private DoubleProperty hours;
	
	public Person(Coder coder, String project, LocalDate date, int line, double price) {
		this.name = new SimpleStringProperty(project);
		this.date = new SimpleObjectProperty<LocalDate>(date);
		this.line = new SimpleIntegerProperty(line);
		this.coder = new SimpleObjectProperty<Coder>(coder);
		this.hours = new SimpleDoubleProperty(price);
	}

	public StringProperty nameProperty() {
		return name;
	}
	
	public String getName() {
		return name.getValue();
	}

	public void setName(StringProperty name) {
		this.name = name;
	}

	public ObjectProperty<LocalDate> dateProperty() {
		return date;
	}
	
	public LocalDate getDate() {
		return date.getValue();
	}

	public void setDate(ObjectProperty<LocalDate> date) {
		this.date = date;
	}

	public int getLine() {
		return line.getValue();
	}
	
	public IntegerProperty lineProperty() {
		return line;
	}

	public void setLine(IntegerProperty line) {
		this.line = line;
	}

	public Coder getCoder() {
		return coder.getValue();
	}
	
	public ObjectProperty<Coder> coderProperty() {
		return coder;
	}

	public void setCoder(ObjectProperty<Coder> coder) {
		this.coder = coder;
	}

	public double getHours() {
		return hours.getValue();
	}
	
	public DoubleProperty hoursProperty() {
		return hours;
	}

	public void setHours(DoubleProperty hours) {
		this.hours = hours;
	}
	
	@Override
	public String toString() {
		return name.getValue();
	}
	
}
