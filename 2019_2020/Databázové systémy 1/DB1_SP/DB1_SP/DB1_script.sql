CREATE TABLE kouzla (
    nazev             VARCHAR2(20 CHAR) NOT NULL,
    cena_vyvolani     INTEGER,
    potrebna_uroven   SMALLINT NOT NULL,
    popis             CLOB NOT NULL,
    id_kouzla         SMALLINT NOT NULL
);

ALTER TABLE kouzla ADD CONSTRAINT kouzla_pk PRIMARY KEY ( id_kouzla );

CREATE TABLE muze_pouzit (
    povolani_id_povolani   SMALLINT NOT NULL,
    kouzla_id_kouzla       SMALLINT NOT NULL
);

ALTER TABLE muze_pouzit ADD CONSTRAINT muze_pouzit_pk PRIMARY KEY ( povolani_id_povolani,
                                                                    kouzla_id_kouzla );

CREATE TABLE postava (
    jmeno                  VARCHAR2(60 CHAR) NOT NULL,
    zkusenosti             INTEGER,
    sila                   SMALLINT NOT NULL,
    obratnost              SMALLINT NOT NULL,
    odolnost               SMALLINT NOT NULL,
    inteligence            SMALLINT NOT NULL,
    charisma               SMALLINT NOT NULL,
    rasa_id_rasy           SMALLINT NOT NULL,
    povolani_id_povolani   SMALLINT NOT NULL,
    id_postavy             INTEGER NOT NULL
);

ALTER TABLE postava ADD CONSTRAINT postava_pk PRIMARY KEY ( id_postavy );

CREATE TABLE povolani (
    nazev             VARCHAR2(20 CHAR) NOT NULL,
    popis             CLOB NOT NULL,
    doporucene_rasy   VARCHAR2(60 CHAR),
    id_povolani       SMALLINT NOT NULL
);

ALTER TABLE povolani ADD CONSTRAINT povolani_pk PRIMARY KEY ( id_povolani );

CREATE TABLE rasa (
    nazev                 VARCHAR2(20 CHAR) NOT NULL,
    velikost              CHAR(2 CHAR) NOT NULL,
    specialni_schopnost   VARCHAR2(60),
    id_rasy               SMALLINT NOT NULL
);

ALTER TABLE rasa ADD CONSTRAINT rasa_pk PRIMARY KEY ( id_rasy );

ALTER TABLE muze_pouzit
    ADD CONSTRAINT muze_pouzit_kouzla_fk FOREIGN KEY ( kouzla_id_kouzla )
        REFERENCES kouzla ( id_kouzla );

ALTER TABLE muze_pouzit
    ADD CONSTRAINT muze_pouzit_povolani_fk FOREIGN KEY ( povolani_id_povolani )
        REFERENCES povolani ( id_povolani );

ALTER TABLE postava
    ADD CONSTRAINT postava_povolani_fk FOREIGN KEY ( povolani_id_povolani )
        REFERENCES povolani ( id_povolani );

ALTER TABLE postava
    ADD CONSTRAINT postava_rasa_fk FOREIGN KEY ( rasa_id_rasy )
        REFERENCES rasa ( id_rasy );
        
INSERT INTO rasa VALUES ('Kroll','C','echolokace',0);     
INSERT INTO rasa VALUES ('Barbar','B','-',1);     
INSERT INTO rasa VALUES ('�lov�k','B','-',2);     
INSERT INTO rasa VALUES ('Elf','B','-',3);    
INSERT INTO rasa VALUES ('Trpasl�k','A','infravid�n�',4);     
INSERT INTO rasa VALUES ('Kud�k','A','-',5);     
INSERT INTO rasa VALUES ('Hobit','A','c�t�n�',6);     
    
INSERT INTO povolani VALUES('V�le�n�k','Bojovn�k preferuj�c� boj z bl�zka. Kryje dru�inu a je pro ni velice d�le�it�.','Kroll, Barbar, �lov�k, Trpasl�k',0);
INSERT INTO povolani VALUES('Hrani���','Specialista na boj na d�lku i na bl�zko, siln� spojen� s p��rodou a p��rodn� magi�.','�lov�k, Elf',1);
INSERT INTO povolani VALUES('Zlod�j','Nen�padn� postava vy�k�vaj�c� ve st�nu na p��le�itost. Porad� si se skrytou past�, nebo z�mkem.','Elf, Hobit, Kuduk',2);
INSERT INTO povolani VALUES('Alchymista','Postava schovan� za zbytkem dru�iny, pou��vaj�c� p�edem p�ipraven� lektvery a magick� p�edm�ty.','�lov�k, Kud�k',3);
INSERT INTO povolani VALUES('Kouzeln�k','Velice siln� postava zam��en� na inteligenci, ses�laj�c� kouzla ze zadn�ch �ad sv� dru�iny.','�lov�k, Elf',4);

INSERT INTO kouzla VALUES('Vyle� lehk� zran�n�','3','2','Vyle� 2 + 1k6 zran�n� dotekem',0);
INSERT INTO kouzla VALUES('Vyle� t�k� zran�n�','5','2','Vyle� 3 + 2k6 zran�n� dotekem',1);
INSERT INTO kouzla VALUES('Mluv se zv��aty','2','2','Po jedno kolo m��e� mluvit se zv��aty (Za tuto dobu se stihne� zeptat na jednu ot�zku a dostat na ni odpov��)',2);
INSERT INTO kouzla VALUES('Odemkni z�mek','','1','B�hem 2 kol odemkni zam�en� z�mek; pravd�podobnost: (1k100 + 10*�rove� postavy)%',3);
INSERT INTO kouzla VALUES('Lezen� po st�n�','','1','Vylez po strm�, �i kolm� st�n�; pravd�podobnost: 70% (kad� kolo)',4);
INSERT INTO kouzla VALUES('Zas�hni bleskem','4','3','Zas�hni nep��tele bleskem za 2k6 (ignoruj obrann� ��slo)',5);
INSERT INTO kouzla VALUES('L��iv� lektvar','2','1','P��prava 2 sm�ny; vyl��� 1 + 2k6 �ivot�',6);
INSERT INTO kouzla VALUES('Ohniv� hl�na','1','1','P��prava 1 sm�na; vrhac� vysoce reaktivn� sm�s; vrhem na nep��tele ubere 2k6 �ivot�',7);

INSERT INTO postava VALUES('Honza','266',18,16,10,15,11,2,0,0);
INSERT INTO postava VALUES('Jirka','16',15,16,10,12,10,1,0,1);
INSERT INTO postava VALUES('Marta','775',20,12,11,15,20,3,1,2);
INSERT INTO postava VALUES('Alice','466',17,17,19,11,14,2,1,3);
INSERT INTO postava VALUES('Artas','112',10,6,18,8,12,3,2,4);
INSERT INTO postava VALUES('Artas','155',18,15,13,12,12,1,2,5);
INSERT INTO postava VALUES('Artas','269',19,20,15,7,10,0,4,6);
INSERT INTO postava VALUES('Artas','887',21,12,10,11,10,0,0,7);
INSERT INTO postava VALUES('Artas','156',16,10,15,11,15,4,2,8);
INSERT INTO postava VALUES('Artas','446',11,12,13,15,12,4,0,9);
INSERT INTO postava VALUES('Artas','266',14,17,21,19,8,5,4,10);
INSERT INTO postava VALUES('Artas','345',5,14,6,11,18,5,3,11);
INSERT INTO postava VALUES('Artas','599',16,12,7,15,12,6,3,12);
INSERT INTO postava VALUES('Artas','687',20,21,20,17,16,6,4,13);
INSERT INTO postava VALUES('Artas','0',18,12,11,10,19,3,1,14);

INSERT INTO muze_pouzit VALUES(1,0);
INSERT INTO muze_pouzit VALUES(1,1);
INSERT INTO muze_pouzit VALUES(1,2);
INSERT INTO muze_pouzit VALUES(2,3);
INSERT INTO muze_pouzit VALUES(2,4);
INSERT INTO muze_pouzit VALUES(4,5);
INSERT INTO muze_pouzit VALUES(3,6);
INSERT INTO muze_pouzit VALUES(3,7);
INSERT INTO muze_pouzit VALUES(4,0);
INSERT INTO muze_pouzit VALUES(4,3);
INSERT INTO muze_pouzit VALUES(4,6);

