/*1. select postav s povolanim valecnik*/
SELECT DISTINCT
    schenkj.postava.*,
    schenkj.povolani.id_povolani
FROM
    schenkj.povolani
    INNER JOIN schenkj.postava ON schenkj.povolani.id_povolani = schenkj.postava.povolani_id_povolani
WHERE
    schenkj.povolani.id_povolani = 0
	
	
	
/*2. select kouzel podle jména postavy*/	
SELECT
    schenkj.postava.jmeno,
    schenkj.kouzla.*
FROM
    schenkj.kouzla
    INNER JOIN schenkj.muze_pouzit ON schenkj.muze_pouzit.kouzla_id_kouzla = schenkj.kouzla.id_kouzla
    INNER JOIN schenkj.povolani ON schenkj.povolani.id_povolani = schenkj.muze_pouzit.povolani_id_povolani
    INNER JOIN schenkj.postava ON schenkj.povolani.id_povolani = schenkj.postava.povolani_id_povolani
WHERE
    schenkj.postava.jmeno = 'Marta'
	
	
	
	
/*První scénář testující jestli první select změníme přidáním postavy*/

INSERT INTO postava VALUES('Jakub','156',16,10,15,11,15,4,4,15);

SELECT
    schenkj.postava.jmeno,
    schenkj.kouzla.*
FROM
    schenkj.kouzla
    INNER JOIN schenkj.muze_pouzit ON schenkj.muze_pouzit.kouzla_id_kouzla = schenkj.kouzla.id_kouzla
    INNER JOIN schenkj.povolani ON schenkj.povolani.id_povolani = schenkj.muze_pouzit.povolani_id_povolani
    INNER JOIN schenkj.postava ON schenkj.povolani.id_povolani = schenkj.postava.povolani_id_povolani
WHERE
    schenkj.postava.jmeno = 'Jakub'





/*Druhý scénář testující, jestli se výpis druhého selectu změní po přidání postavy*/

INSERT INTO postava VALUES('Jakub','345',5,14,6,11,18,5,1,17);


SELECT DISTINCT
    schenkj.postava.*,
    schenkj.povolani.id_povolani
FROM
    schenkj.povolani
    INNER JOIN schenkj.postava ON schenkj.povolani.id_povolani = schenkj.postava.povolani_id_povolani
WHERE
    schenkj.povolani.id_povolani = 1