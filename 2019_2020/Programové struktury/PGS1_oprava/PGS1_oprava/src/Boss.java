import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;

public class Boss implements Runnable {
    private TreeSet<WordRecord> myResult = new TreeSet<>(new WRComparer());
    private final Object fileLock = new Object();
    private final Object resLock = new Object();
    private Scanner in;
    public int volumeID = 0;
    private int threadCount;
    boolean finished = false;
    private int progress = 0;
    private String fullFile;
    public int iter = 0;


    Boss(String file) {
        threadCount = FarmerWorkers.underbosses;
        
        //System.out.println("Boss - starts.");
		try {
			fullFile = Files.readString(Paths.get(file));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


    WordRecord getVolume() {
        synchronized (fileLock) {      
        	//System.out.println("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"+progress+"|"+volumeID);
            StringBuilder articleText = new StringBuilder();
            String[] line;
            int lineCounter;
            String myText = fullFile;

            line = myText.split("\\r?\\n");
            
            if (progress >= line.length - 1) { //nen� dal�� pr�ce, u� byl p�e�ten� cel� vstupn� text
            	//System.out.println("BOSS returns NULL");
            	WordRecord wr = new WordRecord();
            	wr.word = null;
            	wr.frequency = volumeID;
            	return wr;
            	}
            
            for(lineCounter = progress; lineCounter<line.length; lineCounter++){
                progress++;
                //if(progress>=line.length-1){finished = true;}
                if (line[lineCounter].trim().startsWith("VOLUME ")) {
                    volumeID++;
                    //if(volumeID==1)continue;
                    String text = articleText.toString().trim();
                    WordRecord wr1 = new WordRecord();
                    wr1.word = text;
                    wr1.frequency = volumeID-1;
                    //System.out.println("\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t\t"+progress+"|"+volumeID);
                    return wr1;
                }

                articleText.append(line[lineCounter]).append("\n");
            }

            WordRecord wr2 = new WordRecord();
            wr2.word = articleText.toString();
            wr2.frequency = volumeID-1;
            return wr2;
        }
    }
    @Override
    public void run() {
    	//System.out.println("boss - run()");
        Thread[] underbosses = new Thread[threadCount];
        for (int i = 0; i < threadCount; i++) {
            underbosses[i] = new Thread(new Underboss(this, "U" + i));
            underbosses[i].start();
        }
        for (Thread underboss : underbosses) {
            try {
            	
                underboss.join();
                //System.out.println("underboss - join()");
            } catch (InterruptedException e) {
                System.err.println("Boss - underbosses did not joined");
            }
        }
        //System.out.println("boss - here");
        printResult();
    }

    // ukladani vysledku z jednoho vtipu do wordRecords
    void reportResult(TreeSet<WordRecord> results) {
        synchronized (resLock) {

            //System.out.println("Boss - Adding results.");
        	Iterator<WordRecord> it = results.iterator();
        	while (it.hasNext()) {
    			WordRecord listItem = it.next();
    			
    			WordRecord wr = Utils.getWordRecord(listItem.word, myResult);
    			
    			if (wr == null)
    				myResult.add(listItem);
    			else {
    				myResult.remove(wr);
    				wr.frequency += listItem.frequency;
    				myResult.add(wr);
    			}
    		}
        }
    }


    // tisk vysledneho seznamu
    private void printResult() {
    	synchronized (resLock) {
	        if (myResult == null) myResult = new TreeSet<>(new WRComparer());
	        //create file
	        File myObj = new File("");
	        try {
	            myObj = new File("lesmiserables\\lesmiserables.txt");
	            myObj.getParentFile().mkdirs();
	            if (myObj.createNewFile()) {
	                System.out.println("File created: " + myObj.getName());
	            } else {
	                //System.out.println("File already exists.");
	            }
	            FileWriter myWriter = new FileWriter(myObj);
	
	            Iterator<WordRecord> it = myResult.iterator();
	
	            while (it.hasNext()) {
	                WordRecord listItem = it.next();
	                myWriter.write(listItem.word + " " + listItem.frequency+"\n");
	            }
	
	            myWriter.close();
	        } catch (IOException e) {
	            System.out.println("An error occurred.");
	            e.printStackTrace();
	        }
    	}
    }
}
