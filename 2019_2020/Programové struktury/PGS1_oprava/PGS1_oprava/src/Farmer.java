import java.io.*;
import java.util.Iterator;
import java.util.TreeSet;

public class Farmer implements Runnable {
    private TreeSet<WordRecord> myResult = new TreeSet<>(new WRComparer());
    private final Object farmerLock = new Object();
    private final Object reportLock = new Object();
    private final Object getLock = new Object();
    private final Object printLock = new Object();
    private final Object assignLock = new Object();
    private int articleID = 0;
    private int threadCount;
    public final String name;
    public final Master master;
    public int chapterNumber = 0;
    public boolean finished = false;
    public int processingVol = 0;
    public int processingBook = 0;

    
    private int progress = 0;
    public boolean susFlag = false;
    
    private WordRecord myChapter = null;
    private String[] chapterLines;


    Farmer(Master master, String name) {
        this.master = master;
        this.name = name;
        this.threadCount = FarmerWorkers.workers;
        
    }
    // prideleni prace workerovi

    private void assignNewChapter(){
    	 synchronized (assignLock) {	    	
	    	//System.out.println(master.underboss.name+""+master.name+""+this.name+" assignNewChapter");
	    	myChapter = master.getChapter();
	    	processingVol = master.underboss.volumeNumber;
            processingBook = master.bookNumber;
            //System.out.println("\t\t\t\t\t\t\t\tnewChapter please!"+ processingVol +"-"+processingBook);
	    	if (myChapter.word != null) {
	    		WordRecord wr = myChapter;
	            String chapter = wr.word;
	            chapterNumber = wr.frequency;       
	            progress=0;
		    	articleID = 0;
	            this.chapterLines = chapter.split("\\r?\\n");	
	            
	    	}else {    		
	    		chapterNumber = myChapter.frequency;
	    		myChapter = null;
	    		chapterLines = null;
	    	}        
    	 }
    }
    
    WordRecord getArticle() {    	
        synchronized (getLock) {
        	if (myChapter == null || myChapter.word== null || progress >= this.chapterLines.length - 1) { //aktu�ln� kapitola ji� nem� dal�� voln� odstavce, z�sk�m nov� odstavec
            	this.assignNewChapter();            	
            	finished = true;
            }        	
        	if (this.myChapter == null) { //nad��zen� ji� nem� ��dnou pr�ci
        		//System.out.println(this.name+" returns NULL");        		
        		printResult();
        		return null;
        	}
        	if(myChapter.word.equals("")) {//p�i�el pr�zdn� z�znam, kter� nem� cenu zpracov�vat
        		this.assignNewChapter();
        	}
        	if (this.myChapter == null) { //nad��zen� ji� nem� ��dnou pr�ci
        		//System.out.println(this.name+" returns NULL");
        		printResult();
        		return null;
        	}
        	if(finished) {
        		finished = false;
        		printResult();
        		myResult = new TreeSet<>(new WRComparer());
        	}                                  
            if(chapterLines == null) {
            	//System.out.println("chapterLines = "+chapterLines);
            	this.assignNewChapter();
            }	
            StringBuilder articleText = new StringBuilder(); 
            for (int lineCounter = progress; lineCounter < chapterLines.length; lineCounter++) {
                progress++;         
                //pokud se najde klicove slovo je text odeslan
                if (chapterLines[lineCounter].trim().equals("")) {                    
                    String text = articleText.toString().trim();
                    if (!text.equals("")) {                    
	                    WordRecord wr1 = new WordRecord();
	                    wr1.word = text;
	                    wr1.frequency = articleID;
	                    articleID++;
	                    return wr1;
	                }
                    else continue; 
                }
                //jinak je text pridan k textu ktery bude odesilan
                articleText.append(chapterLines[lineCounter]).append("\n");
            }
            //odeslani posledni casti
            WordRecord wr2 = new WordRecord();
            wr2.word = articleText.toString();
            wr2.frequency = articleID;
            return wr2;
        }
    }

    @Override
    public void run() {
    	assignNewChapter();
        Thread[] workers = new Thread[threadCount];
        for (int i = 0; i < threadCount; i++) {
            workers[i] = new Thread(new Worker(this, "W" + i));
            workers[i].start();
        }
        for (Thread worker : workers) {
            try {
            	
                worker.join();
                //System.out.println("worker - join()");
            } catch (InterruptedException e) {
                System.err.println("Farmer - workers did not joined");
            }
        }
    }
    
    // ukladani vysledku z jednoho zaznamu do wordRecords
    void reportResult(TreeSet<WordRecord> results) {
        synchronized (this.reportLock) {
        	Iterator<WordRecord> it = results.iterator();
        	while (it.hasNext()) {
    			WordRecord listItem = it.next();    			
    			WordRecord wr = Utils.getWordRecord(listItem.word, myResult);    			
    			if (wr == null)
    				myResult.add(listItem);
    			else {
    				myResult.remove(wr);
    				wr.frequency += listItem.frequency;
    				myResult.add(wr);
    			}
    		}
            
        }
    }


    // tisk vysledneho seznamu
    private void printResult() {
    	synchronized(reportLock) {
	    	if (myResult == null) myResult = new TreeSet<>(new WRComparer()); 
	    	if (myResult != null)master.reportResult(myResult);    	
		    //if (myResult.size() > 1) {
		        try {
		         	File myObj = new File("lesmiserables\\volume" + processingVol + "\\book" + processingBook + "\\chapter" + chapterNumber + "\\chapter" + chapterNumber + ".txt");
		            myObj.getParentFile().mkdirs();
		            if (myObj.createNewFile()) {
		                System.out.println("File created: " + myObj.getName());//+" -> "+processingVol+"|"+processingBook+"|"+chapterNumber);
		            } else {
		                //System.out.println("File already exists.");
		            }
		            FileWriter myWriter = new FileWriter(myObj);	                
		            Iterator<WordRecord> it = myResult.iterator();
			        while(it.hasNext()) {
			         	WordRecord listItem = it.next();
			          	myWriter.write(listItem.word + " " + listItem.frequency + "\n");
			        }
		            myWriter.close();
		            //add results to state
		            addToState();
		        }
		        catch(IOException e1){
		        	System.out.println("An error occurred.");
		          	e1.printStackTrace();
		        }	                
		   
		    // }else System.out.println("\t\t\t\t\t\tFile NOT created: "+master.underboss.volumeNumber+"|"+master.bookNumber+"|"+chapterNumber);
        }
    }

    private void addToState(){
        synchronized (farmerLock) {
            //state of book
            try {
            	File myObj = new File("lesmiserables\\volume" + processingVol + "\\book" + processingBook + "\\state.txt");
                myObj.getParentFile().mkdirs();
                if (myObj.createNewFile()) {
                    System.out.println("File created: " + myObj.getName());
                } else {
                    //System.out.println("File already exists.");
                }
                FileWriter myWriter = new FileWriter(myObj, true);
                myWriter.write("Chapter" + chapterNumber + " - OK\n");
                myWriter.close();
            } catch (IOException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        }
       	synchronized (master.masterLock) {
            //state of volume
            try {
            	File myObj = new File("lesmiserables\\volume" + processingVol + "\\state.txt");
                myObj.getParentFile().mkdirs();
                if (myObj.createNewFile()) {
                    System.out.println("File created: " + myObj.getName());
                } else {
                    //System.out.println("File already exists.");
                }
                FileWriter myWriter = new FileWriter(myObj, true);
                myWriter.write("Book" + processingBook + " - Chapter" + chapterNumber + " - OK\n");
                myWriter.close();
            } catch (IOException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        }
        synchronized (master.underboss.underbossLock) {
            //state of project
            try {
            	File myObj = new File("lesmiserables\\state.txt");
                myObj.getParentFile().mkdirs();
                if (myObj.createNewFile()) {
                    System.out.println("File created: " + myObj.getName());
                } else {
                    //System.out.println("File already exists.");
                }
                FileWriter myWriter = new FileWriter(myObj, true);
                myWriter.write("Volume" + processingVol + " - Book" + processingBook + " - Chapter" + chapterNumber + " - OK\n");
                myWriter.close();
            } catch (IOException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        }
    }
}
