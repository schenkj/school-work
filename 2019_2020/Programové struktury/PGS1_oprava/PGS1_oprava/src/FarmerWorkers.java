import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;


public class FarmerWorkers {
    static int underbosses = 2;
    static int masters = 2;
    static int farmers = 2;
    static int workers = 4;
    static String file = "";

    private final static String INPUT_FILE_NAME = "lesmiserables_-_input.txt";

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Input file: ");
        String input = sc.next();
		try{
            file = input;
        }
		catch (Exception e) {
			e.printStackTrace();
		}
		
        if(file.equals("") || !new File(file).exists()){
            System.out.println("Wrong name of file input. Set on default value - "+INPUT_FILE_NAME);
            file = INPUT_FILE_NAME;
        }
        //get number of threads and check inputs
		System.out.println("Number of underbosses: ");
        input = sc.next();
		try{
            underbosses = Integer.parseInt(input);
        }
		catch(NumberFormatException e){
            System.out.println("Wrong value - underbosses number is set on 2");
            underbosses = 2;
        }
        if(underbosses<1){
            System.out.println("underbosses has wrong value - underbosses number is set on 2");
            underbosses = 2;
        }

        System.out.println("Number of masters (of one underboss): ");
        input = sc.next();
        try{
            masters = Integer.parseInt(input);
        }
        catch(NumberFormatException e){
            System.out.println("Wrong value - masters number is set on 2");
            masters = 2;
        }
        if(masters<1){
            System.out.println("masters has wrong value - masters number is set on 2");
            masters = 2;
        }

        System.out.println("Number of foremen (of one master): ");
        input = sc.next();
        try{
            farmers = Integer.parseInt(input);
        }
        catch(NumberFormatException e){
            System.out.println("Wrong value - foremen number is set on 2");
            farmers = 2;
        }
        if(farmers<1){
            System.out.println("foremen has wrong value - foremen number is set on 2");
            farmers = 2;
        }

        System.out.println("Number of slaves (of one foreman): ");
        input = sc.next();
        try{
            workers = Integer.parseInt(input);
        }
        catch(NumberFormatException e){
            System.out.println("Wrong value - slaves number is set on 2");
            workers = 2;
        }
        if(workers<1){
            System.out.println("slaves has wrong value - slaves number is set on 4");
            workers = 4;
        }
        
        //initiate application
        System.out.println("Process may take a while, with default file even about 1 minute based on device.\nDo not interrupt run of application nor device please.\n\n\n");
        long startTime = System.currentTimeMillis();

       
		Boss boss = new Boss(file);
		Thread thread = new Thread(boss);
		thread.start();
		try {
			thread.join();
		} catch (InterruptedException e) {
			System.err.println("Boss didn't wait on result");
			e.printStackTrace();
		}
		long endTime   = System.currentTimeMillis();
        long totalTime = endTime - startTime;
		System.out.println("Main - Boss finished in "+totalTime+" ms");
	}
}
