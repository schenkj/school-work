import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.TreeSet;

public class Master implements Runnable {
    public final Underboss underboss;
    public final String name;
    private TreeSet<WordRecord> myResult = new TreeSet<>(new WRComparer());
    private final Object getLock = new Object();
    private final Object reportLock = new Object();
	private final Object assignLock = new Object();
	public final Object masterLock = new Object();
    private int chapterID = 0;
    private int threadCount;
    public int bookNumber = 0;
    boolean finished = false;
    public WordRecord myBook;
    private int progress = 0;
    public boolean susFlag = false;
    private String[] bookLines;
    public int processingVol = 0;

    Master(Underboss underboss, String name) {
        this.underboss = underboss;
        this.name = name;
        this.threadCount = FarmerWorkers.farmers;
        //myBook = underboss.getBook();
    }
    
    private void assignNewBook(){
   	 synchronized (assignLock) {
	    	
	    	//System.out.println(master.underboss.name+""+master.name+""+this.name+" assignNewChapter");
	    	myBook = underboss.getBook();
	    	if (myBook.word != null) {
	    		WordRecord wr = myBook;
	            String chapter = wr.word;
	            bookNumber = wr.frequency;       
	            progress=0;
		    	chapterID = 0;
	            this.bookLines = chapter.split("\\r?\\n");
	            processingVol = underboss.volumeNumber;
	    	}else {    		
	    		bookNumber = myBook.frequency;
	    		myBook = null;
	    		bookLines = null;
	    	}        
   	 }
   }

    WordRecord getChapter() {
        synchronized (getLock) {
        	if (myBook == null || myBook.word== null || progress >= this.bookLines.length - 1) { //aktu�ln� kapitola ji� nem� dal�� voln� odstavce, z�sk�m nov� odstavec
            	this.assignNewBook();
            	finished = true;
            }
        	
        	if (this.myBook == null) { //nad��zen� ji� nem� ��dnou pr�ci
        		//System.out.println(this.name+" returns NULL");
        		printResult();
        		WordRecord wr = new WordRecord();
            	wr.word = null;
            	wr.frequency = chapterID;
            	return wr;
        	}
        	if(myBook.word.equals("")) {
        		this.assignNewBook();
        	}
        	if (this.myBook== null) { //nad��zen� ji� nem� ��dnou pr�ci
        		System.out.println(this.name+" returns NULL");
        		printResult();
        		return null;
        	}
        	if(finished) {
        		finished = false;
        		printResult();
        		myResult = new TreeSet<>(new WRComparer());
        	}
        	if(bookLines == null) {
            	//System.out.println("chapterLines = "+bookLines);
            	this.assignNewBook();
            }
            StringBuilder articleText = new StringBuilder();
            for (int lineCounter = progress; lineCounter < bookLines.length; lineCounter++) {
                progress++;
                //pokud se najde klicove slovo je text odeslan
                if (bookLines[lineCounter].trim().startsWith("CHAPTER ")) {
                	String text = articleText.toString().trim();
                    //if (!text.equals("")) {                    
	                    WordRecord wr1 = new WordRecord();
	                    wr1.word = text;
	                    wr1.frequency = chapterID;
	                    //System.out.println(name+": my Book ID: "+bookNumber);
	                    chapterID++;
	                    return wr1;
	                //}
                    //else continue;
                }
                //jinak je text pridan k textu ktery bude odesilan
                articleText.append(bookLines[lineCounter]).append("\n");
            }

            //odeslani posledni casti
            WordRecord wr2 = new WordRecord();
            wr2.word = articleText.toString();
            wr2.frequency = chapterID;
            finished = true;
            return wr2;
        }
    }
    
    @Override
    public void run() {
    	//System.out.println("Master - run()");
    	assignNewBook();
        Thread[] farmers = new Thread[threadCount];
        for (int i = 0; i < threadCount; i++) {
            farmers[i] = new Thread(new Farmer(this, "F" + i));
            farmers[i].start();
        }
        for (Thread farmer : farmers) {
            try {            	
                farmer.join();
                //System.out.println("farmer - join()");
            } catch (InterruptedException e) {
                System.err.println("Master - farmers did not joined");
            }
        }
    }
    
    // ukladani vysledku z jednoho vtipu do wordRecords
    void reportResult(TreeSet<WordRecord> results) {
        synchronized (this.reportLock) {
        	Iterator<WordRecord> it = results.iterator();
        	while (it.hasNext()) {
    			WordRecord listItem = it.next();    			
    			WordRecord wr = Utils.getWordRecord(listItem.word, myResult);    			
    			if (wr == null)
    				myResult.add(listItem);
    			else {
    				myResult.remove(wr);
    				wr.frequency += listItem.frequency;
    				myResult.add(wr);
    			}
    		}
            
        }
    }


    // tisk vysledneho seznamu
    private void printResult() {        
        synchronized(reportLock) {
        	if (myResult == null) {
            myResult = new TreeSet<>(new WRComparer());
        	}
        	underboss.reportResult(myResult);
	        try {
	            //File myObj = new File("lesmiserables\\volume" + underboss.myVolume.frequency + "\\book" + myBook.frequency + "\\book" + myBook.frequency + ".txt");
	        	File myObj = new File("lesmiserables\\volume" + processingVol + "\\book" + bookNumber + "\\book" + bookNumber + ".txt");
	
	            myObj.getParentFile().mkdirs();
	            if (myObj.createNewFile()) {
	                System.out.println("File created: " + myObj.getName());
	            } else {
	                //System.out.println("File already exists.");
	            }
	            FileWriter myWriter = new FileWriter(myObj);
	
	            Iterator<WordRecord> it = myResult.iterator();
	            while(it.hasNext()) {
	            	WordRecord listItem = it.next();
	            	myWriter.write(listItem.word + " " + listItem.frequency + "\n");
	            }

	            myWriter.close();
	
	            //add results to state
	            addToState();
	        } catch (IOException e) {
	            System.out.println("An error occurred.");
	            e.printStackTrace();
	        }
        }
    }

    private void addToState() {
        synchronized (masterLock) {
        	//System.out.println(name+"Master - adding to state");
            //state of volume
            try {
            	File myObj = new File("lesmiserables\\volume" + processingVol + "\\state.txt");
                myObj.getParentFile().mkdirs();
                if (myObj.createNewFile()) {
                    System.out.println("File created: " + myObj.getName());
                } else {
                    //System.out.println("File already exists.");
                }
                FileWriter myWriter = new FileWriter(myObj, true);
                myWriter.write("Book" + bookNumber + " - OK\n");
                myWriter.close();
            } catch (IOException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        }
        synchronized (underboss.underbossLock) {
            //state of project
            try {
            	File myObj = new File("lesmiserables\\state.txt");
                myObj.getParentFile().mkdirs();
                if (myObj.createNewFile()) {
                    System.out.println("File created: " + myObj.getName());
                } else {
                    //System.out.println("File already exists.");
                }
                FileWriter myWriter = new FileWriter(myObj, true);
                myWriter.write("Volume" + processingVol + " - Book" + bookNumber + " - OK\n");
                myWriter.close();
            } catch (IOException e) {
                System.out.println("An error occurred.");
                e.printStackTrace();
            }
        }
    }
}
