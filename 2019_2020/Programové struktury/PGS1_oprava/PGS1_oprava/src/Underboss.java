import java.io.Closeable;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.TreeSet;

public class Underboss implements Runnable{
	public final Boss boss;
	public final String name;
	private TreeSet<WordRecord> myResult = new TreeSet<>(new WRComparer());
	private final Object stringLock = new Object();
	private final Object reportLock = new Object();
	public final Object underbossLock = new Object();
	private final Object printLock = new Object();
	private int bookID = 0;
	private int threadCount;
	public int volumeNumber = 0;
	private int getBookCalled = 0;
	boolean finished = false;
	public WordRecord myVolume;
	private int progress = 0;
	public boolean susFlag = false;
	private String[] volumeLines;

	Underboss(Boss boss, String name) {
		this.boss = boss;
		this.name = name;
		this.threadCount = FarmerWorkers.masters;
		//myVolume = boss.getVolume();
	}

	private void assignNewVolume(){		
		synchronized (stringLock) {
			//if(progress!=0)printResult();
			
			//System.out.println(this.name+" assignNewVolume");
	    	myVolume = boss.getVolume();
	    	//System.out.println("\t\t\t\t\t\t\t\t\t\t\t\tfreq: "+myVolume.frequency);
	    	if (myVolume.word != null) {
	    		WordRecord wr = myVolume;
	            String chapter = wr.word;
	            volumeNumber = wr.frequency; 
	            bookID = 0;
				progress=0;	            
	            this.volumeLines = chapter.split("\\r?\\n");	
	    	}else {    		    		
	    		volumeNumber = myVolume.frequency;
	    		myVolume = null;
	    		volumeLines = null;
	    	} 
		}
    }
	WordRecord getBook() {
		synchronized (stringLock) {
			
			//if(myVolume==null) {System.out.println("null");}
			if (myVolume == null || myVolume.word== null || progress >= this.volumeLines.length - 1) { //aktu�ln� kapitola ji� nem� dal�� voln� odstavce, z�sk�m nov� odstavec
            	this.assignNewVolume();
            	finished = true;
            }
        	
        	if (this.myVolume == null) { //nad��zen� ji� nem� ��dnou pr�ci
        		//System.out.println(this.name+" returns NULL");
        		printResult();
        		WordRecord wr = new WordRecord();
            	wr.word = null;
            	wr.frequency = bookID;
            	return wr;
            }
        	if(myVolume.word.equals("")) {
	    		this.assignNewVolume();
	    		finished = true;
	    	}
        	if(finished) {
        		finished = false;
        		//gi.undeWrite(volumeNumber);
        		myResult = new TreeSet<>(new WRComparer());
        	}
        	//if(this.myVolume.word.equals(""))System.out.println("Empty Volume");
            StringBuilder articleText = new StringBuilder();
            
            
			for(int lineCounter = progress; lineCounter<volumeLines.length; lineCounter++){
				progress++;
				
				if (volumeLines[lineCounter].trim().startsWith("BOOK ")) {
					String text = articleText.toString().trim();
					WordRecord wr1 = new WordRecord();
					wr1.word = text;
					wr1.frequency = bookID;
					//System.out.println("\t\t\t\t\t\t"+name+": my Volume ID: "+volumeNumber);
					bookID++;
					return wr1;
				}

				articleText.append(volumeLines[lineCounter]).append("\n");
			}

			//bookID++;
			WordRecord wr2 = new WordRecord();
			wr2.word = articleText.toString();
			wr2.frequency = bookID;
			return wr2;
		}
	}

	@Override
	public void run() {
		assignNewVolume();
		Thread [] masters = new Thread[threadCount];
		for(int i = 0; i < threadCount; i++) {
			masters[i] = new Thread(new Master(this, "M"+i));
			masters[i].start();
		}
		//System.out.println("underboss - masters created");
		for(Thread master : masters) {
			try {
				master.join();
				//System.out.println("master - join()");

			}
			catch(InterruptedException e){
				System.err.println("Underboss - masters did not joined");
			}
		}   
	}
	
	// ukladani vysledku z jednoho vtipu do wordRecords
	void reportResult(TreeSet<WordRecord> results) {
        synchronized (this.reportLock) {
        	Iterator<WordRecord> it = results.iterator();
        	while (it.hasNext()) {
    			WordRecord listItem = it.next();    			
    			WordRecord wr = Utils.getWordRecord(listItem.word, myResult);    			
    			if (wr == null)
    				myResult.add(listItem);
    			else {
    				myResult.remove(wr);
    				wr.frequency += listItem.frequency;
    				myResult.add(wr);
    			}
    		}
            
        }
    }


	// tisk vysledneho seznamu
	private void printResult() {
		synchronized(reportLock) {
			//System.out.println("\t\t\t\t\t\tunderboss - print");
			if(myResult==null){
				myResult = new TreeSet<>(new WRComparer());
				
			}
			boss.reportResult(myResult);		
			//System.out.println("\t\t\ttry to print volume summary"+volumeNumber+"--"+boss.volumeID);
			try {
				//File myObj = new File("lesmiserables\\volume"+myVolume.frequency+"\\volume"+myVolume.frequency+".txt");
				if(boss.iter<volumeNumber)boss.iter++;
				File myObj = new File("lesmiserables\\volume"+boss.iter+"\\volume"+boss.iter+".txt");
				myObj.getParentFile().mkdirs();
				if (myObj.createNewFile()) {
					//System.out.println("\t\t\t\t\t\tFile VOLUME created: " + myObj.getName());
				} else {
					//System.out.println("File already exists.");
				}
				FileWriter myWriter = new FileWriter(myObj);
				Iterator<WordRecord> it = myResult.iterator();
	
				while (it.hasNext()) {
					WordRecord listItem = it.next();
					myWriter.write(listItem.word + " " + listItem.frequency+"\n");
				}
	
				myWriter.close();
				//System.out.println("\t\t\tprint was hopefuly successfull "+volumeNumber+" -> data: "+myResult.size());

				//zapsani do state
				addToState();
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}
	}

	private void addToState(){
		synchronized (underbossLock) {
			//state of project			
			try {
				File myObj = new File("lesmiserables\\state.txt");
				myObj.getParentFile().mkdirs();
				if (myObj.createNewFile()) {
					System.out.println("File created: " + myObj.getName());
				} else {
					//System.out.println("File already exists.");
				}
				FileWriter myWriter = new FileWriter(myObj, true);
				//myWriter.write("Volume" + myVolume.frequency + " - OK\n");
				myWriter.write("Volume" + volumeNumber + " - OK\n");
				myWriter.close();
			} catch (IOException e) {
				System.out.println("An error occurred.");
				e.printStackTrace();
			}
		}
	}
}
