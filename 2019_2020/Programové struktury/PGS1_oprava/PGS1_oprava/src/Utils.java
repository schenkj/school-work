import java.util.TreeSet;

class Utils {

	// ziskani zaznamu slova z results
		static WordRecord getWordRecord(String item, TreeSet<WordRecord> results) {
			if(results==null){
				return null;
			}

			for (WordRecord listItem : results) {
				if (listItem.word.equals(item))
					return listItem;
			}		
			return null;
		}
}
