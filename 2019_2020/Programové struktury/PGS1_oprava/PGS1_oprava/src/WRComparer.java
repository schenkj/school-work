import java.util.Comparator;

public class WRComparer implements Comparator<WordRecord> {

    public int compare(WordRecord arg0, WordRecord arg1) {

        if (arg0.word.compareTo(arg1.word) == 0) {
			return Integer.compare(arg1.frequency, arg0.frequency);
        } else return arg0.word.compareTo(arg1.word);


    }
}
