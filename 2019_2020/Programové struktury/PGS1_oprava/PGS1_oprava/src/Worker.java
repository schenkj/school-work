import java.util.Iterator;
import java.util.TreeSet;

public class Worker implements Runnable {

    private final Farmer farmer;
    private final String name;
    private TreeSet<WordRecord> results = new TreeSet<>(new WRComparer());
    private int articleNumber = 0;
    public boolean susFlag = false;

    Worker(Farmer farmer, String name) {
        this.farmer = farmer;
        this.name = name;

    }

    @Override
    public void run() {
    	WordRecord wr;
    	while((wr = farmer.getArticle()) != null) {
    		results = new TreeSet<>(new WRComparer());
    		compute(wr);    		  
    	}
    }

    private void compute(WordRecord wr1) {
        String myArticle;       

        myArticle = wr1.word;        
        articleNumber = wr1.frequency;
        if(myArticle.equals(""))return;
        String[] words = myArticle.split("[^\\p{L}0-9]+");
        for (int i = 0; i < words.length; i++) {

            // prevedeni slova na mala pismena
            words[i] = words[i].toLowerCase();
            if (words[i].equals("")) {
                continue;
            }
            // zpracovani slova
            WordRecord wr = Utils.getWordRecord(words[i], results);

            if (wr == null) { // nove slovo ve vtipu
                wr = new WordRecord();
                wr.word = words[i];
                wr.frequency = 1;
            } else { // slovo se jiz ve vtipu vyskytlo
                results.remove(wr);
                wr.frequency++;
            }
            results.add(wr);
        }
        // ulozeni vysledku do seznamu u farmera
        farmer.reportResult(results);
    }


}
