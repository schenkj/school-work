'''
Created on 26. 4. 2020

@author: schenkj
'''
from pip._internal.utils.outdated import SELFCHECK_DATE_FMT

class MyInfo:
    def __init__(self, oc, pred, prac, sem, typ):
        self.oc = oc
        self.pred = pred
        self.prac = prac
        self.sem = sem
        self.typ = typ
    
    def __eq__(self, other):
        return self.oc==other.oc and self.pred==other.pred and self.prac==other.prac and self.sem==other.sem and self.typ==other.typ
    
    def __str__(self):
        return "\n"+self.oc+"|"+self.prac+"|"+self.pred+"|"+self.sem+"|"+self.typ
    
    def __repr__(self):
        return self.__str__()
    
    def __hash__(self):
        return hash(self.__str__())
        
def make_comparator(less_than):
    def compare(x, y):
        if less_than(x, y):
            return -1
        elif less_than(y, x):
            return 1
        else:
            return 0
    return compare

       
# Hlavni funkce 
from Sax_reader import Sax_reader
from Txt_reader import Txt_reader
import sys
import operator
import os
import time
from lib2to3.tests.pytree_idempotency import diff
from test.test_importlib.data01 import subdirectory

def main():
    

    start = time.time()
    arguments = len(sys.argv)
    if(arguments!=3):
        print("Chyba I/O!")
        #print("chybny pocet parametru {0:1d}".format(arguments))
        return 0
    file = sys.argv[1];
    myInfo = []
    
    
    #print("before if")
    if(file[-4]=="." and file[-3]=="x" and file[-2]=="m" and file[-1]=="l"):
        myInfo = Sax_reader(file)
    elif(file[-4]=="." and file[-3]=="t" and file[-2]=="x" and file[-1]=="t"):
        #print("else")
        myInfo = Txt_reader(file)
    else:
        print("Chyba I/O!")
        return 0
    
    if myInfo == 0:
        print("Chyba I/O!")
        return 0
    #file = "predzapisove-akce-test.txt"
    #Txt_reader(file)
    #print("after read")
    if not os.access(sys.argv[2], os.W_OK):
        print("Chyba I/O!")
        return 0
    
    oc = myInfo[0].copy()
    activity = myInfo[1].copy()
    id = myInfo[2].copy()  
    prac = myInfo[3].copy()
    pred=myInfo[4].copy()
    typ=myInfo[5].copy()
    sem=myInfo[6].copy()
    
    #print("before print")
    #print ("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}".format(oc[-1],activity[-1],id[-1],prac[-1],pred[-1],typ[-1],sem[-1]))
    
    studentsCounter = {}                                                      #pocitani nemizicich studentu
    for i in range(0,len(oc)):        
        if studentsCounter.__contains__(oc[i]):
            if activity[i]=="delete":
                studentsCounter[oc[i]]-=1
            else:
                studentsCounter[oc[i]]+=1
        else:
            studentsCounter[oc[i]]=1
    differentStudents = len(studentsCounter.keys())
    for student in studentsCounter.keys():
        if(studentsCounter[student]<=0):
            differentStudents-=1
    
    delSum = 0                                                              #pocitani general infa
    fullSum = 0
    counter=0;
    oc.sort()
    current = ""
    for student in oc:
        if activity[fullSum]=="delete":
            delSum+=1
        fullSum+=1   
    print("Počet všech předzápisových akcí: {0}".format(fullSum))
    print("Počet zrušených akcí (delete): {0}".format(delSum))
    nonDeletedInsert = fullSum - 2*delSum
    print("Počet skutečně zapsaných akcí: {0}".format(nonDeletedInsert))
    print("Počet studentů: {0}".format(differentStudents))
    
    oc = myInfo[0].copy()                                               #vraceni pole do neserazene formy
    
    subjectListOfLists = []                                             #napocitani poctu predmetu do seznamu
    gotIt = 0
    for i in range(0, len(pred)):#pro vsechny prvky
        for j in range(0, len(subjectListOfLists)):#zkontroluj ostatni nalezene prvky
            if(subjectListOfLists[j][0] == pred[i] and subjectListOfLists[j][1] == sem[i] and subjectListOfLists[j][3] == prac[i]):#jestli jsem dany prvek uz nasel
                gotIt=1#oznac si ze je nalezeny
                if activity[i]=="delete":#pokud je akce delete, tak je o jednu akci mene
                    subjectListOfLists[j][2]-=1
                else:#pokud neni, tak byl dan insert a je o akci vice
                    subjectListOfLists[j][2]+=1
                break
        if gotIt==0:#jestli prvek neznam, tak ho poznam
            subjectListOfLists.append([pred[i], sem[i], 1, prac[i]])
        else:#jestli prvek znam, tak nastavim, ze dalsi neznam
            gotIt = 0
    #print(len(subjectListOfLists))    
    differentSubjects = len(subjectListOfLists)
    for k in range(0,len(subjectListOfLists)):
        if(subjectListOfLists[k][2]==0):
            differentSubjects-=1
    
    print("Počet předmětů: {0}".format(differentSubjects))
    
    #print(wfCounter)
    counterWorkfields = 1
    #print(oc)
    oc=myInfo[0]
    #print(oc)
    data = [] #[osobni cislo, predmet, katedra, semestr, typ vyuky]
    for i in range(0,len(oc)):
        if activity[i] == "delete":
            info = MyInfo(oc[i], pred[i], prac[i], sem[i], typ[i])
            data.pop(data.index(info, 0, len(data)))
            
        else:
            info = MyInfo(oc[i], pred[i], prac[i], sem[i], typ[i])
            data.append(info)
    #print("------------------------------------------------------------")
    #sortedDict = sorted(data, cmp=make_comparator(cmpValue), reverse=True)
    data = set(data)
    data = sorted(data, key=lambda lam: lam.typ)
    data = sorted(data, key=lambda lam: lam.sem)
    data = sorted(data, key=lambda lam: lam.pred)
    data = sorted(data, key=lambda lam: lam.prac)
    #print("-")
    wfCounter = {}
    mySubjectCounter = 0
    actualPrac = ""
    actualPred = ""
    actualSem = ""
    myDepartmentCounter = 0
    hahahaMyPerfectArray = [0,0,0]
    
    for i in data:
        
        if actualPrac!=i.prac:
            if hahahaMyPerfectArray[0]!=0:
                mySubjectCounter = hahahaMyPerfectArray[0]
            elif hahahaMyPerfectArray[1]!=0:
                mySubjectCounter = hahahaMyPerfectArray[1]
            else:
                mySubjectCounter = hahahaMyPerfectArray[2]
            #print(str(hahahaMyPerfectArray)+"\t"+actualPrac)    
            myDepartmentCounter+=mySubjectCounter
            wfCounter[actualPrac]=myDepartmentCounter
            myDepartmentCounter = 0
            actualPrac=i.prac
            actualPred=i.pred
            actualSem=i.sem
            mySubjectCounter = 0
            hahahaMyPerfectArray = [0,0,0]
        
        elif actualPred!=i.pred or actualSem!=i.sem:
            if hahahaMyPerfectArray[0]!=0:
                mySubjectCounter = hahahaMyPerfectArray[0]
            elif hahahaMyPerfectArray[1]!=0:
                mySubjectCounter = hahahaMyPerfectArray[1]
            else:
                mySubjectCounter = hahahaMyPerfectArray[2]
            #print(str(hahahaMyPerfectArray)+"\t"+actualPrac)    
            myDepartmentCounter+=mySubjectCounter
            hahahaMyPerfectArray = [0,0,0]
            mySubjectCounter = 0
            actualPred = i.pred
            actualSem=i.sem
            
        #print(i)
        if i.typ == "Př":
            hahahaMyPerfectArray[0]+=1
        
        if i.typ == "Cv":
            hahahaMyPerfectArray[1]+=1
         
        if i.typ == "Se":
            hahahaMyPerfectArray[2]+=1
            
    if hahahaMyPerfectArray[0]!=0:
        mySubjectCounter = hahahaMyPerfectArray[0]
    elif hahahaMyPerfectArray[1]!=0:
        mySubjectCounter = hahahaMyPerfectArray[1]
    else:
        mySubjectCounter = hahahaMyPerfectArray[2]
    myDepartmentCounter+=mySubjectCounter
    wfCounter[actualPrac]=myDepartmentCounter 
    wfCounter.pop("")
        

    f = open(sys.argv[2], "w")
    f.write("Počet všech předzápisových akcí: {0}\n".format(fullSum))
    f.write("Počet zrušených akcí (delete): {0}\n".format(delSum))
    f.write("Počet skutečně zapsaných akcí: {0}\n".format(nonDeletedInsert))
    f.write("Počet studentů: {0}\n".format(differentStudents))
    f.write("Počet předmětů: {0}\n".format(differentSubjects))
    f.write("Počet pracovišť: {0}\n".format(len(wfCounter)))  
    print("Počet pracovišť: {0}\n".format(len(wfCounter)))      
    #print(data)           
    wfAndStudents = sorted(wfCounter.items(), key=operator.itemgetter(0))  
    wfAndStudents = sorted(wfAndStudents, key=operator.itemgetter(1))   
    #print(wfAndStudents)
    #wfAndStudents = [v[0] for v in sorted(wfCounter.iteritems(), key=lambda(k, v): (-v, k))]
    #vypis pracovist s poctem studentu         
    for k in range(0,len(wfAndStudents)):
        if int(wfAndStudents[k][1])>0:
            print("{0}. {1}: {2}".format(counterWorkfields, wfAndStudents[k][0], wfAndStudents[k][1]))
            f.write("{0}. {1}: {2}\n".format(counterWorkfields, wfAndStudents[k][0], wfAndStudents[k][1]))
            counterWorkfields+=1
    f.close()   
    end = time.time()
    print(end - start)
main()
