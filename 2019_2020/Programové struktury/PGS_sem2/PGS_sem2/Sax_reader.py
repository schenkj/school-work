'''
Created on 26. 4. 2020

@author: jakub
'''
# potrebne pro SAX
from xml.sax import handler
from xml.sax import make_parser

   
class BarHandler(handler.ContentHandler):
    
    # konstruktor tridy    
    def __init__(self):
        
        # inicializace dat tridy
        self.__inDepartment    = False # indikator stavu
        self.__inSubject     = False # indikator stavu
        self.__inTerm   = False # indikator stavu
        self.__oc = []  # seznam oc
        self.__activity   = []    # seznam ...
        self.__id = []
        self.__prac = []
        self.__pred = []
        self.__typ = []
        self.__sem = []

    # prekryti metody startElement(self)
    def startElement(self, name, attrs):  
        try:
            if (name == "actor"):
                self.__oc.append(attrs.get("personalNumber"))
            elif (name == "processedData"):
                self.__activity.append(attrs.get("activity"))
            elif (name == "timetableAction"):
                self.__id.append(attrs.get("tt:id"))
            elif (name == "tt:department"):
                self.__prac.append("")
                self.__inDepartment = True
            elif (name == "tt:subject"):
                self.__typ.append(attrs.get("kind"))
                self.__pred.append("")
                self.__inSubject = True               
            elif (name == "tt:term"):
                self.__sem.append("")
                self.__inTerm = True
        except:
            return 0                       
        # ulozeni stavu, kde se prave ve XML nachazime
        

    # prekryti metody endElement(self, name)
    def endElement(self, name):
        try:
            if (name == "tt:department"):
                self.__inDepartment = False
            elif (name == "tt:subject"):
                self.__inSubject = False
            elif (name == "tt:term"):
                self.__inTerm = False
        except:
            return 0 
        # ulozeni stavu, kde se prave ve XML nachazime
        
        
    # prekryti metody characters(self, chrs)    
    def characters(self, chrs):                                 
        if self.__inDepartment:
            str = self.__prac.pop(len(self.__prac)-1)
            str+=chrs
            self.__prac.append(str)
                
        elif self.__inSubject:
            str = self.__pred.pop(len(self.__pred)-1)
            str+=chrs
            self.__pred.append(str)
        
        elif self.__inTerm:
            str = self.__sem.pop(len(self.__sem)-1)
            str+=chrs
            self.__sem.append(str)
    
    def gimiAll(self):
        myList = []
        myList.append(self.__oc)
        myList.append(self.__activity)
        myList.append(self.__id)
        myList.append(self.__prac)
        myList.append(self.__pred)
        myList.append(self.__typ)
        myList.append(self.__sem)
        
        return myList
    
def Sax_reader(file):
    try:
        handler = BarHandler()
        parser = make_parser()                               # vytvoreni instance parseru 
        parser.setContentHandler(handler)                    # nastaveni handleru
        
        inFile = open(file, "r", encoding="UTF-8")           # otevreni souboru pro cteni
        parser.parse(inFile)                                 # parsovani souboru    
        inFile.close()                                       # zavreni souboru
    except:
        return 0   
    
    myData = handler.gimiAll()   
   
    return myData
 