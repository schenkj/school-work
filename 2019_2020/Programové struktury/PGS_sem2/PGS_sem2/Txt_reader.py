from _socket import close
from _multiprocessing import sem_unlink

def Txt_reader(file):
    try:
        inFile = open(file, "r", encoding="UTF-8")
        fullFile = inFile.read()
        inFile.close()
    except:
        return 0
    oc = []
    activity = []
    id = []
    prac = []
    pred = []
    typ = []
    sem = []
    str = fullFile.split("\n")
    s1 = []
    for splited in str:
        for x in splited.split(";"):
            s1.append(x)
        
    x = 0;
    
    for s in s1:
        #s = s.replace("\n","")
        if(x%7==0):
            oc.append(s)
            #print(oc[-1])
        elif(x%7==1):
            activity.append(s)
            #print(activity[-1])
        elif(x%7==2):
            id.append(s)
            #print(id[-1])
        elif(x%7==3):
            prac.append(s)
            #print(prac[-1])
        elif(x%7==4):
            pred.append(s)
            #print(pred[-1])
        elif(x%7==5):
            typ.append(s)
            #print(typ[-1])
        elif(x%7==6):
            sem.append(s)
            #print(sem[-1])
            
        x=x+1    
    
    oc.remove(oc[-1])
    myInfo = []
    myInfo.append(oc)
    myInfo.append(activity )   
    myInfo.append(id)
    myInfo.append(prac)
    myInfo.append(pred)
    myInfo.append(typ)
    myInfo.append(sem)
    
    if not (len(oc) == len(activity) == len(id) == len(prac) == len(pred) == len(typ) == len(sem)):
        return 0
    
    return myInfo
