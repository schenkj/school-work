import javax.swing.*;
import javax.swing.text.DefaultCaret;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;

public class A18B0311P {
	public static ArrayList<String> myClassesForClassification = new ArrayList<String>();
	public static ArrayList<Item> myTrainingStrings = new ArrayList<Item>();
	public static String allInOneTrainData = "";
	public static ArrayList<String> myTestedStrings = new ArrayList<String>();
	public static String myParamAlg;
	public static String myClassAlg;
	public static String myModel;
	public static boolean IAmAfterTraining = false;
	public static boolean fullRun = true;
	public static JFrame frame;
	public static void main(String[] args) {
		long startTime = System.nanoTime();
		if(args.length==6) {
			String classes = args[0];
			String training = args[1];
			String test = args[2];
			String paramAlg = args[3];
			String classAlg = args[4];
			String model = args[5];
			try {
				loadInfo(classes, training, test, paramAlg, classAlg);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
			
			
			/*System.out.println(classes);
			System.out.println(training);
			System.out.println(test);
			System.out.println(paramAlg);
			System.out.println(classAlg);
			System.out.println(model);*/
			myModel = model;
			myParamAlg = paramAlg;
			myClassAlg = classAlg;
		}
		else if(args.length==2) {
			fullRun = false;
			myClassAlg = args[0];
			myModel = args[1];
			//GUI
	        frame = new JFrame("Text Frame");
	        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	        frame.setSize(800, 400);
	        JPanel panel = new JPanel();
	        JButton send = new JButton("Send");
	        panel.add(send);
	        JTextArea ta = new JTextArea("Insert your text here");
	        ta.setFont(new Font("Serif", Font.ITALIC, 16));
	        ta.setLineWrap(true);
	        ta.setWrapStyleWord(true);
	        
	        JScrollPane scroll = new JScrollPane(ta);
	        frame.getContentPane().add(BorderLayout.SOUTH, panel);
	        frame.getContentPane().add(BorderLayout.CENTER, scroll);
	        frame.setVisible(true);
	        
	        send.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					String sendText = ta.getText();
					myTestedStrings.add(sendText);
					//TODO odeslat text na klasifikaci
					frame.getContentPane().remove(panel);
					//System.out.println(sendText);
				}
	        });
	    //rest     
		        
		}
		else { 
			System.out.println("Wrong number of inputs: "+args.length);
			return;
		}

		while(myTestedStrings.isEmpty()) {
			System.out.print("");
		}
		
		if(args.length==2) {myTrainingStrings=null;}
		Trainer tr = new Trainer(myTrainingStrings);
		//System.out.println("my param alg: "+myParamAlg);
		Classifier cs = new Classifier(myTestedStrings, tr);	
		
		
		if(args.length==2) {					//nacteni z modelu
			File f = new File(myModel);
			if(myModel!=null && !myModel.isBlank()) {
				try {
					readModel(tr, cs);
				}
				catch (Exception e) {
					e.printStackTrace();
				}	
				
			}
		}
		else {
			tr.train(myParamAlg, null);		
		}
		
		System.out.println("------------------after training");
		IAmAfterTraining = true;
		
		cs.cassify(myClassAlg);
		int succ = 0;
		if(myClassAlg.equals("knn")) {
			//System.out.println("KNN - for tested data");
			for(int i = 0; i < myTestedStrings.size(); i++){
				String[] rightRes = myTestedStrings.get(i).split("\\W+");
				System.out.println(rightRes[0]+" - "+cs.resultList.get(i));
				if(rightRes[0].equals(cs.resultList.get(i)))succ++;
			}
			if(fullRun)System.out.println("Success rate: "+(100*succ/myTestedStrings.size())+"%");
			else frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));		
		}
		
		else if(myClassAlg.equals("nb")) {
			//System.out.println("NB - for tested data");
			for(int i = 0; i < myTestedStrings.size(); i++){
				String[] rightRes = myTestedStrings.get(i).split("\\W+");
				if(fullRun)System.out.println(rightRes[0]+" - "+cs.resultList.get(i));
				else System.out.println("Result: "+cs.myResults[0]+" "+cs.myResults[1]+" "+cs.myResults[2]);
				if(rightRes[0].equals(cs.resultList.get(i)))succ++;
			}
			if(fullRun)System.out.println("Success rate: "+(100*succ/myTestedStrings.size())+"%");
			else frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
		}
		
		if(args.length==6) {	//ulozeni do modelu
			try {
				saveModel(tr, cs);
			}
			catch (Exception e) {
				e.printStackTrace();
			}			
		}
		
		long endTime = System.nanoTime();
		double nanos = endTime - startTime;
		double micros = (double) (nanos/1000.0);
		double milis = (double) (micros/1000.0);
		double secs = (double) (milis/1000.0);
		double mins = (double) (secs/60.0);
		//System.out.println("time: "+mins);
	}
	
	
	
	private static void loadInfo(String classes, String training, String test, String paramAlg, String classAlg) throws FileNotFoundException {
		File f;
		Scanner myReader;
		f = new File("./"+classes);		
		myReader = new Scanner(f);
		while (myReader.hasNextLine()) {
		    String data = myReader.nextLine();
		    myClassesForClassification.add(data);
		    //System.out.println("myClassesForClassification "+data);
		}
		myReader.close();
		
		File folder = new File("./"+training);
		File[] listOfTrainingFiles = folder.listFiles();
		
		for(int i = 0; i < listOfTrainingFiles.length; i++) {
			Scanner sc1;
			String kind = "";
			try {
				sc1 = new Scanner(Path.of("./"+listOfTrainingFiles[i]));
				kind = sc1.nextLine();
				sc1.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			} 
			
			String data = "";
			try {
				data = Files.readString(Path.of("./"+listOfTrainingFiles[i]));
			} catch (IOException e) {
				e.printStackTrace();
			}
			allInOneTrainData+=" "+data;
			for(int j = 0; j < myClassesForClassification.size(); j++) {
				if(kind.contains(myClassesForClassification.get(j))) {
					myTrainingStrings.add(new Item(data, 0, myClassesForClassification.get(j)));
				}					
			}
			
		}
		File testfolder = new File("./"+test);
		File[] listOfTestFiles = testfolder.listFiles();

		for(int i = 0; i < listOfTestFiles.length; i++) {
			String data = "";
			try {
				data = Files.readString(Path.of("./"+listOfTestFiles[i]));
			} catch (IOException e) {
				e.printStackTrace();
			}
			myTestedStrings.add(data);
		}
		
	}
	
	public static void saveModel(Trainer tr, Classifier cs) {
		try {
			File f = new File(myModel);
			if(!f.canWrite()) {System.out.println("An error occured - cant write model there");return;}
			FileWriter myWriter = new FileWriter(f);
			//v cem je to zapsane
		     myWriter.write(tr.choice+"\n");
		     if(tr.choice.contentEquals("bow")) {
		    	 myWriter.write(tr.allDifferentStringsInSetBOW.size()+"\n");
			     for(int i = 0; i < tr.bowResults.size(); i++) {
			    	 for(int j = 0; j < tr.bowResults.get(i).length; j++) {
			    		myWriter.write(tr.bowResults.get(i)[j]+","); 
			    	 }
			    	 myWriter.write("\n");
			     }
		     }
		     else if(tr.choice.contentEquals("ngram")) {
		    	 myWriter.write(tr.allDifferentStringsInSetNGRAM.size()+"\n");
			     for(int i = 0; i < tr.ngramResults.size(); i++) {
			    	 for(int j = 0; j < tr.ngramResults.get(i).length; j++) {
			    		myWriter.write(tr.ngramResults.get(i)[j]+","); 
			    	 }
			    	 myWriter.write("\n");
			     }				     
		     }
		     else if(tr.choice.contentEquals("tfidf")) {
		    	 myWriter.write(tr.allDifferentStringsInSetTFIDF.size()+"\n");
			     for(int i = 0; i < tr.tfidfResults.size(); i++) {
			    	 for(int j = 0; j < tr.tfidfResults.get(i).length; j++) {
			    		myWriter.write(tr.tfidfResults.get(i)[j]+","); 
			    	 }
			    	 myWriter.write("\n");
			     }				     
		     }
		     myWriter.write("&\n");
		     for(int i = 0; i <A18B0311P.myClassesForClassification.size(); i++) {
		    	 myWriter.write(A18B0311P.myClassesForClassification.get(i)+",");
		     }
		     myWriter.write("\n");
		     if(cs.PClass!=null) {
		    	 for(int i = 0; i < cs.PClass.length; i++) {
		    		 myWriter.write(cs.PClass[i]+",");
		    	 }
		     }
		     myWriter.write("\n");
		     for(int i = 0; i < tr.numOfStringsInClass.length; i++) {
		    	 myWriter.write(tr.numOfStringsInClass[i]+",");
		     }
		     myWriter.write("\n");
		     
		     //nb completed
		     
		     for(int i = 0; i < tr.numberOfFilesWithClass.size(); i++) {
		    	myWriter.write(tr.numberOfFilesWithClass.get(i)+","); 
		     }
		     myWriter.write("\n");
		     // data pro trenovani
		     //tfidf
		     for(int i = 0; i < tr.tfidfAllStrings.size(); i++) {
		    	 myWriter.write(tr.tfidfAllStrings.get(i)+",");
		     }
		     myWriter.write("\n");
		     for(Map.Entry<String, Integer> entry : tr.tfidfWordCount.entrySet()) {
		    	    String key = entry.getKey();
		    	    Integer value = entry.getValue();
		    	    myWriter.write(key+"-"+value+",");
		     }
		     myWriter.write("\n");
		     //bow
		     for(int i = 0; i < tr.bowString.size(); i++) {
		    	 myWriter.write(tr.bowString.get(i)+",");
		     }
		     myWriter.write("\n");
		     //ngram
		     for(int i = 0; i < tr.ngramString.size(); i++) {
		    	 myWriter.write(tr.ngramString.get(i)+",");
		     }
		     myWriter.write("\n");
		     myWriter.close();
		     
		}
		catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void readModel(Trainer tr, Classifier cs) {
		Scanner sc1;
		try {
			sc1 = new Scanner(Path.of("./"+myModel));
			tr.choice = sc1.nextLine().trim();
			//pocet vsech stringu
			int in = Integer.parseInt(sc1.nextLine().trim());
		    cs.allStrings = in;
		    
		    //nacitani natrenovanych vektoru
		    String s = sc1.nextLine();
		    String split[] = s.split(",");
		    double[] myVector;
		    while(!s.equals("&")) {		    	
		    	myVector = new double[split.length];
		    	for(int i = 0; i < split.length; i++) {//pokud to pada na null, tak posledni prvek je prazdny... zkratit for o -1
		    		myVector[i] = Double.parseDouble(split[i]);
		    	}
		    	tr.bowResults.add(myVector);
		    	tr.ngramResults.add(myVector);
		    	tr.tfidfResults.add(myVector);
		    	cs.trainingVector.add(myVector);
		    	s = sc1.nextLine();
		    	split = s.split(",");
		    }
		    //classes for classification
		    s = sc1.nextLine();
	    	split = s.split(",");
		    for(int i = 0; i < split.length; i++) {
		    	myClassesForClassification.add(split[i]);
		    }
		    //pravdepodobnosti jednotlivych trid
		    s = sc1.nextLine();
	    	split = s.split(",");
	    	double [] pc = new double[myClassesForClassification.size()];
		    for(int i = 0; i < split.length; i++) {
		    	pc[i] = Double.parseDouble(split[i]);
		    }
		    cs.PClass = pc;
		    //soucty slov ve tridach
		    s = sc1.nextLine();
	    	split = s.split(",");
	    	tr.numOfStringsInClass = new int[myClassesForClassification.size()];
		    for(int i = 0; i < split.length; i++) {
		    	tr.numOfStringsInClass[i] = Integer.parseInt(split[i]);
		    }		     
		    //nb completed
		    //pocet dokumentu e stejnou tridou (pro knn)
		    s = sc1.nextLine();
		    split = s.split(",");
		    for(int i = 0; i < split.length; i++) {
		    	tr.numberOfFilesWithClass.add(Integer.parseInt(split[i]));
		    }
		    //tfidf
		    s = sc1.nextLine();
		    split = s.split(",");
		    for(int i = 0; i < split.length; i++) {
		    	 tr.tfidfAllStrings.add(split[i]);
		     }
		    s = sc1.nextLine();
		    split = s.split(",");
		    if(split.length>1)
		    for(int i = 0; i < split.length; i++) {
		    	String [] split2 = split[i].split("-");
		    	tr.tfidfWordCount.put(split2[0], Integer.parseInt(split2[1]));
		    }
		     //bow
		     s = sc1.nextLine();
		     //System.out.print(s);
			 split = s.split(",");
		     for(int i = 0; i < split.length; i++) {
		    	 tr.bowString.add(split[i]);
		     }
		     //ngram
		     s = sc1.nextLine();
			 split = s.split(",");
		     for(int i = 0; i < split.length; i++) {
		    	 tr.ngramString.add(split[i]);
		     } 
		    
			sc1.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		} 
	}
}
