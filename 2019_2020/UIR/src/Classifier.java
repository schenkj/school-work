import java.util.ArrayList;

public class Classifier {
	public ArrayList<String> myList = new ArrayList<String>();
	public ArrayList<String> resultList = new ArrayList<String>();
	public Trainer tr;
	public String choice;
	public int allStrings;
	public double PClass[];
	public String[] myResults = new String[3];
	public ArrayList<double[]> trainingVector = new ArrayList<double[]>();
	public Classifier(ArrayList<String> list, Trainer trainer) {
		myList = list;
		tr = trainer;
	}
	public void cassify(String algor) {
		String alg = algor.toLowerCase();
		if(alg.equals("nb") || alg.equals("bays")) {
			nb();
			choice="nb";
		}
		
		else if(alg.equals("knn") || alg.equals("k-nn")) {
			knn();
			choice="knn";
		}
		
		else System.out.println("I don't know this algorithm.");
	}
	
	public void nb() {
		if(tr.choice.equals("bow")) {
			trainingVector = tr.bowResults;
			if(allStrings==0)allStrings = tr.allDifferentStringsInSetBOW.size();
		}
		else if(tr.choice.equals("tfidf")) {
			trainingVector = tr.tfidfResults;
			if(allStrings==0)allStrings = tr.allDifferentStringsInSetTFIDF.size();
		}
		else if(tr.choice.equals("ngram")) {
			trainingVector = tr.ngramResults;
			if(allStrings==0)allStrings = tr.allDifferentStringsInSetNGRAM.size();
		}			
		else {
			System.out.println("classifier did not found choice");
			return;
		}
		if(A18B0311P.fullRun) {
			PClass = new double[A18B0311P.myClassesForClassification.size()];
			for(int j = 0; j < PClass.length; j++) {
				for(int k = 0; k < A18B0311P.myTrainingStrings.size(); k++) {
					if(A18B0311P.myTrainingStrings.get(k).getType().equals(A18B0311P.myClassesForClassification.get(j))) {
						PClass[j]++;
					}
				}
				PClass[j]/=A18B0311P.myTrainingStrings.size();
			}
		} 
		for(int i = 0; i < myList.size(); i++) {	//pro kazdy testovany soubor		
			double [] results = new double[A18B0311P.myClassesForClassification.size()];
			double [] myResults = new double[A18B0311P.myClassesForClassification.size()];
			tr.train(tr.choice, myList.get(i));
			double[] newVect = tr.vec;
			//p�es v�echny vektory t��d
			for(int j = 0; j < A18B0311P.myClassesForClassification.size(); j++) {
				double num = 0;
				//pro ka�d� slovo testovaneho souboru
				for(int k = 0;k < newVect.length; k++) {
					if(newVect[k]!=0) {
						double adding = (trainingVector.get(j)[k]*1.0 + 1.0) / (tr.numOfStringsInClass[j]*1.0 + 1.0 + allStrings*1.0);//+tr.ATnumOfStringsInClass*1.0
						num += Math.log(adding);
					}
				}
				results[j] = num;
			}
			double myMax = -Double.MAX_VALUE;
			int myMaxIndex = A18B0311P.myClassesForClassification.size();
			for(int j = 0; j < A18B0311P.myClassesForClassification.size(); j++) {
				results[j]+=Math.log(PClass[j]);
				myResults[j] = results[j];
				if(results[j]>myMax) {
					myMax = results[j];
					myMaxIndex = j;
				}
			}
			double one = -Double.MAX_VALUE;
			double two = -Double.MAX_VALUE;
			double three = -Double.MAX_VALUE;
			int first = 0;
			int second = 0;
			int third = 0;
			for(int j = 0; j < myResults.length; j++) {
				if(myResults[j]>one) {
					three = two;
					two = one;
					one = myResults[j];
					third = second;
					second = first;
					first = j;
				}
				else if(myResults[j]>two) {
					three = two;
					two = myResults[j];
					third = second;
					second = j;
				}
			}
			this.myResults[0] = A18B0311P.myClassesForClassification.get(first);
			this.myResults[1] = A18B0311P.myClassesForClassification.get(second);
			this.myResults[2] = A18B0311P.myClassesForClassification.get(third);
			resultList.add(A18B0311P.myClassesForClassification.get(myMaxIndex));
		}
	}
	
	public void knn() {
		System.out.println("knn");
		if(tr.choice=="bow") {
			trainingVector = tr.bowResults;
		}
		else if(tr.choice=="tfidf") {
			
			trainingVector = tr.tfidfResults;
		}
		else if(tr.choice=="ngram") {
			trainingVector = tr.ngramResults;
		}			
		else {
			System.out.println("classifier did not found choice");
			return;
		}
		int minIndex;
		
		if(trainingVector.size()==0) {
			System.out.println("empty class vector");
		}
		for(int i = 0; i < trainingVector.size(); i++) {
			
			for(int k = 0; k < trainingVector.get(i).length; k++) {
				int num = tr.numberOfFilesWithClass.get(i);
				if(tr.numberOfFilesWithClass.get(i)!=0)trainingVector.get(i)[k]/=num;
				else trainingVector.get(i)[k] = Double.MAX_VALUE;
			}
		}		
		
		for(int j = 0; j < myList.size(); j++) {	//pro kazdy testovany soubor		
			tr.train(tr.choice, myList.get(j));
			double[] newVect = tr.vec;
			double minDist = Double.MAX_VALUE;
			
			//else System.out.println("calculate distances ");
			minIndex = trainingVector.size();
			int maxIterations = minIndex;
			int i;
			for(i = 0; i < maxIterations; i++) {//pro kazdy natrenovany vektor (tzn. pro ka�dou tridu)
				double currSum = 0;
				for(int k = 0; k < trainingVector.get(i).length; k++) {//pro kazdou jeho slo�ku
					//k-ta slozka jednoho z natrenovanych vektoru a testovaneho vektoru na druhou
					
					currSum+= (trainingVector.get(i)[k]-newVect[k])*(trainingVector.get(i)[k]-newVect[k]);
				}
				double dist = Math.sqrt(currSum);
				if(minDist>dist && trainingVector.get(i).length!=0 && trainingVector.get(i)[0]!= Double.MAX_VALUE) {
					
					minDist = dist;
					minIndex = i;
				}
			}
			
			//v minIndex je index nejblizsiho vektoru k tomu testovanemu
			resultList.add(A18B0311P.myClassesForClassification.get(minIndex));
		}

	}
}
