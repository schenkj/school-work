
public class Item{
	private String val;
	private int count;
	private String type;
	public Item(String val, int count, String type) {
		this.val = val;
		this.count = count;
		this.type = type;
	}
	public void found() {
		count++;
	}
	public String getVal() {return val;}
	public int getCount() {return count;}
	public String getType() {return type;}
}