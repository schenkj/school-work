import java.util.ArrayList;
import java.util.HashMap;

public class Trainer {
	public ArrayList<Item> myList = new ArrayList<Item>();
	public ArrayList<String> bowString = new ArrayList<String>();
	public ArrayList<String> ngramString = new ArrayList<String>();
	public ArrayList<double[]> bowResults = new ArrayList<double[]>();
	public ArrayList<double[]> ngramResults = new ArrayList<double[]>();
	public ArrayList<ArrayList<String>> tfidfBowString = new ArrayList<ArrayList<String>>();
	public ArrayList<ArrayList<Double>> tfidfBowResults = new ArrayList<ArrayList<Double>>();
	public ArrayList<String> tfidfBowTypes = new ArrayList<String>();
	public ArrayList<double[]> tfidfResults = new ArrayList<double[]>();
	public int[] numOfStringsInClass;
	public ArrayList<String>tfidfAllStrings = new ArrayList<String>();
	public ArrayList<Integer>numberOfFilesWithClass = new ArrayList<Integer>();
	public String choice;
	public ArrayList<String> allDifferentStringsInSetBOW = new ArrayList<String>();
	public ArrayList<String> allDifferentStringsInSetNGRAM = new ArrayList<String>();
	public ArrayList<String> allDifferentStringsInSetTFIDF = new ArrayList<String>();
	public HashMap<String, Integer> tfidfWordCount = new HashMap<String, Integer>();
	
	public int ATnumOfStringsInClass = 0;
	public String ATstring; //prichozi string z testovani
	public double[] vec; // vector pro testovani
	public Trainer(ArrayList<Item> list) {
		if(list==null)return;
		myList = list;
	}
	public void train(String algor, String s) {
		if(!A18B0311P.IAmAfterTraining) {
			ATstring = null;
			for(int i = 0; i < A18B0311P.myClassesForClassification.size(); i++) {
				numberOfFilesWithClass.add(0);
			}
		}
		else ATstring = s;
		choice = algor.toLowerCase();
		
		if(choice.equals("bow") || choice.equals("bagofwords")) {
			bagOfWords();
			choice="bow";
		}
		
		else if(choice.equals("tfidf") || choice.equals("tf-idf")) {
			if(!A18B0311P.IAmAfterTraining) {
				for(int i = 0; i < A18B0311P.myClassesForClassification.size(); i++) {
					tfidfBowResults.add(new ArrayList<Double>());
					tfidfBowString.add(new ArrayList<String>());
					tfidfBowTypes.add("");
				}
			}
			tfidf();
			choice="tfidf";
		}
		
		else if(choice.equals("ngram") || choice.equals("n-gram")) {
			ngram();
			choice="ngram";
		}
		else System.out.println("I don't know this algorythm.");
	}
	
	private void tfidf() {		
		System.out.println("Do not turn me off, it may take a while");
		ArrayList <String> typDokumetu = new ArrayList<String>();
		ArrayList <double[]> vektorDokumetu = new ArrayList<double[]>();
		if(!A18B0311P.IAmAfterTraining) {
			//tahle cast mi delala takove problemy, ze obsahuje jako jedina ceske nazvy pro proměnné
			for(int i = 0; i < myList.size(); i++) {
				String [] s = myList.get(i).getVal().split("[^\\p{L}0-9]+");
				for(int j = 0; j < s.length; j++) {
					
					if(!tfidfAllStrings.contains(s[j]))tfidfAllStrings.add(s[j]);	
					if(!allDifferentStringsInSetTFIDF.contains(s[j])) {allDifferentStringsInSetTFIDF.add(s[j]);}
				}
			}
			for(int i = 0; i < myList.size(); i++) {
				for(int j = 0; j < allDifferentStringsInSetTFIDF.size(); j++) {
					String s = allDifferentStringsInSetTFIDF.get(j);
					if(myList.get(i).getVal().contains(s)) {
						if(tfidfWordCount.containsKey(s)) {
							tfidfWordCount.put(s, tfidfWordCount.get(s)+1);
						}
						else tfidfWordCount.put(s, 1);
					}
				}
			}
			//inicializace vektorů
			for(int  i = 0; i < myList.size(); i++) {
				vektorDokumetu.add(new double[tfidfAllStrings.size()]);
				typDokumetu.add("");
			}
			
			for(int j = 0; j < myList.size(); j++) {
				
				for(int i = 0; i < tfidfAllStrings.size(); i++) {
					int pocetVyskytuSlovaVJednomDokumentu = 0;
					String [] s = myList.get(j).getVal().split("[^\\p{L}0-9]+");
					String a = tfidfAllStrings.get(i);
					for(int k = 0; k < s.length; k++) {
						if(s[k].equals(a))pocetVyskytuSlovaVJednomDokumentu++;
					}
					
					double finalniHodnota = ((pocetVyskytuSlovaVJednomDokumentu*1.0)/(1.0*s.length));
					finalniHodnota*=Math.log((myList.size()*1.0)/(tfidfWordCount.get(a).intValue()*1.0));
					//mam hodnotu do vektoru pro konkrétní slovo v konkrétním dokumentu
					vektorDokumetu.get(j)[i] = finalniHodnota;
					
				}
				typDokumetu.set(j,myList.get(j).getType());
				//System.out.println("\tvektorDokumentu: ["+vektorDokumetu.get(j)[0]+" "+vektorDokumetu.get(j)[1]+" "+vektorDokumetu.get(j)[2]+" "+vektorDokumetu.get(j)[3]+" "+"...]");

				int num = numberOfFilesWithClass.get(A18B0311P.myClassesForClassification.indexOf(myList.get(j).getType()));
				numberOfFilesWithClass.set(A18B0311P.myClassesForClassification.indexOf(myList.get(j).getType()),num+1);
			}
		}
		else {
			//System.out.println("trenuji test");
			double[] vektorTestu;
			//inicializace vektoru
			vektorTestu = new double[tfidfAllStrings.size()];			
			for(int i = 0; i < tfidfAllStrings.size(); i++) {
				String a = tfidfAllStrings.get(i);
				int pocetVyskytuSlovaVJednomDokumentu = 0;
				String [] s = ATstring.split("[^\\p{L}0-9]+");
				for(int k = 0; k < s.length; k++) {
					if(s[k].equals(a))pocetVyskytuSlovaVJednomDokumentu++;
				//	if(!tfidfAllStrings.contains(s[k]))ATnumOfStringsInClass++;
				}
				if(pocetVyskytuSlovaVJednomDokumentu == 0)vektorTestu[i]=0;
				else {
					double finalniHodnota = ((pocetVyskytuSlovaVJednomDokumentu*1.0)/(s.length*1.0));
					double myNum = Math.log((myList.size()*1.0)/(tfidfWordCount.get(a).intValue()*1.0));
					finalniHodnota*= myNum;
					//mam hodnotu do vektoru pro konkrétní slovo v konkrétním dokumentu
					vektorTestu[i] = finalniHodnota;		
				}
			}
			vec = vektorTestu;
			//System.out.println("\tvektorTestu: ["+vektorTestu[0]+" "+vektorTestu[1]+" "+vektorTestu[2]+" "+vektorTestu[3]+" "+"...]");
		}
		if(!A18B0311P.IAmAfterTraining) {
			numOfStringsInClass = new int[A18B0311P.myClassesForClassification.size()];
			for(int i = 0; i < A18B0311P.myClassesForClassification.size(); i++) {
				tfidfResults.add(new double[tfidfAllStrings.size()]);
				for(int j = 0; j < myList.size(); j++) {
					if(myList.get(j).getType().equals(A18B0311P.myClassesForClassification.get(i))){
						numOfStringsInClass[i]+=myList.get(j).getVal().split("[^\\p{L}0-9]+").length;
					}
				}
			}
			
			//System.out.println("tfidfResults"+tfidfResults.size());
			for(int i = 0; i < vektorDokumetu.size(); i++) {
				for(int j = 0; j < vektorDokumetu.get(i).length; j++) {
					tfidfResults.get(A18B0311P.myClassesForClassification.indexOf(typDokumetu.get(i)))[j] = vektorDokumetu.get(i)[j];
				}
				//tfidfResults.set(A18B0311P.myClassesForClassification.indexOf(typDokumetu.get(i)),vektorDokumetu.get(i));
			}
		}
	}
	
	
	private void bagOfWords() {
		if(!A18B0311P.IAmAfterTraining) {
			//System.out.println("bow");
			String myString = myList.get(0).getVal();
			//String myString = A18B0311P.allInOneTrainData;
			for(int i = 1; i < myList.size(); i++) {
				myString+=" "+myList.get(i).getVal();
			}
			myString.toLowerCase();
			String [] separated = myString.split("[^\\p{L}0-9]+");//jednotliva slova
			for(int i = 0; i < separated.length; i++) {
				if(!bowString.contains(separated[i])) {
					bowString.add(separated[i]);
					if(!allDifferentStringsInSetBOW.contains(separated[i]))allDifferentStringsInSetBOW.add(separated[i]);
				}
			}
			numOfStringsInClass = new int[A18B0311P.myClassesForClassification.size()];
			for(int i = 0; i < A18B0311P.myClassesForClassification.size(); i++) {
				bowResults.add(new double[bowString.size()]);
				for(int j = 0; j < myList.size(); j++) {
					if(myList.get(j).getType().equals(A18B0311P.myClassesForClassification.get(i))){
						numOfStringsInClass[i]+=myList.get(j).getVal().split("[^\\p{L}0-9]+").length;
					}
				}
			}
			//ted mam bow - potrebuji natrenovane vektory trid
			for(int i = 0; i < myList.size(); i++) {//pro kazdy soubor
				String myType = myList.get(i).getType();
				String s = myList.get(i).getVal();
				s.toLowerCase();
				String [] sepS = s.split("[^\\p{L}0-9]+");
				for(int j = 0; j < sepS.length; j++) {//pro kazde slovo
					//zvedni slozku vektoru reprezentovanou danym slovem o jedna ve vektru tridy odpovidajici souboru
					//System.out.println(A18B0311P.myClassesForClassification.indexOf(myType)+" "+bowString.indexOf(sepS[j]));
					bowResults.get(A18B0311P.myClassesForClassification.indexOf(myType))[bowString.indexOf(sepS[j])]+=1;
				}
				int num = numberOfFilesWithClass.get(A18B0311P.myClassesForClassification.indexOf(myType));
				numberOfFilesWithClass.set(A18B0311P.myClassesForClassification.indexOf(myType),num+1);
			}
		}
		else {
			vec = new double[bowString.size()];
			String s = ATstring;
			s.toLowerCase();
			String [] sepS = s.split("[^\\p{L}0-9]+");
			for(int j = 0; j < sepS.length; j++) {//pro kazde slovo
				//zvedni slozku vektoru reprezentovanou danym slovem o jedna ve vektru tridy odpovidajici souboru
				if(bowString.contains(sepS[j]))vec[bowString.indexOf(sepS[j])]+=1;
				if(!bowString.contains(sepS[j]))ATnumOfStringsInClass++;
			}
		}
		
	}
	
	private void ngram() {
		if(!A18B0311P.IAmAfterTraining) {//System.out.println("ngram");
		String myString = A18B0311P.allInOneTrainData;
		myString.toLowerCase();
		String [] separated = myString.split("[^\\p{L}0-9]+");//jednotliva slova
		for(int i = 0; i < separated.length-1; i++) {
			if(!ngramString.contains(separated[i]+" "+separated[i+1])) {
				ngramString.add(separated[i]+" "+separated[i+1]);
				if(!allDifferentStringsInSetNGRAM.contains(separated[i]+" "+separated[i+1]))allDifferentStringsInSetNGRAM.add(separated[i]+" "+separated[i+1]);
			}
		}
		numOfStringsInClass = new int[A18B0311P.myClassesForClassification.size()];
		for(int i = 0; i < A18B0311P.myClassesForClassification.size(); i++) {
			ngramResults.add(new double[ngramString.size()]);
			for(int j = 0; j < myList.size(); j++) {
				if(myList.get(j).getType().equals(A18B0311P.myClassesForClassification.get(i))){
					numOfStringsInClass[i]+=myList.get(j).getVal().split("[^\\p{L}0-9]+").length;
				}
			}
		}
		//ted mam bow - potrebuji natrenovane vektory trid
		
			for(int i = 0; i < myList.size(); i++) {//pro kazdy soubor
				String myType = myList.get(i).getType();
				String s = myList.get(i).getVal();
				s.toLowerCase();
				String [] sepS = s.split("[^\\p{L}0-9]+");
				for(int j = 0; j < sepS.length-1; j++) {//pro kazde slovo (resp dvojici slov)
					String s1 = sepS[j]+" "+sepS[j+1];
					//zvedni slozku vektoru reprezentovanou danym slovem o jedna ve vektru tridy odpovidajici souboru
					if(ngramString.indexOf(s1) == -1)System.out.println(s1);
					else ngramResults.get(A18B0311P.myClassesForClassification.indexOf(myType))[ngramString.indexOf(s1)]+=1;
				}	
				int num = numberOfFilesWithClass.get(A18B0311P.myClassesForClassification.indexOf(myType));
				numberOfFilesWithClass.set(A18B0311P.myClassesForClassification.indexOf(myType),num+1);
			}
		}
		else {//mam testovany text pred zarazenim
			vec = new double[ngramString.size()];
			String s = ATstring;
			s.toLowerCase();
			String [] sepS = s.split("[^\\p{L}0-9]+");
			for(int j = 0; j < sepS.length-1; j++) {//pro kazde slovo
				String s1 = sepS[j]+" "+sepS[j+1];
				//zvedni slozku vektoru reprezentovanou danym slovem o jedna ve vektru tridy odpovidajici souboru
				if(ngramString.contains(s1))vec[ngramString.indexOf(s1)]+=1;
				if(!ngramString.contains(sepS[j]))ATnumOfStringsInClass++;
			}			
		}
	}
}
