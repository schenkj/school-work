cmake_minimum_required(VERSION 3.17)
project(BIT01)

set(CMAKE_CXX_STANDARD 14)

add_executable(BIT01 main.cpp)