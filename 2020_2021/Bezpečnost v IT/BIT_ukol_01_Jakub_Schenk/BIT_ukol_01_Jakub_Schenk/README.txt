Readme k projektu BIT_ukol_01_Jakub_Schenk - program ukazující příklady steganografie

Program funguje v jednom kroku při spuštění se dvěma argumenty. Tyto argumenty jsou cesty k bitmapovým souborům, přičemž prvním z argumentů je obrázek, do kterého zprávu schováváme a druhý je kódovaná zpráva. 

V prvním kroce je vytvořen obrázek merged_image.bmp. Ten je vytvářen jako nový soubor, do kterého nahráváme data. Nejdříve je nahrána hlavička bmp souboru, do kterého je schovávána zpráva. Následně jsou nahrávána obrazová data tohoto souboru, ale poslední dva bity každého bytu jsou zaměněny za část tajné zprávy. Tajnou zprávou je déka tajné zprávy (pro toto číslo je ve zprávě vyhrazeno 8 bytů) a následně její obsah. Zbylý prostor, následující po ukončení tajné zprávy, je vyplněn 0 (tedy každý byte za zprávou v merged_image má 7. a 8. bit rovný 0). 

Dekódování probíhá obráceným způsobem. Je přijmut parametr se souborem (jelikož vše program provádí v jednom kroku, tak toto není podstatné, ale nebylo by náročné rozdělit zakódování a rozkódování) a ten je zpracováván. Nejdříve je ze souboru přeskočena hlavička, pak je načteno 8 bytů délky zprávy a následně samotná zpráva. Ta je po bytech ukládána do nového souboru message.bmp.

Výsledkem celého běhu programu je tedy nový soubor merged_image.bmp (soubor se skrytou zprávou) a message.bmp (samotná zpráva).