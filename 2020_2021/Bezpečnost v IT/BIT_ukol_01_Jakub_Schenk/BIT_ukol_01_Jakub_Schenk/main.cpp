#include <iostream>
#include <fstream>




using namespace std;

int deconstruct(string string);
int merge(string s1, string s2);

int main(int argc, char *argv[]) {
    if(argc!=3){
        cout << "Wrong number of arguments" << endl;
        return 2;
    }
    else{
        if(merge(argv[1], argv[2])==0){
            cout << "Merge operation completed. File merged_image.bmp created" << endl;
            string name = "merged_image.bmp";
            deconstruct(name.c_str());
            cout << "Deconstruct operation completed. File message created." << endl;
            return 0;

        }
        return 1;
    }
}

int merge(string s1, string s2) {
    unsigned char byte, handle;
    char bits [8];
    FILE * hide;
    hide = fopen(s2.c_str(), "rb");
    fseek (hide , 0 , SEEK_END);
    long hideSize = ftell (hide);
    rewind (hide);
    if(hideSize<1){
        cout << "Input file couldnt be opened. Shutting down..." << endl;
        return -1;
    }
    FILE * base;
    base = fopen(s1.c_str(), "rb");
    fseek (base , 0 , SEEK_END);
    long baseSize = ftell (base);
    rewind (base);
    if(baseSize<1){
        cout << "Input file couldnt be opened. Shutting down..." << endl;
        return -1;
    }
    FILE * merge = fopen("merged_image.bmp", "rb+");

    int j=0, k = 0;
    unsigned char message;
    int msgArr [8];
    int sizeOfFS = 0;
    int sizeMessage [8][8];
    int fileSize = hideSize;
    //cout<<fileSize<<endl;
    for(int i = 7; i >=0; i--){
        for(int j = 7; j >=0; j--){
            sizeMessage[i][j]=fileSize%2;
            fileSize/=2;
            //cout << sizeMessage[i][j];
        }
        //cout << " ";
    }//cout<<endl<<sizeMessage[1][0]<<sizeMessage[1][1]<<sizeMessage[1][2]<<endl;
    //cout<<endl;
    while(j<baseSize){
        fread(&handle, sizeof(char),1, base);
        if(j<54){
            //hlavicka
            fwrite(&handle, sizeof(char), 1, merge);
            j++;
            k=0;
            continue;
        }
        if(sizeOfFS<8 && (j-54)%4==0) {
            //j--;
            for(int i = 0; i < 8; i++){
                msgArr[i]=sizeMessage[sizeOfFS][i];
                //cout<<msgArr[i];
            }
            //cout<<"|";
            //cout<<"("<<sizeOfFS<<") ";
            k=0;
            //if(sizeOfFS==7){cout<<endl;}
            sizeOfFS++;
            //cout << "here" <<endl;
        }
        //tady jsem skoncil se zapisem velikosti

        handle-=handle%4;//vynulovani dvou poslednich bitu
        if((j-54)%4==0 && hideSize+8*4+54>j/4 && j>=54+8*4){
            fread(&message, sizeof(char), 1, hide);
            for(int i = 7; i >= 0; i--){
                msgArr[i] = message%2;
                message/=2;
                k=0;
                //v msgArr je ted byte v bitech
            }
        }
        if(j%4==0 && hideSize+8*4+54<=j/4){
            for(int i = 0; i < 8; i++)msgArr[i]=0;
            //cout<<"here2"<<endl;
            k=0;
        }
        handle+=msgArr[k]*2 + msgArr[k+1];
        //if(k>=8)            cout<<"here"<<endl;
        //cout << handle << " <- " << msgArr[k] << " + " << msgArr[k+1] <<endl;
        j++;
        k+=2;
        fwrite(&handle, sizeof(char), 1, merge);
    }
    fclose(merge);
    fclose(hide);
    fclose(base);
}

int deconstruct(string s) {
    unsigned char tmp, byte = 0x0;
    FILE * fin;
    fin = fopen(s.c_str(),"rb+");
    fseek (fin , 0 , SEEK_END);
    long finSize = ftell (fin);
    rewind (fin);
    if(finSize < 1){
        cout << "Input file couldnt be opened. Shutting down..." << endl;
        return -1;
    }
    FILE *code;
    code = fopen("message.bmp", "wb+");
    unsigned char by = 0;
    int i = 0, j = 0, k = 0, cntr=0, sizeCntr = 0;
    unsigned char bytesOfSize [8];
    int64_t sizeOF = 0;
    while (cntr < finSize) {
        if (cntr < 54) {cntr++;fread(&byte, sizeof(char), 1, fin);continue;}//hlavicka bitmapy
        fread(&byte, sizeof(char), 1, fin);
        by += byte%4;
        if(sizeCntr<=7 && k%4==3){
            //cout << (int)by <<endl;
            bytesOfSize[sizeCntr] = by;
            sizeCntr++;
            //cout<<sizeCntr;
            if(sizeCntr==8){
                //mam všechny byty -> prevod do int
                //cout<<endl;
                for(int i =0; i < 8; i++){
                    //tady mam jednotlive byty a potřebuji je postupně dostat do intu
                    //cout<<sizeOF<<" + "<<(int)bytesOfSize[i]<<endl;
                    sizeOF*=256;
                    sizeOF+=bytesOfSize[i];
                    k=-1;
                }
            }
            by=0;
        }
        if(k%4==3 && sizeCntr>7 && cntr/4<54+8*4+sizeOF){
            //cout << by << endl;
            fwrite(&by, sizeof(char), 1, code);
            by = 0;
        }
        k++;
        //posun bity o 2 mista
        by *= 4;
        if(cntr>sizeOF*4+54+8*4 && cntr>86)break;
        cntr++;
    }
    //cout <<sizeOF <<endl;
    fclose(code);
    fclose(fin);
}
