Readme k projektu BIT_ukol_02_Jakub_Schenk - program dešifrující několik různých druhů šifer pomocí frekvenční analýzy

Program dle zadání ke spuštění vyžaduje parametr, který obsahuje cestu k souboru s šiframa. Toto je jediný parametr, který je programem vyžadován. 
Program otestuje první z šifer na césarovu šifru. Ze všech iterací si uloží  zapíše tu, ve které najde nejvíce bigramů, které jsou používány v běžné řeči. Tento výsledek následně zapíše. 

Druhou z šifer by měl otestovat na substituční šifru a třetí na monoalfabetickou. Bohužel jsem nezvládl program naimplementovat tak, aby mi vracel rozumné výsledky, a jelikož byla tento týden zveřejněna další samostatná práce, tak jsem se rozhodl tuto odeslat v nedokončeném stavu a zaměřit se na práci další. 
Výstupem programu je soubor result.txt ve formátu, který byl specifikován v zadání, s tím rozdílem, že pro druhý a třetí výsledek neprobíhá žádný podstatný výpočet a do výsledného souboru tyto výsledky zapisovány nejsou. 

Teoreticky by měl program pro druhou šifru vyzkoušet abecedu, kteou by získal následujícím způsobem: Bigramy a trigramy, které se vyskytly v šifře by byly namapovány na bigramy a trigramy, které jsou nejčastěji používané v angličtině. Ostatní znaky by byli vyzoušeny buď náhodně, nebo všechny (podle toho, kolik znaků by bylo neznámých).

Ke třetí šifře jsem se nestihl dostat, takže po analýze a napočítání bigramů a trigramů bych jí musel lépe nastudovat.

Omlouvám se, že odesílám nehotový projekt, ale po mnoha hodinách práce by mi bylo líto jej ani neodevzdat.