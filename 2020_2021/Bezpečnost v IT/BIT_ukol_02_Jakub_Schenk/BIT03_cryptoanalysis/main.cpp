#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <array>
#include <fstream>

using namespace std;



void countLetters(int a, string basicString);

void tryCesar(string basicString, int a, int key = 0);

int substringCounter(string &substr, string &str1);

void trySubstitution(string c, int a);

void changeAlphabeth(int a);

void countBigrams(string c);

int c1Numbers [26];
int c2Numbers [26];
int c3Numbers [26];
char bigramsCip [50][2];
string res1, res2, res3;
string bigrams [] = {"th", "he", "in", "en", "nt", "re", "er", "an", "ti", "as", "on", "at", "se", "nd"};
string trigrams [] = {"the", "and", "tha", "ent", "ing", "ion", "tio", "for", "nde", "has", "nce", "edt", "tis"};
char alphabethFreqSort [] = {'e', 't', 'a', 'o', 'i', 'n', 's', 'h', 'r', 'd', 'l', 'c', 'u', 'm', 'w', 'f', 'g', 'y', 'p', 'b', 'v', 'k', 'j', 'x', 'q', 'z'};
char alphabeth[26];
int main(int argc, char *argv[]) {
    if(argc!=2){
        cout << "Wrong number of arguments" << endl;
        return 2;
    }
    string s1 = argv[1];
    //string s1 = "C:\\Users\\jakub\\OneDrive\\Plocha\\cv4_zadani_cvrtek\\sifra1.txt";
    FILE *f = fopen(s1.c_str(), "rb");
    fseek(f, 0, SEEK_END);
    long fsize = ftell(f);
    fseek(f, 0, SEEK_SET);  /* same as rewind(f); */
    if(fsize<2){cout<<"couldnt find the file"<<endl;return 3;}
    memset(c1Numbers, 0, 26*sizeof(int));
    memset(c2Numbers, 0, 26*sizeof(int));
    memset(c3Numbers, 0, 26*sizeof(int));

    char content [fsize];// = malloc(fsize + 1);
    fread(content, sizeof(char), fsize, f);
    fclose(f);
    int i = -1;
    string c1, c2, c3;
    while(true){
        i++;
        if(content[i]=='#')break;
        c1+=content[i];
    }while(true){
        i++;
        if(content[i]=='#')break;
        c2+=content[i];
    }/*while(true){
        i++;
        if(content[i]=='#')break;
        c3+=content[i];
    }*/

    ///crack caesar--------------------------------------------------------------------------------
    countLetters(1,c1);
    int prevMax = c1.size();
    int maxIndex = -1;
    bool breaker = false;
    string best;
    int bestCounter = 0;
    //cout<<"here ";
    int bestKey;
    for(int j = 0; j < 26; j++){
        //cout<<"here "<<j;
        int counter = 0;
        int max = -1;

        for(int i = 0; i < 26; i++){
            if(c1Numbers[i]<prevMax && c1Numbers[i]>max){
                max = c1Numbers[i];
                maxIndex = i;
            }
        }
        prevMax = max;
        //cout<<"maxIndex: "<<maxIndex<<endl;
        tryCesar(c1, 1, maxIndex);
        for(int i = 0; i < bigrams->length(); i++){
            counter+=substringCounter(bigrams[i], res1);
        }
        if(counter>bestCounter){
            bestCounter = counter;
            best = res1;
            bestKey=maxIndex;
        }
    }
    cout<<"best match for caesar cipher: \n"<<best<<"\nKey: "<<bestKey;
    //zapis vysledku

    string endline = "\n#\n";
    ofstream res ("result.txt");
    if (res.is_open())
    {
        res << best;
        res << endline;
        res<<"Cryptanalysis failed!";
        res<<endline;
        res<<"Cryptanalysis failed!";
        res.close();
    }
    else cout << "Unable to open file";

    //za tímto returnem jsou výpočty, které jsou zbatečné, protože projekt není dokončený, z tohoto důvodu jsem se rozhodl program ukončit už zde
    return 0;

    ///crack substitution---------------------------------------------------------------
    //cout<<endl<<"---------------------------------------------------------------"<<endl;
    countLetters(2,c2);
    countBigrams(c2);
    //countTrigrams(c2);
    //for(int i = 0; i < 26; i++) cout<<c2Numbers[i]<<" ";
    //cout<<endl;

    bestCounter = 0;
    //zkus několik pravděpodobných abeced a kdyžtak to pak vzdej
    //do res uloz svuj vysledek
    trySubstitution(c2,2);
    for(int j = 0; j < 3; j++){
        //porovnej res nejdrive substitution podle znaku, pak podle bigramu a pak podle trigramu a ziskej nejlepsi
        int counter = 0;
        for(int i = 0; i < bigrams->length(); i++){
            counter+=substringCounter(bigrams[i], res2);
        }
        for(int i = 0; i < trigrams->length(); i++){
            counter+=substringCounter(trigrams[i], res2);
        }
        if(counter>bestCounter){
            bestCounter = counter;
            best = res1;
        }
        if(j==0)changeAlphabeth(2);//try change with bigrams
        else if(j==1)changeAlphabeth(3);//try change with trigrams
    }
    //cout<<"best match for substitution cipher: \n"<<best;
  /*  best = "Cryptanalysis failed!";
    cout<<best<<endl;
    fwrite(&best, sizeof(best), 1, res);
    fwrite(&endline, sizeof(endline), 1, res);
    cout<<best<<endl;
    fwrite(&best, sizeof(best), 1, res);
    fclose(res);*/
    return 0;
}

void countBigrams(string cip) {
    string c;
    //cout<<"counting bigrams"<<endl;
    char bigramsA[cip.size()-1][2];
    int bigramsCount[cip.size()-1];
    //reset poli
    for(int i = 0; i < cip.size()-1; i++){
        bigramsA[i][0]=' ';
        bigramsA[i][1]=' ';
        bigramsCount[i]=0;
        //odfiltrovat mezery
        if(cip[i]!=' ' && cip[i]!='\n')c+=cip[i];
    }
    char tmp[2];
    //pro kazdou dvojici znaku zkontroluj jestli je nemas v databazi, kdyztak zvedni counter, nebo zaved novy prvek
    for(int i = 1; i < c.size(); i++){
        tmp[0] = (char)c[i-1];
        tmp[1]=(char)c[i];
        //cout<<"tmp: "<<tmp[0]<<tmp[1]<<" ";
        for(int j = 0; j < c.size()-1; j++){
            if(bigramsA[j][0]==tmp[0] && bigramsA[j][1]==tmp[1]){bigramsCount[j]++;break;}
            else if(bigramsA[j][0]==' ' || bigramsA[j][1]==' '){
                bigramsA[j][0]=tmp[0];
                bigramsA[j][1]=tmp[1];
                bigramsCount[j]++;
                break;
            }
        }
    }
    //cout<<"here"<<endl;
    //vypis
    /*for(int i = 0; i < cip.size()-1; i++){
        if(bigrams[i][0]==' ')break;
        cout<<bigrams[i][0]<<bigrams[i][1]<<"("<<bigramsCount[i]<<") ";
    }*/
    int maxAllowed = c.size();
    for(int j = 0; j < min((unsigned int)50,c.size()); j++){
        int max = 0;
        int maxIndex = -1;
        for(int i = 0; i < c.size(); i++){
            //najdi nejastejsi bigramy a zapis je
            if(bigramsA[i][0]==' ' || bigramsA[i][1]==' ' )break;
            if(max<bigramsCount[i] && bigramsCount[i]<maxAllowed){
                //cout<<max<<"<"<<bigramsCount[i]<<"&&"<<bigramsCount[i]<<"<"<<maxAllowed<<endl;
                max = bigramsCount[i];
                maxIndex = i;
            }
        }
        maxAllowed = max;
        //cout<<"here2"<<endl;
        if(maxAllowed <=2)break;
        bigramsCip[j][0] = bigramsA[maxIndex][0];
        bigramsCip[j][1] = bigramsA[maxIndex][1];

        //cout<<bigramsCip[j]<<"<="<<bigramsA[maxIndex][0]<<bigramsA[maxIndex][1]<<"("<<maxIndex<<")-"<<maxAllowed<<endl;
        //cout<<"here3"<<endl;
    }

    //cout<<"bigrams counting done"<<endl;
}

void changeAlphabeth(int a) {
    //cout<<"Change alphabeth: "<<a<<endl;
    if(a==2){
        memset(&alphabeth, 0, 26 * sizeof(bool));
        //change by bigrams
        if(bigramsCip[0][0]==bigramsCip[1][1]){
            //index je pismeno realne abecedy a hodnota je obraz do sifrovane abecedy
            alphabeth['h' - 97]=bigramsCip[1][1];
            alphabeth['t' - 97]=bigramsCip[1][0];
            alphabeth['e' - 97]=bigramsCip[0][1];
        }
        //preskocit th a he
        bool change=true;
        //dokud neznam celou abecedu, hledam nova pismenka
        int counter = 0;
        while(change){
            counter++;
            if(counter>50)break;
            //cout<<"baf1"<<endl;
            change = false;
            //pro kazde pismeno, ktere jeste neznam
            for(int i = 0; i < 26; i++){
                if(alphabeth[i] != 0)continue;//pokud uz k pismenu znam obraz, preskocim jej
                //mam pismeno ke kteremu neznam obraz
                //podivam se do bigramu, jestli v nich je
                for(int j = 0; j < 14/*bigrams.size()*/; j++){
                    //cout<<"777"<<bigrams->size();
                    //pokud je toto pismeno obsazene v bigramu (realne) && znam obraz druheho pismene z bigramu
                    //cout<<j<<": "<<bigrams[j][0]<<bigrams[j][1]<<"|";
                    if(bigrams[j][0]==i+97 && alphabeth[bigrams[j][1] - 97]!=0){
                        //cout<<"888";
                        //pismeno je na prvni pozici tohoto bigramu && uz znam obraz druheho pismene z tohoto bigramu
                        //najdi obraz bigramu ve kterem se nachazi moje zname pismeno na druhem miste a na prvnim miste bude tedy hledany obraz k memu zkoumanemu pismenu
                        for(int k = 0; k < 50/*bigramCip.size()*/; k++){
                            if(bigramsCip[k][1]==alphabeth[bigrams[j][1] - 97]){
                                alphabeth[i]=bigramsCip[k][0];
                                //cout<<"baf0";
                                change=true;
                                break;
                            }
                        }
                    }
                    else if(bigrams[j][1]==i+97 && alphabeth[bigrams[j][0] - 97]!=0){
                        //cout<<"999";
                        //pismeno je na druhe pozici tohoto bigramu && uz znam obraz prvniho pismene z tohoto bigramu
                        //najdi obraz bigramu ve kterem se nachazi moje zname pismeno na prvnim miste a na druhem miste bude tedy hledany obraz k memu zkoumanemu pismenu
                        for(int k = 0; k < 50/*bigramCip.size()*/; k++){
                            if(bigramsCip[k][0]==alphabeth[bigrams[j][0] - 97]){
                                alphabeth[i]=bigramsCip[k][1];
                                //cout<<"baf1";
                                change=true;
                                break;
                            }
                        }
                    }
                }
                if(alphabeth[i]==0)change=true;
            }

        }
        //cout<<"baf2"<<endl;
        for(int i = 0; i < 26; i++){
            //cout<<(char)(alphabeth[i]);
        }
        //cout<<"here"<<bigramsCip[0]<<bigramsCip[1]<<endl;
    }
    else if(a==3){
        //change by trigrams
    }
    else{
        //try combinations and random
    }
}

void trySubstitution(string c, int a) {
    bool forbiden [26];
    memset(forbiden, 0, 26);
    string res = ""+c;
    //pro kazdy znak
    for(int i = 0; i < 26; i++){
        int max=-1;
        int index=-1;
        //najdi nejcastejsi znak (krom tech nalezenych uz drive)
        for(int j = 0; j < 26; j++){
            if(forbiden[j]){continue;}
            if(max < c2Numbers[j]){
                max = c2Numbers[j];
                index = j;
            }
        }
        //cout<<index<<" ";
        forbiden[index]=true;
        //cout<<i<<" => "<<index<<"("<<max<<")"<<"\t||\t"<<(char)index+97<<"->"<<alphabethFreqSort[i]<<endl;
        for(int j = 0; j < c.size(); j++){
            if(c[j]==index+97)res[j]=alphabethFreqSort[i];
        }
    }
    //cout<<c<<endl;
    //cout<<"--------------------------------------------"<<endl;
    //cout<<res<<endl;
    res2=res;
}

void tryCesar(string c, int a, int key) {
    if(a==1)res1="";
    else if(a==2)res2="";
    else if(a==3)res3="";
    int max = -1;
    int maxIndex = -1;
    int * cNmbrs;
    if(a==1) cNmbrs = c1Numbers;
    else if(a==2) cNmbrs = c2Numbers;
    else if(a==3) cNmbrs = c3Numbers;
    if(key != 0)maxIndex = key;
    else{
        for(int i = 0; i < 26; i++){
            //cout<<cNmbrs[i]<<" ";
            if(max<cNmbrs[i]){
                max = cNmbrs[i];
                maxIndex = i;
            }
        }
        //cout<<"No key used -> found most fit (probably): "<<maxIndex<<" and used it insted."<<endl;
    }
    //cout<<endl<<"key: "<<maxIndex<<endl;
    //ziskani informace o kolik je asi posun proveden
    int posun = maxIndex-4;
    //cout<<posun<<endl;
    char tmp;
    for(int i = 0; i < c.size(); i++){
        tmp = c[i];
        if(tmp==' ' || tmp=='\n')continue;
        //cout<<c[i]<<" ";
        tmp -= posun;
        if(tmp<97)tmp+=26;
        if(tmp>122)tmp-=26;
        if(a==1)res1+=tmp;
        else if(a==2)res2+=tmp;
        else if(a==3)res3+=tmp;
    }
    /*if(a==1)cout<<"result:"<<endl<<res1<<endl;
    else if(a==2)cout<<"result:"<<endl<<res2<<endl;
    else if(a==3)cout<<"result:"<<endl<<res3<<endl;*/
}

void countLetters(int a, string c) {
    //cout<<endl;
    //cout<<endl<<c.size()<<endl;
    for(int i = 0; i < c.size()-2; i++){
        //cout<<c[i]<<"("<<i<<") ";
        if(c[i]==' ' || c[i]=='\n')continue;

        if(a==1)c1Numbers[c[i]-97]++;
        else if(a==2) c2Numbers[c[i]-97]++;
        else c3Numbers[c[i]-97]++;
        //cout<<c1Numbers[0]<<" ";
    }
    //cout<<endl;
}
int substringCounter(string &substr, string &str1){
    int a = substr.length();
    int b = str1.length();
    int res = 0;

    /* A loop to slide substr[] one by one */
    for (int i = 0; i <= b - a; i++){
        /* For current index i, check for
           pattern match */
        int j;
        for (j = 0; j < a; j++){
            if (str1[i + j] != substr[j])break;
        }
        // if substr[0...a-1] = str1[i, i+1, ...i+a-1]
        if (j == a){
            res++;
            j = 0;
        }
    }
    return res;
}


