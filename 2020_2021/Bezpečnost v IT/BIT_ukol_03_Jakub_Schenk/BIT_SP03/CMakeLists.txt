cmake_minimum_required(VERSION 3.17)
project(BIT_SP03)

set(CMAKE_CXX_STANDARD 14)

add_executable(BIT_SP03 main.cpp)