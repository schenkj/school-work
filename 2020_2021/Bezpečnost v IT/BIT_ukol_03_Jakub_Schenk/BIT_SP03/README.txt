Readme k projektu BIT_ukol_03_Jakub_Schenk - program, který zakóduje a následně dekóduje input pomocí Fiestelovi metody šifrování

Program funguje v jednom kroku při spuštění se dvěma argumenty. Tyto argumenty jsou cesty k textovým souborům, přičemž prvním z argumentů je soubor se zprávou k zakódování a  druhý je soubor obsahující klíče v předem daném formátu (kY=XXXX, kde Y je číslo klíče a X je binární hodnota). 

V prvním kroce jsou načtena data jak zprávy, tak klíčů. Zpráva je uložena jako pole charů, takže je rovnou rozdělena na bloky ve kterých bude šifrována. Z klíčů jsou uloženy jen jejich hodnoty a to také do pole. 

Druhým krokem je zakódování zprávy. Kóduje se tolikrát kolik je programu poskytnuto klíčů (pokaždé s jiným klíčem v pořadí v jakém byli zapsány v souboru). Výsledek je po těchto iteracích následně uložen jako dvojice hexadecimálních čísel. Pokud takto přeložíme celou zprávu, získáme pro každý znak zprávy dvě hexadecimální čísla a první z výsledků pro soubor output.txt, který bude vytvořen na samotném konci. 

Ve třetím kroce je zakódovaná zpráva přepsána opět do znaků normální abecedy (ovšem je nečitelná a tyto znaky nejsou většinou ani alfanumerické). Tuto zprávu společně s klíči, které jsou v opačném pořadí opět nechám zakódovat stejným způsobem jako zprávu původní k druhém kroce. Výsledkem je opět dvojice hexadecimálních čísel pro každý znak. Celá zpráva takto přepsaná je druhým výsledkem pro soubor output.txt.

Ve čtvrtém kroce je tato zpráva opět přepsána do znaků běžné abecedy. Zpráva, která je takto dekódována, je identická se zprávou, která byla programu poskytnuta v podobě prvního argumentu. Tuto zpávu ukládám jako poslední výsledek pro soubor output.txt.

Posledním krokem zapíšu parciální výsledky v požadovaném formátu. Tyto výsledky jsou uloženy do souboru output.txt ve složce, ze které byl program spuštěn.

Výsledkem celého běhu programu je tedy nový soubor output.txt.

Bohužel se mi nepodařilo dokončit druhý z úkolů, zakódování bitového souboru. Ze mě neznámého důvodu nejsem schopen soubor načíst do proměnné a následně ho zpracovat jako krátkou textovou zprávu, takže pokud se uživatel pokusí programu podstrčit soubor, který nemá koncovku .txt, tak je sice vytvořen soubor s koncovkou sejného typu jako poskytnutý vstupní soubor, ale nepodařilo se mi jej otevřít.