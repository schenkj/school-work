#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <fstream>

using namespace std;

void code(string input, string keys[], int numberOfKeys);

void encode(int block[8], string keys[], int numberOfKeys);

void iteration(int R[4], string key,int L[4]);

int R1[4];
int block [8];
vector<string>myResult;
vector<int>myResultBinary;
string toFile [3];


int main(int argc, char *argv[]) {
    if (argc != 3) {cout << "Wrong number of arguments.";return 2;}
    FILE *in = fopen(argv[1], "rb+");
    FILE *key = fopen(argv[2], "rb+");
    fseek(in, 0, SEEK_END);
    long fsize = ftell(in);
    fseek(in, 0, SEEK_SET);  /* same as rewind(in); */
    fseek(key, 0, SEEK_END);
    long keySize = ftell(key);
    fseek(key, 0, SEEK_SET);  /* same as rewind(in); */
    if(keySize == 0 || fsize == 0){cout<<"File not found.";return 3;}
    ////////////////////////////////////////////////////////////////////////read files

    //keys
    char keyVal [fsize+1];
    for(int i = 0; i < fsize+1; i++)keyVal[i]='\0';
    fread(keyVal, 1, fsize, key);
    fclose(key);
    //vypis
    int keyCounter = 0;
    int wholeKeySize = 0;
    for(int i = 0; i < keySize; i++){
        //cout <<"\'"<<keyVal[i]<<"\'";
        if(keyVal[i]=='\n'){
            keyCounter++;
            if(wholeKeySize==0)wholeKeySize=i;
        }

    }
    if(keyVal[keySize]!='\n')keyCounter++;
    string keysString = keyVal;
    char keyArr [keyCounter][wholeKeySize];
    int charCounter = 0;
    int arrCounter = 0;

    //cout<<keysString<<endl;
    for(int i = 0; i < keysString.size(); i++){
        if(keysString[i]=='\n'){
            arrCounter++;
            charCounter=0;
            //cout<<keyArr[arrCounter-1]<<endl;
        }
        else {
            keyArr[arrCounter][charCounter]=keysString[i];
            //cout<<keyArr[arrCounter]<<"<="<<keysString[i]<<endl;
            charCounter++;
        }
    }
    //cout<<"whole key size: "<<wholeKeySize<<endl;
    string keys [keyCounter];
    string tmp;
    tmp = keyArr[0];
    //cout<<"keyCounter: "<<keyCounter<<endl;
    for(int i = 0; i < keyCounter; i++){
        //cout<<"here"<<i;
        //cout<<"-------------"<<endl<<(i*wholeKeySize)+4<<" - "<<((i+1)*wholeKeySize)-1<<endl;
        keys[i]= tmp.substr((i*wholeKeySize)+3, wholeKeySize-4);
        //cout<<" - key: "<<keys[i]<<endl;
    }
    //cout<<"keys->size(): "<<keys[5]<<endl;

    ///////////////////////////////////////////////////////input


    char input [fsize + 1];
    for(int i = 0; i < fsize+1; i++)input[i]='\0';
    fread(&input, 1, fsize, in);
    fclose(in);
    //cout<<fsize<<" "<<input;

    ////////////////////////////////////////////////////////////////////////////////////code
    //cout<<keys<<" "<<keyCounter<<endl;
    code(input, keys, keyCounter);

    //vypisani zakodovaneho vysledku jako hex
    string res1 = "";
    for (string n : myResult)res1 = res1+n+" ";
    //cout<<endl<<fsize<<"baf baf"<<endl<<input<<endl<<"start of hidden hex"<<res1<<endl<<"end of hidden hex"<<endl;
    toFile[0]=res1;
    string inputCipher = "";
    for(char c : myResultBinary)inputCipher = inputCipher+c;
    //inverse key
    string inverseKeys [keyCounter];
    for(int i = 0, j = keyCounter-1; i < keyCounter; i++, j--)inverseKeys[i]=keys[j];
    //vynulovani myResult a myResultBinary
    myResult.clear();
    myResultBinary.clear();
    //decode
    code(inputCipher, inverseKeys, keyCounter);
    string res2 = "";
    for (string n : myResult)res2 = res2+n+" ";
    toFile[1]=res2;
    inputCipher = "";
    for(char c : myResultBinary)inputCipher = inputCipher+c;
    toFile[2]=inputCipher;
    //zapsani do file
    ofstream out;
    //cout<<toFile[2];
    out.open ("output.txt");
    out << toFile[0] <<"\n"<< toFile[1] <<"\n"<< toFile[2];
    out.close();

    string nm = argv[1];
    if(nm[nm.size()-1]!='t' || nm[nm.size()-2]!='x' || nm[nm.size()-3]!='t'){
        ofstream out1;
        string nm1 = "";
        nm1 = nm1+"output."+nm[nm.size()-3]+nm[nm.size()-2]+nm[nm.size()-1];
        out1.open (nm1);
        out1 <<toFile[2];
        out1.close();
    }
}

void code(string input, string keys[], int numberOfKeys){
    //pro vsechny znaky ve zprave
    for(int i = 0; i < input.size(); i++){
        //cout<<(int)input[i]<<" ";
        //blok k zakodovani (char bitove) ---- jeden char je jeden blok k zakodovani
        for(int j = 7, k = 0; j >=0; j--, k++)
        {
            block[k]=((int)input[i] >> j) & 1;
            //cout << (((int)input[i] >> j) & 1 );
        }
        //cout<<endl;
        encode(block, keys, numberOfKeys);
        //vypsani puvodniho znaku jako hexa cisla
        int first = 8*block[0] + 4*block[1] + 2*block[2] + block[3];
        int sec = 8*block[4] + 4*block[5] + 2*block[6] + block[7];
        char st, nd;
        if(first==10)st='a';
        else if(first==11)st='b';
        else if(first==12)st='c';
        else if(first==13)st='d';
        else if(first==14)st='e';
        else if(first==15)st='f';
        else st=(char)first+48;
        if(sec==10)nd='a';
        else if(sec==11)nd='b';
        else if(sec==12)nd='c';
        else if(sec==13)nd='d';
        else if(sec==14)nd='e';
        else if(sec==15)nd='f';
        else nd=(char)sec+48;
        //cout<<"("<<st<<nd<<")"<<" ";
    }
}

void encode(int block[8], string keys[], int numberOfKeys) {
    //urceni zakladu
    int L [4]= {block[0],block[1],block[2], block[3]};
    int R [4]= {block[4],block[5],block[6], block[7]};
    int L1 [4];

    //cout<<keys->size()<<endl;
    for(int i = 0; i < numberOfKeys; i++){
        //nove L (L1) = stare R
        for(int j = 0; j < 4; j++)L1[j]=R[j];
        //nove R (R1) vypocitej v nasledujici fci (je globalni)
        iteration(R,keys[i],L);
        //cout<<"-----------------------------------------------"<<endl;
        //nastaveni L a R pro dalsi iteraci
        for(int j = 0; j < 4; j++){
            L[j] = L1[j];
            R[j] = R1[j];
        }
    }

    int res[8] = {R[0], R[1], R[2], R[3], L[0], L[1], L[2], L[3]};
    //cout<<"res: "<<res[0]<<res[1]<<res[2]<<res[3]<<res[4]<<res[5]<<res[6]<<res[7]<<" = ";
    int first = 8*R[0] + 4*R[1] + 2*R[2] + R[3];
    int sec = 8*L[0] + 4*L[1] + 2*L[2] + L[3];
    char st, nd;
    if(first==10)st='a';
    else if(first==11)st='b';
    else if(first==12)st='c';
    else if(first==13)st='d';
    else if(first==14)st='e';
    else if(first==15)st='f';
    else st=(char)first+48;
    if(sec==10)nd='a';
    else if(sec==11)nd='b';
    else if(sec==12)nd='c';
    else if(sec==13)nd='d';
    else if(sec==14)nd='e';
    else if(sec==15)nd='f';
    else nd=(char)sec+48;
    //cout<<st<<nd<<"";
    char a [2] = {st,nd};
    //cout<<"st nd: "<<st<<" "<<nd;
    string b = "";
    b = b+st+nd;
    myResult.push_back(b);
    int c = 128*R[0] + 64*R[1] + 32*R[2] + 16*R[3] + 8*R[4] + 4*R[5] + 2*R[6] + R[7];
    myResultBinary.push_back(c);
}

void iteration(int R[4], string key, int L[4]) {
    int nKey [4];
    //vytvoreni negace klice
    for(int i = 0; i < key.size(); i++){
        if(key[i]=='1')nKey[i]=0;
        else nKey[i]=1;
    }

    //Ri = ((Ri-1 xor Ki)and nKey)xor Li-1 ------> R1 = ((R xor key)and nKey) xor L
    //prvni zavorka R xor key
    int xo[4];
    //cout<<R[0]<<R[1]<<R[2]<<R[3]<<" xor "<<key[0]<<key[1]<<key[2]<<key[3]<<" = ";
    for(int i = 0; i < key.size(); i++){
        if((char)R[i]==key[i])xo[i]=0;
        else xo[i]=1;
        //cout<<xo[i];
    }
    //druha zavorka xo and nKey
    //cout<<" and "<<nKey[0]<<nKey[1]<<nKey[2]<<nKey[3]<<" = ";
    int an[4];
    for(int i = 0; i < key.size(); i++){
        if(xo[i]==1 && nKey[i]==1)an[i]=1;
        else an[i]=0;
        //cout<<an[i];
    }
    //cout<<" xor "<<L[0]<<L[1]<<L[2]<<L[3]<<" = ";
    //treti zavorka an xor L
    //na konci se to nahrava do nove iterace R
    for(int i = 0; i < 4; i++){
        if(an[i] == L[i])R1[i]=0;
        else R1[i]=1;
        //cout<<R1[i];
    }
    //cout<<endl;

}
