Readme k projektu BIT_ukol_01_Jakub_Schenk - program šifrující asymetricky pomocí Zavazadlového algoritmu

Program funguje v jednom kroce a je spouštěn se třemi argumenty. Prvním je text, který by měl být zašifrován. Druhým je parametr "p" a třetím je parametr "q". Obě tyto čísla musí být nezáporná celá čísla podléhající některým podmínkám, které jsou známé v souvislosti s tímto algoritmem a jsou v průběhu výpočtu kontrolovány. Pokud nejsou splněny je program s příslušnou chybovou hláškou ukončen. 

Nejprve je zkontrolován vstup od uživatele. Následuje kontrola parametrů "p" a "q" a existence obdrženého souboru. Podle hodnoty proměnné přímo v programu je načten soukromý klíč. Tento parametr není zadáván uživatelem, aby výsledek odpovídal zadání. Pokud by měla být změněna cesta k souboru se soukromým klíčem, musela by být změnena přímo v kódu programu.
Dalším krokem je kontrola klíče a vygenerování veřejného klíče. Výsledek toho je zapsán do souboru public_key.txt, stejně jako tomu je v zadání. V tomto kroce je také získávána hodnota p^-1, která je potřebná pro dekódování zašifrované zprávy. Tuto hodnotu získávám pomocí brute force, tedy není implementován rozšířený Eukleidův algoritmus. 
Následuje zakódování a dekódování zprávy. Obě akce jsou vypsány do konzole v upraveném formátu, tedy celočíselné hodnoty oddělené čárkou. 
Posledním krokem je zapsání těchto hodnot stejným způsobem do souboru output.txt Po zapsání těchto dvou řádků hodnot je vypsán ještě výsledek přeložený do textové čitelné podoby.

Bohužel se mi nedaří zašifrovat a následně úspěšně dešifrovat větší .bmp soubor. Program úspěšně proběhne a vytvoří výsledek, ale ten je poškozený a neodpovídá původnímu vstupu. Rád bych znal něčí názor na to, proč tomu tak je, protože jsem na nic osobně nepřišel. Pokud je tedy programu poskytnut k zašifrování soubor s jinou koncovkou než .txt, je krom souboru output.txt vytvořen ještě soubor knapsack_output."koncovka", kdy koncovka odpovídá koncovce poskytnutého souboru. 

Závěrem	- Není implementovaný rozšířený eukleidův algoritmus, ale je využito brute force.
	- Soubor .bmp, který byl poskytnut k testování, je zašifrován a dešifrován, ale dojde k jeho poškození.
	- Výsledky jsou vypsány na konzoli a zapsány do souboru output.txt a public_key.txt, případně knapsack_output.txt

Pozn.
Raději poskytuji celou složku, ve které byl program vytvářen, kvůli možným problémům s překladem. To je způsobeno zvoleným programovacím jazykem C++.