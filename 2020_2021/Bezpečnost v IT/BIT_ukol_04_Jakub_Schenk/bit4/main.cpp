#include <iostream>
#include <fstream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <math.h>
#include <vector>


using namespace std;
string privateKeyFile = "private_key_2/private_key.txt";
int * publicKey;
int * privateKeyInt;
int * translated;
string decoded;
int keySize = 0;
int blockSize = 0;
int p;
int q;
int p1;
ofstream out;
ofstream out2;

bool getPublicKey(string privateKey1){
    ifstream file;
    file.open(privateKey1);
    char ch;
    string privateKeyString;
    int ln = 0;
    while (file.get(ch)){privateKeyString = privateKeyString+ch;ln++;}
    file.close();
    cout<<"private: ";
    cout<<privateKeyString<<endl;
    int * pk = new int [ln];
    int j = 0, i;
    for(i = 0; i<ln;i++)pk[i]=0;
    for(i = 0; privateKeyString[i]!='\0'; i++){
        if(privateKeyString[i]==',')j++;
        else pk[j]= pk[j] * 10 + (privateKeyString[i] - 48);
    }
    int pkSize = j+1;
    int * privateKey = new int[pkSize];
    publicKey = new int[pkSize];
    privateKeyInt = new int[pkSize];
    int sum=0;
    for(i = 0; i < ln; i++){
        if(pk[i]!=0 && pk[i]<sum)return false;
        sum+=pk[i];
        if(i<=j)privateKey[i]=pk[i];
        else break;
        //cout<< pk[i]<<", ("<<i<<")";
    }
    //cout<<"1";
    if(q<=sum)return false;
    //kontrola společných dělitelů
    int tmp = (int)(p/2+1);
    //cout<<"2";
    for(i = 2; i < tmp; i++){
        if((p%i)==0){//cout<<p%i<<" "<<q%i<<"|";
            if((q%i)==0)return false;
        }
    }
    //cout<<"3";
    if(q%p==0)return false;
    //cout<<"4";
    //vypocet verejneho klice
    int * publicKey1 = new int[pkSize];
    string toFile = "";
    string intTmp;
    stringstream ss;
    cout<<"public: ";
    for(int i = 0; i < pkSize; i++){
        publicKey1[i] = (privateKey[i]*p)%q;
        cout<<publicKey1[i]<<",";
        publicKey[i]=publicKey1[i];
        ss<<publicKey1[i];
        ss>>intTmp;
        ss.clear();
        //cout<<intTmp;
        //string a = std::to_string(20);
        if(i!=pkSize-1)toFile=toFile + intTmp +",";
        else toFile=toFile + intTmp;
    }
    //cout<<endl<<"here"<<endl;
    keySize = pkSize;
    //publicKey = publicKey1;
    for(int i = 0; i < keySize; i++){
        //publicKey[i]=publicKey1[i];
        //cout<<publicKey[i]<<" ";
    }
    cout<<"\np="<<p<<" q="<<q<<" keySize="<<keySize<<" p^-1="<<p1<<endl<<endl;

    privateKeyInt=privateKey;


    //cout<<keySize<<endl;

    ofstream out1;
    //cout<<toFile[2];
    out1.open ("public_key.txt");
    out1 << toFile;
    out1.close();
    //cout<<endl<<sum<<endl;

    return true;
}

int iter(char block []){
    char tmp;
    int * bits1 = new int [8];
    int * bits = new int [8*blockSize];
    for(int i = 0; i < blockSize*8; i++)bits[i]=0;
    for(int i = 0; i < 8; i++)bits1[i]=0;
    int cntr = 0;
    for(int i = 0; i < blockSize; i++){
        tmp = block[i];
        for(int j = 7, k=0; j >= 0; j--,k++){
            bits1[k] = (tmp >> j) & 1;
            //cout<<bits1[k];
        }
        //cout<<" (";
        for(int l = cntr; l < cntr+8; l++) {
            bits[l] = bits1[l - cntr];
        }
        cntr+=8;
    }
    //cout<<") ----->";
    int ret = 0;
    for(int i = 0; i < blockSize*8; i++) {
        if(bits[i]==1){ret+=publicKey[i];/*cout<<publicKey[i]<<" ";*/}
    }
    //cout<<endl;
    return ret;
}

char * decode(int code, int it){
    //cout<<"--------------------------------------------------------------------------"<<endl;
    //cout<<"\""<<code<<"\"("<<it<<")"<<endl;
    char * res = new char[blockSize];
    int holder = code*p1%q;
    translated[it]=holder;
    //cout<<holder<<" ";
    int * block = new int [blockSize*8];
    for(int i = blockSize*8 -1; i >=0; i--){
        if(holder>=privateKeyInt[i]){
            holder-=privateKeyInt[i];
            block[i]=1;
        }
        else block[i]=0;
    }
    //cout<<endl;
    //for(int i = 0; i < blockSize*8; i++)cout<<block[i];
    //cout<<endl;
    auto blocks = new int [blockSize][8];

    for(int i = 0, j=-1; i < blockSize*8; i++){
        if(i%8==0){j++;/*cout<<" ";*/}
        blocks[j][i%8]=block[i];
        //cout<< blocks[i%8][j];
    }

    int sum=0;
    char c;
    //cout<<endl<<endl<<"here"<<endl;
    for(int i = 0; i < blockSize; i++){
        sum = 0;
        for(int j = 7, k=0; j >=0 ; j--, k++){
            sum+=blocks[i][j]*pow(2,k);
            //cout<<" "<<blocks[j][i]* pow(2,k);
        }
        c = sum;
        //cout<<c<<"("<<sum<<") ";
        res[i]=c;
    }

    return res;
}

void getP1(){
    int res = 0;
    p1=0;
    while(res != 1){
        p1++;
        res = (p*p1)%q;
    }
}

bool encode(string input, string koncovka){
    if(keySize%8!=0){
        cout<<"Spatna velikost klice"<<endl;
        return false;
    }
    blockSize = keySize/8;
    //char ** inputBlocks = new char [input.size()] [blockSize];
    char inputBlocks [input.size()] [blockSize];
    translated = new int[input.size()];
    for(int i = 0; i < input.size(); i++)for(int j = 0; j < blockSize; j++)inputBlocks[i][j]='\0';

    int i,j;
    for(i = 0; i < input.size(); i+=blockSize){
        for(j = 0; j < blockSize; j++){
            inputBlocks[i][j]=input[i+j];
            cout<<inputBlocks[i][j];
        }
        cout<<"|";
    }cout<<endl<<endl;

    if(input.size()%2==1)inputBlocks[i][j+1]=0;
    int * iterRes = new int[input.size()/blockSize];
    for(int i = 0; i < input.size(); i+=blockSize){
        iterRes[i] = iter(inputBlocks[i]);
    }

    for(int i = 0; i < input.size(); i+=blockSize){
        cout<<iterRes[i]<<",";
        if(i!=input.size()-1) out<<iterRes[i]<<", ";
        else out<<iterRes[i];
    }
    out<<endl;

    int it = 0;
    ifstream file;
    file.open(privateKeyFile);
    char ch;
    string privateKeyString;
    int ln = 0;
    while (file.get(ch)){privateKeyString = privateKeyString+ch;ln++;}
    file.close();
    int * pk = new int [ln];
    j = 0, i;
    for(i = 0; i<ln;i++)pk[i]=0;
    for(i = 0; privateKeyString[i]!='\0'; i++){
        if(privateKeyString[i]==',')j++;
        else pk[j]= pk[j] * 10 + (privateKeyString[i] - 48);
    }
    int pkSize = j+1;
    int * privateKey = new int [pkSize];
    int sum=0;
    for(i = 0; i < ln; i++){
        sum+=pk[i];
        if(i<=j)privateKey[i]=pk[i];
        else break;
    }

    for(int i = 0; i < blockSize*8; i++)cout<<privateKey[i]<<",";
    cout<<endl;
    privateKeyInt=privateKey;
    char * abc;
    char * vysledek = new char [input.size()];
    string result1 = "";
    for(int i = 0; i < input.size(); i+=blockSize){
        abc = decode(iterRes[i],it);
        for(int j = 0; j < blockSize; j++){
            vysledek[i+j]=abc[j];
            //cout<<"abc:"<<abc[j]<<" - vysledek[i+j]="<<vysledek[i+j];
        }
        //cout<<" ";
        //result.append(abc);
        //result1 = result1 + "" + abc;
        //cout<<result1;
        it++;
    }
    //cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
    //for(int i = 0; i < input.size(); i++)cout<<result1[i];
    //cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
    //cout<<result1;
    cout<<endl<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
    for(int i = 0; i < input.size(); i+=blockSize){
        if(i!=input.size()-1)out<<translated[i]<<", ";
        else out<<translated[i];
        cout<<translated[i]<<" ";
    }
    out<<endl;
    cout<<endl<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
    for(int i = 0; i < input.size(); i++){
        cout<<vysledek[i];
        out<<vysledek[i];
        if(koncovka!="txt")out2<<vysledek[i];
    }
    return true;
    //decoded = vysledek;
    //decoded = decoded + '\0';
    //cout<<endl<<"++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl<<decoded<<endl;
}

int main(int argc, char *argv[]) {
    if(argc!=4){
        cout << "Spatny pocet argumentu, potrebuji 3." << endl;
        return 2;
    }
    else{
        if(isdigit(argv[2][0]) && isdigit(argv[3][0])){
            p = atoi(argv[2]);
            q = atoi(argv[3]);
            /*ifstream file;
            file.exceptions(ifstream::badbit);
            string s = "";
            string koncovka = "";
            string inp = argv[1];
            try{
                file.open(argv[1]);
                char ch;
                s ="";
                while(file.get(ch))s=s+ch;
            }
            catch (const ifstream::failure& e){
                cout<<"Neco je spate se soubrem";
                return 2;
            }*/
            string inp = argv[1];

            ifstream in_file(inp, ios::binary);
            in_file.seekg(0, ios::end);
            int file_size = in_file.tellg();

            FILE * in;
            in = fopen(inp.c_str(), "rb+");
            //char * s = new char[file_size];
            string s;
            //for(int i = 0; i < file_size; i++)s[i]='\0';
            char c;
            for(int i = 0; i < file_size; i++){
                fread(&c,1,1,in);
                //cout<<c;
                //s[i]=c;
                s=s+c;
            }
            string koncovka = "";

            cout<<"file size: "<<file_size<<" input:"<<inp<<endl;
            if(file_size<10000)cout<<s<<endl;
            koncovka = koncovka + argv[1][inp.size()-3] + argv[1][inp.size()-2] + argv[1][inp.size()-1];

            //cout<<endl<<"koncovka: "<<koncovka<<endl;
            //vse se deje nize
            getP1();
            out.open ("output.txt");
            cout<<"koncovka="<<koncovka<<endl;
            if(koncovka!="txt"){

                out2.open("knapsack_output."+koncovka);
            }
            if(!getPublicKey(privateKeyFile)){cout<<"\nHodnoty p a q jsou chybne a v teto kombinaci nefunguji.\n"<<endl;return 2;}
            if(!encode(s, koncovka)){cout<<"\nZakodovani neuspelo, nastala nejaka chyba\n"<<endl;return 3;}
            else cout<<"\nZakodovani probehlo uspesne\n.";
            out.close();
            if(koncovka!="txt")out2.close();
            //konec toho vseho... nic moc zajimaveho co
        }
        else{
            cout << "Druhy a treti argument musi být cela cisla. Prvni z nich nesmi mit zadné spolecne soucinitele s modulo druhe z nich"<<endl;
            return 2;
        }
        return 1;
    }
}