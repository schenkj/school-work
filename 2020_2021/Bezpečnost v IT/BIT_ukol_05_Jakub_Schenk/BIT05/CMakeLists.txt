cmake_minimum_required(VERSION 3.17)
project(BIT05)

set(CMAKE_CXX_STANDARD 14)

add_executable(BIT05 main.cpp)