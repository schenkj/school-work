Readme k projektu BIT_ukol_05_Jakub_Schenk - program ukazující hashování pomocí zadané hash funkce

Program funguje v jednom kroku při spuštění se jedním argumentem. Tím je cesta k souboru, který má být zahashován.

Nejprve je obsah souboru načten do lokální proměnné, odkud je po jednotlivých znacích hashován. Hashovací fce proběhne tolikrát, kolik je bloků zprávy. V tomto případě tomu je polovina velikosti souboru zaokrouhlena nahoru. Je tomu tak, protože jsou kódovány bloky po 16 bitech a soubor je čten po znacích, které mají jen 8 bitů. Pro jednu iteraci je tedy nutné kódovat dva znaky jako jeden blok dat k zakódování. Pokud je množství znaků v souboru liché, je doplněn poslední půlblok osmi nulami, aby mohl být poslední znak souboru také zpracován. V každé iteraci se pracuje s daty z předchozí iterace a počítají se nová, ta jsou pak nahrána do starých promenných a proces se opakuje. Nakonci je výsledek vypsán v hexadecimální podobě na konzoli.

Výsledkem celého běhu programu je tedy výpis na obrazovku v podobě šestnásti hexadecimálních číslic.

Projekt opět zasílám raději celý, abych nezapoměl přiložit nějakou důležitou komponentu potřebnou ke spuštění.