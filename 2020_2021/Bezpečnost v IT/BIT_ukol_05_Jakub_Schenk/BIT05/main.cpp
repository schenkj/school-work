#include <iostream>
#include <sstream>
#include <math.h>

int * startA = new int[16];
int * startB = new int[16];
int * startC = new int[16];
int * startD = new int[16];
int * newA = new int[16];
int * newB = new int[16];
int * newC = new int[16];
int * newD = new int[16];

using namespace std;

int * logickyAnd(int a [16], int b [16]){
    int * c = new int [16]{0};
    for(int i = 0; i < 16; i++){
        if(a[i]==1 && b[i]==1)c[i]=1;
        else c[i]=0;
    }
    return c;
}

int * logickyOr(int a [16], int b [16]){
    int * c = new int [16]{0};
    for(int i = 0; i < 16; i++){
        if(a[i]==1 || b[i]==1)c[i]=1;
        else c[i]=0;
    }
    return c;
}

int * logickyXor(int a [16], int b [16]){
    int * c = new int [16]{0};
    for(int i = 0; i < 16; i++){
        if(a[i]==b[i])c[i]=0;
        else c[i]=1;
    }
    return c;
}

int * negace(int a [16]){
    int * c = new int [16];
    for(int i = 0; i < 16; i++){
        if(a[i]==0)c[i]=1;
        else c[i]=0;
    }
    return c;
}

void nastavInicializacniVektor(){
    newA = new int[16]{0};
    newB = new int[16]{0};
    newC = new int[16]{0};
    newD = new int[16]{0};
    for(int i = 0; i < 16; i++){
        newA[i]=0;
        newB[i]=0;
        newC[i]=0;
        newD[i]=0;
    }
    startA = new int [16]{0,0,1,0,1,1,1,0,1,1,0,0,0,0,0,0};
    startB = new int [16]{1,1,1,0,0,0,1,1,1,0,1,1,1,1,0,0};
    startC = new int [16]{1,1,1,1,1,1,0,1,0,0,0,1,0,1,1,1};
    startD = new int [16]{1,0,1,0,0,0,0,1,0,0,1,1,1,1,1,1};
    /*cout<<endl;
    for(int i = 0; i < 16; i++)cout<<startA[i];cout<<" ";
    for(int i = 0; i < 16; i++)cout<<startB[i];cout<<" ";
    for(int i = 0; i < 16; i++)cout<<startC[i];cout<<" ";
    for(int i = 0; i < 16; i++)cout<<startD[i];cout<<"\n";
    for(int i = 0; i < 16; i++)cout<<newA[i];cout<<" ";
    for(int i = 0; i < 16; i++)cout<<newB[i];cout<<" ";
    for(int i = 0; i < 16; i++)cout<<newC[i];cout<<" ";
    for(int i = 0; i < 16; i++)cout<<newD[i];cout<<"\n";*/
}

int * f1(int b [16], int c [16]){
    int * a1 = logickyAnd(b,c);
    int * a2 = logickyAnd(negace(b),c);
    int * res = logickyXor(a1,a2);
    delete[]a1;
    delete[]a2;
    //cout<<"f1";
    return res;
}

int * f2(int b [16], int c [16]){
    int * a1 = logickyOr(b,c);
    int * a2 = logickyXor(b,c);
    int * res = logickyAnd(a1,a2);
    delete[]a1;
    delete[]a2;
    //cout<<"f2";
    return res;
}

int * getFc(int counter, int b [16], int c [16]){
    if(!counter) return f2(b,c);
    else return f1(b,c);
}

int * getArr(char c1, char c2){
    int * res = new int[16];
    //cout<<"Mi=";
    for(int i = 0; i < 8; i++){
        res[i]=(c2>>i)&1;
        //cout<<res[i];
    }//cout<<" ";
    for(int i = 8; i < 16; i++){
        res[i]=(c1>>(i-8))&1;
        //cout<<res[i];
    }//cout<<endl;
    int * res2 = new int[16]{0};
    int j;
    for(int i = 0, j=15; i<16; i++,j--){
        res2[j]=res[i];
    }
    return res2;
}

void printHexa(int * b){
    int b1 [] = {b[0], b[1], b[2], b[3]};
    int b2 [] = {b[4], b[5], b[6], b[7]};
    int b3 [] = {b[8], b[9], b[10], b[11]};
    int b4 [] = {b[12], b[13], b[14], b[15]};
    int * c = new int[4]{0};
    for(int i = 0; i < 4; i++){
        c[0]+=pow(2,3-i)*b1[i];
        c[1]+=pow(2,3-i)*b2[i];
        c[2]+=pow(2,3-i)*b3[i];
        c[3]+=pow(2,3-i)*b4[i];
        //cout<<c[0]<<c[1]<<c[2]<<c[3]<<" ";
        //cout<<b1[i]<<b2[i]<<b3[i]<<b4[i]<<" ";
        //cout<<pow(2,3-i)*b1[i]<<"("<<c[0]<<")";
    }
    //cout<<endl;
    /*for(int i = 0; i < 4; i++)cout<<b1[i];cout<<" ";
    for(int i = 0; i < 4; i++)cout<<b2[i];cout<<" ";
    for(int i = 0; i < 4; i++)cout<<b3[i];cout<<" ";
    for(int i = 0; i < 4; i++)cout<<b4[i];cout<<" -> ";
    for(int i = 0; i < 4; i++)cout<<c[i];cout<<endl;
    cout<<"\n\n";*/
    for(int i = 0; i < 4; i++){
        if(c[i]<10)cout<<c[i];
        else if(c[i]==10)cout<<"a";
        else if(c[i]==11)cout<<"b";
        else if(c[i]==12)cout<<"c";
        else if(c[i]==13)cout<<"d";
        else if(c[i]==14)cout<<"e";
        else if(c[i]==15)cout<<"f";
        else cout<<"!"<<c[i]<<"!";
    }
    cout<<" ";
}

int main(int argc, char *argv[]) {
    //nasteni vstupu
    if (argc != 2) {cout << "Wrong number of arguments.";return 2;}
    FILE *in = fopen(argv[1], "rb+");
    fseek(in, 0, SEEK_END);
    long fsize = ftell(in);
    fseek(in, 0, SEEK_SET);  /* same as rewind(in); */
    if(fsize == 0){cout<<"File not found.";return 3;}
    ////////////////////////////////////////////////////////////////////////read file
    char inVal [fsize+2];
    for(int i = 0; i < fsize+2; i++)inVal[i]='\0';
    fread(inVal, 1, fsize, in);
    fclose(in);
    //for(int i = 0; i < fsize+1; i++)cout<<inVal[i];
    //cout<<endl<<endl<<fsize<<endl;
    nastavInicializacniVektor();
    //alg
    int iters = fsize;
    int counter = 0;

    for(int i = 0; i < iters; i+=2){
        //cout<<"i="<<i<<" "<<inVal[i]<<inVal[i+1]<<"\n";
        counter++;
        copy(startD, startD+16, newB);
        copy(startC, startC+16, newD);
        newA = logickyXor(getFc(counter%2, startB, startC), startD);
        if(i==iters-1 && inVal[i+1]=='\0')inVal[i+1]=0;
        int * tmp = logickyXor(startA, startB);
        newC = logickyXor(tmp, getArr(inVal[i], inVal[i+1]));
        /*for(int j = 0; j < 16; j++){cout<<startA[j]<<"->"<<newA[j]<<" | ";}cout<<endl;
        for(int j = 0; j < 16; j++){cout<<startB[j]<<"->"<<newB[j]<<" | ";}cout<<endl;
        for(int j = 0; j < 16; j++){cout<<startC[j]<<"->"<<newC[j]<<" | ";}cout<<endl;
        for(int j = 0; j < 16; j++){cout<<startD[j]<<"->"<<newD[j]<<" | ";}cout<<endl;*/
        copy(newA, newA+16, startA);
        copy(newB, newB+16, startB);
        copy(newC, newC+16, startC);
        copy(newD, newD+16, startD);
    }
    /*for(int i = 0; i < 16; i++)cout<<startA[i]<<" ";cout<<endl;
    for(int i = 0; i < 16; i++)cout<<startB[i]<<" ";cout<<endl;
    for(int i = 0; i < 16; i++)cout<<startC[i]<<" ";cout<<endl;
    for(int i = 0; i < 16; i++)cout<<startD[i]<<" ";cout<<endl;*/
    printHexa(startA);
    printHexa(startB);
    printHexa(startC);
    printHexa(startD);
    //cout<<endl;
    return 0;
}

