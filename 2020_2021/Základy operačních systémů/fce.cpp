#include "main.h"
#include "fce.h"
#include <cstdio>
#include <string>
#include <algorithm>
#include <fstream>
#include <string.h>
#include <stdio.h>
#include <iostream>
#include <iterator>
#include <sstream>

using namespace std;
extern string name;

//pomocna fce pro rm a rmdir
bool memReset(){
    f = fopen(fileName.c_str(), "rb+");
    string a1 = ".";
    std::vector<string> parsed = tokenize(a1);
    pseudo_inode currentInode{};
    if (a1[0] == '/' || a1[0] == '\\') {
        //zacit od rootu
        fseek(f, sb.inode_start_address, SEEK_SET);
        fread(&currentInode, sizeof(struct pseudo_inode), 1, f);
    } else {
        //zacni z aktualni inode
        fseek(f, sb.inode_start_address + (currentNodeId * sizeof(struct pseudo_inode)), SEEK_SET);
        fread(&currentInode, sizeof(struct pseudo_inode), 1, f);
    }
    //presun hloubeji do adresarove struktury
    for (int i = 0; i < parsed.size(); i++) {
        currentInode = findInode(currentInode, parsed[i]);
    }
    //printf("%d \n %d\n",currentNodeId, currentInode.nodeid);
    int max_count = sb.cluster_size / (int32_t) sizeof(struct directory_item);
    struct directory_item dirArr[max_count];
    fseek(f, sb.data_start_address + currentInode.direct1 * sb.cluster_size, SEEK_SET);
    fread(&dirArr, sizeof(struct directory_item), max_count, f);
    //printf("%d", sb.data_start_address);
    pseudo_inode node;
    string s;
    for (int i = 0; i < max_count; i++) {
        if (strcmp(dirArr[i].item_name, "")){
            fseek(f, sb.inode_start_address + dirArr[i].inode*sizeof(pseudo_inode), SEEK_SET);
            fread(&node, sizeof(pseudo_inode), 1, f);
            if(node.isDirectory)s="+";
            else s="-";
            //printf("%s%s\n", s.c_str(), dirArr[i].item_name, dirArr[i].inode);//printf("%s\n", dirArr[i].item_name);
        }
    }
    fclose(f);
    return true;
}

//get file size
int get_file_size(string filename) {
    FILE *p_file = NULL;
    p_file = fopen(filename.c_str(), "rb");
    fseek(p_file, 0, SEEK_END);
    int size = ftell(p_file);
    fclose(p_file);
    return size;
}

//string parser
vector<string> tokenize(string str) {
    vector<string> out;
    std::replace(str.begin(), str.end(), '\\', '/'); // replace all 'x' to 'y'
    char a[str.size()];
    strcpy(a, str.c_str());
    char *token = strtok(a, "/");
    while (token != nullptr) {
        out.emplace_back(token);
        token = strtok(nullptr, "/");
    }
    return out;
}

//move to directory located in current directory
pseudo_inode findInode(pseudo_inode start, const string &findName) {
    FILE *fptr = fopen(fileName.c_str(), "rb+");
    int max_count = sb.cluster_size / (int32_t) sizeof(struct directory_item);
    struct directory_item dirArr[max_count];
    fseek(fptr, sb.data_start_address + start.direct1 * sb.cluster_size, SEEK_SET);
    fread(&dirArr, sizeof(struct directory_item), max_count, fptr);
    for (int i = 0; i < max_count; i++) {
        if (dirArr[i].item_name == findName) {
            //printf("findInode: %d|\"%s\" = \"%s\"\n", dirArr[i].inode, dirArr[i].item_name, findName.c_str());
            pseudo_inode res{};
            fseek(fptr, sb.inode_start_address + dirArr[i].inode * sizeof(struct pseudo_inode), SEEK_SET);
            fread(&res, sizeof(struct pseudo_inode), 1, fptr);
            fclose(fptr);
            return res;
        }
    }
    //directory not found - return to start
    /*pseudo_inode root;
    fseek(fptr, sb.inode_start_address, SEEK_SET);
    fread(&root, sizeof(pseudo_inode), 1, fptr);*/
    fclose(fptr);
    return start;
}


//copy file s1 to new location s2
bool copyF(vector<string> v) {
    f = fopen(fileName.c_str(), "rb+");
    string s1 = v[1];
    string s2 = v[2];
    pseudo_inode originInode;
    pseudo_inode destInode;
    pseudo_inode prevInode;
    pseudo_inode oldFileInode;
    vector<string> pathOrigin = tokenize(s1);
    vector<string> pathDest = tokenize(s2);
    //presun origin inode do mista odkud kopiruji
    if (s1[0] == '/' || s1[0] == '\\') {
        //absolute route - dostan se na root
        fseek(f, sb.inode_start_address, SEEK_SET);
        fread(&originInode, sizeof(pseudo_inode), 1, f);
    } else {
        //relative route - presun se do aktualni slozky ve ktere uzivatel je
        fseek(f, sb.inode_start_address + currentNodeId * sizeof(pseudo_inode), SEEK_SET);
        fread(&originInode, sizeof(pseudo_inode), 1, f);
    }
    //postupne se zanoruj
    if (pathOrigin.size() > 1) {
        for (int i = 0; i < pathOrigin.size() - 1; i++) {
            prevInode = originInode;
            originInode = findInode(originInode, pathOrigin[i]);
            //printf("%s\n",pathOrigin[i].c_str());
            if (prevInode.nodeid == originInode.nodeid) {
                printf("PATH NOT FOUND\n");
                return false;
            }
        }
    }
    //ziskam inode souboru
    oldFileInode = findInode(originInode, pathOrigin[pathOrigin.size() - 1]);
    if (oldFileInode.nodeid == originInode.nodeid) {
        printf("FILE NOT FOUND\n");
        return false;
    }
    //zopakovani pro misto kam kopiruji
    if (s2[0] == '/' || s2[0] == '\\') {
        fseek(f, sb.inode_start_address, SEEK_SET);
        fread(&destInode, sizeof(pseudo_inode), 1, f);
    } else {
        fseek(f, sb.inode_start_address + currentNodeId * sizeof(pseudo_inode), SEEK_SET);
        fread(&destInode, sizeof(pseudo_inode), 1, f);
    }
    if (pathDest.size() > 1) {
        for (int i = 0; i < pathDest.size() - 1; i++) {
            prevInode = destInode;
            destInode = findInode(destInode, pathDest[i]);
            if (prevInode.nodeid == destInode.nodeid) {
                printf("PATH NOT FOUND\n");
                return false;
            }
        }
    }

    //mam inody odkud a kam... je potreba najit v pameti misto
    int sizeFout = oldFileInode.file_size;
    //printf("file size: %d", sizeFout);
    //vytvoreni inode souboru
    struct pseudo_inode newFile{};
    memset(&newFile, 0, sizeof(pseudo_inode));
    newFile.isDirectory = false;
    newFile.references = 0;
    newFile.file_size = sizeFout;

    //kouknu na cestu, zjistim jmeno souboru
    char nameOfNewFile[12];
    memset(&nameOfNewFile, 0, 12);
    string s = pathDest[pathDest.size() - 1];
    string token = s.substr(0, s.find('.'));
    strncpy(nameOfNewFile, token.c_str(), min(7, (int) s.size()));
    string nazev = nameOfNewFile;
    s = pathDest[pathDest.size() - 1];
    token = s.substr(s.rfind('.') + 1, s.size() - (s.rfind('.') + 1));
    string pripona = token;
    string fname = nazev+"."+pripona;
    //strncpy(nameOfNewFile + 8, pripona.c_str(), min(3, (int) pripona.size()));
    strncpy(nameOfNewFile, fname.c_str(), 12);
    nameOfNewFile[11] = '\0';
    //printf("mam jmeno: %s", nameOfNewFile);
    //zjistim jak velky prostor budu potrebovat
    int numOfDirects = sizeFout / sb.cluster_size + 1;
    if (sizeFout % sb.cluster_size == 0)numOfDirects--;

    //inode bitmap zapis nove polozky
    uint8_t inode_bitmap[sb.bitmap_start_address - sb.bitmapi_start_address];
    fseek(f, sb.bitmapi_start_address, SEEK_SET);
    fread(&inode_bitmap, sizeof(inode_bitmap), 1, f);
    bool emptyFound = false;
    //projeď všechny skupiny dat po 8 v inode bitmap
    for (int i = 0; i < sizeof(inode_bitmap); i++) {
        //v každé skupině koukni na prvky
        for (int j = 7; j >= 0; j--) {
            //když najdeš 0, tak je to tam volné a zapiš tam
            if (((inode_bitmap[i] >> j) & 0b1) == 0) {
                //volné místo nalezeno
                emptyFound = true;
                fseek(f, sb.bitmapi_start_address + sizeof(inode_bitmap[i]) * i, SEEK_SET);
                inode_bitmap[i] += pow(2, j);
                //uloz 1 do inode bitmap na nalezene misto
                fwrite(&inode_bitmap[i], sizeof(inode_bitmap[i]), 1, f);
                newFile.nodeid = i * 8 + (7 - j);
            }
            if (emptyFound)break;
        }
        if (emptyFound)break;
    }
    //printf("inode bitmap completed\n");
    //zapis do data bitmap
    int si = sb.inode_start_address - sb.bitmap_start_address;
    uint8_t data_bitmap[si];
    fseek(f, sb.bitmap_start_address, SEEK_SET);
    fread(&data_bitmap, sizeof(data_bitmap), 1, f);
    emptyFound = false;
    vector<int> odkazyNaVolneBloky{};
    int numOfIndirects = 0;
    int cnst = sb.cluster_size / sizeof(uint32_t);
    //vypocet kolik je potreba indirect
    if (numOfDirects > 5)numOfIndirects++;
    if (numOfDirects > cnst + 5) {
        //vytvoreni indirect2
        numOfIndirects++;
        numOfIndirects += ((numOfDirects - 5 - cnst) / (cnst)) + 1;
        if ((numOfDirects - 5 - cnst) % (cnst) == 0)numOfIndirects--;
    }
    //priprav misto a odkaz pro kazdy cluster dat, ktery budu potrebovat
    for (int j = 0; j < numOfDirects + numOfIndirects; j++) {
        emptyFound = false;
        //projeď všechny skupiny dat po 8 v data bitmap
        for (int k = 0; k < si; k++) {
            for (int l = 7; l >= 0; l--) {
                //printf("%d", (data_bitmap[k] >> l) & 0b1);
                //když najdeš 0, tak je to tam volno a zapiš tam jednicku
                if (((data_bitmap[k] >> l) & 0b1) == 0) {
                    data_bitmap[k] += pow(2, l);
                    //volné místo nalezeno
                    fseek(f, sb.bitmap_start_address + sizeof(data_bitmap[k]) * k, SEEK_SET);
                    fwrite(&data_bitmap[k], sizeof(data_bitmap[k]), 1, f);
                    odkazyNaVolneBloky.push_back((8 * k + (7 - l)));
                    emptyFound = true;
                }
                if (emptyFound)break;
            }
            if (emptyFound)break;
        }
    }
    //printf("data bitmap completed\n");
    //mam misto v pameti, tak muzu postupne cist a zapisovat
    char bfr[sb.cluster_size];
    int indirectCounter = 2;
    uint32_t indirect;
    bool endFound = false;

    //nactu direct1 z venku
    fseek(f, sb.data_start_address + oldFileInode.direct1 * sb.cluster_size, SEEK_SET);
    fread(&bfr, sb.cluster_size, 1, f);
    //zapisu jej dovnitr
    fseek(f, sb.data_start_address + odkazyNaVolneBloky[0] * sb.cluster_size, SEEK_SET);
    fwrite(&bfr, sizeof(bfr), 1, f);
    newFile.direct1 = odkazyNaVolneBloky[0];
    if (numOfDirects < 2) {
        endFound = true;
    }
    if (!endFound) {//nactu direct2 z venku
        fseek(f, sb.data_start_address + oldFileInode.direct2 * sb.cluster_size, SEEK_SET);
        fread(&bfr, sb.cluster_size, 1, f);
        //zapisu jej dovnitr
        fseek(f, sb.data_start_address + odkazyNaVolneBloky[1] * sb.cluster_size, SEEK_SET);
        fwrite(&bfr, sizeof(bfr), 1, f);
        newFile.direct2 = odkazyNaVolneBloky[1];
    }

    //nactu direct3 z venku
    if (numOfDirects < 3) {
        endFound = true;
    }
    if (!endFound) {
        fseek(f, sb.data_start_address + oldFileInode.direct3 * sb.cluster_size, SEEK_SET);
        fread(&bfr, sb.cluster_size, 1, f);
        //zapisu jej dovnitr
        fseek(f, sb.data_start_address + odkazyNaVolneBloky[2] * sb.cluster_size, SEEK_SET);
        fwrite(&bfr, sizeof(bfr), 1, f);
        newFile.direct3 = odkazyNaVolneBloky[2];
    }
    //nactu direct4 z venku
    if (numOfDirects < 4) {
        endFound = true;
    }
    if (!endFound) {
        fseek(f, sb.data_start_address + oldFileInode.direct4 * sb.cluster_size, SEEK_SET);
        fread(&bfr, sb.cluster_size, 1, f);
        //zapisu jej dovnitr
        fseek(f, sb.data_start_address + odkazyNaVolneBloky[3] * sb.cluster_size, SEEK_SET);
        fwrite(&bfr, sizeof(bfr), 1, f);
        newFile.direct4 = odkazyNaVolneBloky[3];
    }
    //nactu direct5 z venku
    if (numOfDirects < 5) {
        endFound = true;
    }
    if (!endFound) {
        fseek(f, sb.data_start_address + oldFileInode.direct5 * sb.cluster_size, SEEK_SET);
        fread(&bfr, sb.cluster_size, 1, f);
        //zapisu jej dovnitr
        fseek(f, sb.data_start_address + odkazyNaVolneBloky[4] * sb.cluster_size, SEEK_SET);
        fwrite(&bfr, sizeof(bfr), 1, f);
        newFile.direct5 = odkazyNaVolneBloky[4];
    }
    if (numOfDirects < 6) {
        endFound = true;
    }
    //printf("directs completed\n");
    //pojdme na indirect1
    else newFile.indirect1 = odkazyNaVolneBloky[numOfDirects];
    uint32_t odkazDirect;
    for (int i = 0; i < sb.cluster_size / sizeof(uint32_t); i++) {
        if(endFound)break;
        //printf("for!!!");
        memset(bfr, 0, sb.cluster_size / sizeof(char));
        //nactu odkaz na datovy blok
        fseek(f, sb.data_start_address + oldFileInode.indirect1 * sb.cluster_size + i * sizeof(uint32_t), SEEK_SET);
        fread(&odkazDirect, sizeof(uint32_t), 1, f);
        //prectu si obsah toho na co mi ukazuje muj nove ziskany odkaz
        fseek(f, sb.data_start_address + odkazDirect * sb.cluster_size, SEEK_SET);
        fread(&bfr, sizeof(bfr), 1, f);
        //zapisu jej dovnitr
        fseek(f, sb.data_start_address + odkazyNaVolneBloky[i + 5] * sb.cluster_size, SEEK_SET);
        fwrite(&bfr, sizeof(bfr), 1, f);
        //ulozit odkaz do newFile.indirect1
        fseek(f, sb.data_start_address + newFile.indirect1 * sb.cluster_size + i * sizeof(uint32_t), SEEK_SET);
        fwrite(&odkazyNaVolneBloky[i + 5], sizeof(uint32_t), 1, f);
        if (numOfDirects < i + 7 || endFound) {
            endFound=true;
            break;
        }
    }
    //pojdme na indirect2

    newFile.indirect2 = odkazyNaVolneBloky[numOfDirects + 1];
    for (int j = 0; j < sb.cluster_size / sizeof(uint32_t); j++) {
        if (endFound)break;
        uint32_t indirect = odkazyNaVolneBloky[numOfDirects + 1 + j];
        uint32_t odkazIndirect;
        //nactu odkaz na indirect
        fseek(f, sb.data_start_address + oldFileInode.indirect2 * sb.cluster_size + j * sizeof(uint32_t), SEEK_SET);
        fread(&odkazIndirect, sizeof(uint32_t), 1, f);
        for (int i = 0; i < sb.cluster_size / sizeof(uint32_t); i++) {

            memset(bfr, 0, sb.cluster_size / sizeof(char));
            //nactu odkaz na datovy blok
            fseek(f, sb.data_start_address + odkazIndirect * sb.cluster_size + i * sizeof(uint32_t), SEEK_SET);
            fread(&odkazDirect, sizeof(uint32_t), 1, f);
            //prectu si obsah toho na co mi ukazuje muj nove ziskany odkaz
            fseek(f, sb.data_start_address + odkazDirect * sb.cluster_size, SEEK_SET);
            fread(&bfr, sizeof(bfr), 1, f);
            //zapisu jej dovnitr  5 za directy, cnst 1 za indirect1 j za indirecty uz nactene v indirectu2 a i za uz nactene directy z tohoto indirectu
            fseek(f, sb.data_start_address + odkazyNaVolneBloky[i + 5 + cnst * (1 + j)] * sb.cluster_size, SEEK_SET);
            fwrite(&bfr, sizeof(bfr), 1, f);
            //ulozit odkaz do indirect
            fseek(f, sb.data_start_address + indirect * sb.cluster_size + i * sizeof(uint32_t), SEEK_SET);
            fwrite(&odkazyNaVolneBloky[i + 5 + cnst * (1 + j)], sizeof(uint32_t), 1, f);
            if (numOfDirects < (i + 7 + cnst * (1 + j))) {
                endFound = true;
                break;
            }
        }
        //ulozim indirect do newFile.indirect2
        fseek(f, sb.data_start_address + newFile.indirect2 * sb.cluster_size + j * sizeof(uint32_t), SEEK_SET);
        fwrite(&indirect, sizeof(uint32_t), 1, f);
    }
    //zapsani samotne inode
    fseek(f, sb.inode_start_address + newFile.nodeid * sizeof(pseudo_inode), SEEK_SET);
    fwrite(&newFile, sizeof(pseudo_inode), 1, f);

    //zapsani inodu do nadrazene slozky
    struct directory_item child;
    child.inode = newFile.nodeid;
    memcpy(child.item_name, nameOfNewFile, 12);

    //1) najit kolik podslozek a souboru pod sebou nadrazena slozka ma - kontrola zda ma slozka jeste misto
    struct directory_item dirItems[sb.cluster_size / sizeof(directory_item)];
    fseek(f, sb.data_start_address + sb.cluster_size * destInode.direct1, SEEK_SET);
    fread(&dirItems, sb.cluster_size, 1, f);
    int m;
    //najdi v diritems me nadslozky volny misto pro child
    //todo pokud se dostanu na konec foru, tak mam plnou pamet slozky... mozna by stalo za to to osetrit.... neeee!
    //printf("%d | %d | %d\n",(sb.cluster_size / sizeof(directory_item)),sb.cluster_size, currInode.direct1);
    for (m = 0; m < (sb.cluster_size / sizeof(directory_item)); m++) {
        //printf("for %s\n",dirItems[m].item_name);
        if (dirItems[m].inode == 0 && !strcmp(dirItems[m].item_name, ""))break;
    }
    //2) zapsat
    fseek(f, sb.data_start_address + sb.cluster_size * destInode.direct1 +
               m * sizeof(directory_item), SEEK_SET);
    fwrite(&child, sizeof(directory_item), 1, f);
    emptyFound = true;

    printf("OK\n");
    fclose(f);
    return true;
}

//Move file s1 to new location s2, or rename s1 to s2
bool moveF(vector<string> v) {
    f = fopen(fileName.c_str(), "rb+");
    string s1 = v[1];
    string s2 = v[2];
    pseudo_inode originInode;
    pseudo_inode destInode;
    pseudo_inode prevInode;
    pseudo_inode oldFileInode;
    vector<string> pathOrigin = tokenize(s1);
    vector<string> pathDest = tokenize(s2);
    //presun origin inode do mista odkud kopiruji
    if (s1[0] == '/' || s1[0] == '\\') {
        //absolute route - dostan se na root
        fseek(f, sb.inode_start_address, SEEK_SET);
        fread(&originInode, sizeof(pseudo_inode), 1, f);
    } else {
        //relative route - presun se do aktualni slozky ve ktere uzivatel je
        fseek(f, sb.inode_start_address + currentNodeId * sizeof(pseudo_inode), SEEK_SET);
        fread(&originInode, sizeof(pseudo_inode), 1, f);
    }
    //postupne se zanoruj
    if (pathOrigin.size() > 1) {
        for (int i = 0; i < pathOrigin.size() - 1; i++) {
            prevInode = originInode;
            originInode = findInode(originInode, pathOrigin[i]);
            //printf("%s\n",pathOrigin[i].c_str());
            if (prevInode.nodeid == originInode.nodeid) {
                printf("PATH NOT FOUND\n");
                return false;
            }
        }
    }
    //ziskam inode souboru
    oldFileInode = findInode(originInode, pathOrigin[pathOrigin.size() - 1]);
    if (oldFileInode.nodeid == originInode.nodeid) {
        printf("FILE NOT FOUND\n");
        return false;
    }
    //zopakovani pro misto kam kopiruji
    if (s2[0] == '/' || s2[0] == '\\') {
        fseek(f, sb.inode_start_address, SEEK_SET);
        fread(&destInode, sizeof(pseudo_inode), 1, f);
    } else {
        fseek(f, sb.inode_start_address + currentNodeId * sizeof(pseudo_inode), SEEK_SET);
        fread(&destInode, sizeof(pseudo_inode), 1, f);
    }
    if (pathDest.size() > 1) {
        for (int i = 0; i < pathDest.size() - 1; i++) {
            prevInode = destInode;
            destInode = findInode(destInode, pathDest[i]);
            if (prevInode.nodeid == destInode.nodeid) {
                printf("PATH NOT FOUND\n");
                return false;
            }
        }
    }
    //kouknu na cestu, zjistim jmeno puvodniho souboru
    char nameOfOldFile[12];
    memset(&nameOfOldFile, 0, 12);
    string s = pathOrigin[pathOrigin.size() - 1];
    string token = s.substr(0, s.find('.'));
    string nazev = token;
    strncpy(nameOfOldFile, token.c_str(), min(7, (int) s.size()));
    s = pathOrigin[pathOrigin.size() - 1];
    token = s.substr(s.rfind('.') + 1, s.size() - (s.rfind('.') + 1));
    string pripona = token;
    string fname = nazev+"."+pripona;
    //strncpy(nameOfNewFile + 8, pripona.c_str(), min(3, (int) pripona.size()));
    strncpy(nameOfOldFile, fname.c_str(), 12);
    nameOfOldFile[11] = '\0';
    //printf("mam jmeno: %s", nameOfOldFile);

    //kouknu na cestu, zjistim jmeno noveho souboru
    char nameOfNewFile[12];
    memset(&nameOfNewFile, 0, 12);
    s = pathDest[pathDest.size() - 1];
    token = s.substr(0, s.find('.'));
    strncpy(nameOfNewFile, token.c_str(), min(7, (int) s.size()));
    nazev = nameOfNewFile;
    s = pathDest[pathDest.size() - 1];
    token = s.substr(s.rfind('.') + 1, s.size() - (s.rfind('.') + 1));
    pripona = token;
    fname = nazev+"."+pripona;
    //strncpy(nameOfNewFile + 8, pripona.c_str(), min(3, (int) pripona.size()));
    strncpy(nameOfNewFile, fname.c_str(), 12);
    nameOfNewFile[11] = '\0';
    //printf("mam jmeno: %s", nameOfNewFile);

    vector<string> vec = {"ls","."};
    /*fakeSwitch(vec);
    vec = {"ls","a"};
    fakeSwitch(vec);
    vec = {"ls","b"};
    fakeSwitch(vec);
    vec = {"ls", "c"};
    fakeSwitch(vec);*/

    //printf("%s %s\n", nameOfOldFile, nameOfNewFile);

    //odebrani odkazu ze stareho mista
    struct directory_item child;
    child.inode = oldFileInode.nodeid;
    memcpy(child.item_name, nameOfOldFile, 12);

    //printf("jdu mazat");
    //1) nacist vsechny diritems me nadslozky pro porovnani
    struct directory_item dirItems[sb.cluster_size / sizeof(directory_item)];
    fseek(f, sb.data_start_address + sb.cluster_size * originInode.direct1, SEEK_SET);
    fread(&dirItems, sizeof(directory_item), sb.cluster_size / sizeof(directory_item), f);
    int m;
    //najdi v diritems me nadslozky muj diritem
    for (m = 0; m < (sb.cluster_size / sizeof(directory_item)); m++) {
        //pokud me najdes break - mam ulozene m jako index
        if (dirItems[m].inode == oldFileInode.nodeid && !strcmp(dirItems[m].item_name, nameOfOldFile)){break;}
    }
    //2) zapsat
    uint8_t a = 0;
    fseek(f, sb.data_start_address + sb.cluster_size * originInode.direct1 + m * sizeof(directory_item), SEEK_SET);
    for(int i = 0; i < sizeof(directory_item)/sizeof(uint8_t); i++){
        fwrite(&a, sizeof(uint8_t), 1, f);
    }
    //odkaz na soubor byl smazan z puvodni lokace
    memset(&child, 0, sizeof(directory_item));
    child.inode = oldFileInode.nodeid;
    memcpy(child.item_name, nameOfNewFile, 12);
    //vytvoreni odkazu v nove lokaci
    dirItems[sb.cluster_size / sizeof(directory_item)];
    fseek(f, sb.data_start_address + sb.cluster_size * destInode.direct1, SEEK_SET);
    fread(&dirItems, sizeof(directory_item), sb.cluster_size / sizeof(directory_item), f);
    //najdi v diritems me nadslozky muj diritem
    for (m = 0; m < (sb.cluster_size / sizeof(directory_item)); m++) {
        //pokud me najdes break - mam ulozene m jako index
        if (dirItems[m].inode == 0 && !strcmp(dirItems[m].item_name, ""))break;
    }
    //2) zapsat
    fseek(f, sb.data_start_address + sb.cluster_size * destInode.direct1 + m * sizeof(directory_item), SEEK_SET);
    fwrite(&child, sizeof(directory_item), 1, f);
    printf("OK\n");
    fclose(f);
    return true;
}


//delete file s1
bool delF(vector<string> v) {
    //posun do slozky ze ktere odstranuji a ziskani odstranovaneho inode
    f = fopen(fileName.c_str(), "rb+");
    string s1 = v[1];
    std::vector<string> parsed = tokenize(s1);
    pseudo_inode fileNode{};
    pseudo_inode dirNode{};
    if (s1[0] == '/' || s1[0] == '\\') {
        //zacit od rootu
        fseek(f, sb.inode_start_address, SEEK_SET);
        fread(&fileNode, sizeof(struct pseudo_inode), 1, f);
    } else {
        //zacni z aktualni inode
        fseek(f, sb.inode_start_address + (currentNodeId * sizeof(struct pseudo_inode)), SEEK_SET);
        fread(&fileNode, sizeof(struct pseudo_inode), 1, f);
    }
    pseudo_inode prevInode;
    for (int i = 0; i < parsed.size(); i++) {
        if (i == parsed.size() - 1) {
            dirNode = fileNode;
        }
        //fileNode = findInode(fileNode, parsed[i]);
        prevInode = fileNode;
        fileNode = findInode(fileNode, parsed[i]);
        if (prevInode.nodeid == fileNode.nodeid) {
            printf("PATH NOT FOUND\n");
            return false;
        }
    }
    //printf("nodes nacteny: %d, %d\n", fileNode.nodeid, dirNode.nodeid);
    //todo rmdir nize
    //nactu obsah slozky ve ktere je odstranovany soubor a odstranim na nej odkaz
    int maxDitems = sb.cluster_size / (int32_t) sizeof(struct directory_item);
    struct directory_item dirItems[maxDitems];
    fseek(f, sb.data_start_address + dirNode.direct1 * sb.cluster_size, SEEK_SET);
    fread(&dirItems, sizeof(struct directory_item), maxDitems, f);
    //printf("Vypis slozky:\n");
    /*for(int i = 0; i < maxDitems; i++){
        if(dirItems[i].item_name!="")printf("%s\n", dirItems[i].item_name);
    }*/
    //printf("--------------\n");
    uint8_t a = 0;
    //hledam dirItem
    //todo zkontrolovat
    for (int i = 0; i < maxDitems; i++) {
        //pokud nazev dirItemu odpovida nazvu odstranovane slozky, odstranim ji
        if (dirItems[i].item_name == parsed[parsed.size() - 1]) {
            //printf("Founed: %s",dirItems[i].item_name);
            //posun do dat -> direct odkud chci mazat -> na pozici i kde lezi directory_item ktery chci odebrat
            fseek(f, sb.data_start_address + (dirNode.direct1 * sb.cluster_size) + (sizeof(directory_item) * i), SEEK_SET);
            for (int j = 0; j < (sizeof(directory_item) / sizeof(uint8_t)); j++) {
                fwrite(&a, sizeof(uint8_t), 1, f);
            }
            //printf("Odkaz odebran: (pro slozku s indexem) %d\n", i);
        }
    }
    fflush(f);
    memReset();
    //vector<string>vec = {"ls","."};
    //fakeSwitch(vec);
    //vec = {"ls","a"};
    //fakeSwitch(vec);
    //vec = {"ls","b"};
    //fakeSwitch(vec);
    //vec = {"ls", "c"};
    //fakeSwitch(vec);

    //todo uvolnit mista v bitmapach - inodebitmap na indexu teto slozky a databitmap pro . a ..
    //uvolneni pameti v inode bitmap
    //int sizeOfInodeBitmap = (sb.disk_size / 10) / sizeof(struct pseudo_inode);
    int sizeOfInodeBitmap = sb.bitmap_start_address - sb.bitmapi_start_address;
    uint8_t inode_bitmap[sizeOfInodeBitmap / sizeof(uint8_t)];
    fseek(f, sb.bitmapi_start_address, SEEK_SET);
    //fread(inode_bitmap, sizeOfInodeBitmap * sizeof(uint8_t), 1, f);
    fread(inode_bitmap, sizeof(uint8_t), sizeOfInodeBitmap / sizeof(uint8_t), f);

    //celociselne deleni -> dostanu spravny byte
    int indexB = fileNode.nodeid / 8;
    //zbytek po deleni je urcity bit v bytu
    int multipB = 7 - (fileNode.nodeid % 8);
    //printf("\n%d", inode_bitmap[indexB]);
    inode_bitmap[indexB] -= pow(2, multipB);//todo zkontrolovat
    fseek(f, sb.bitmapi_start_address + indexB * sizeof(uint8_t), SEEK_SET);
    fwrite(&inode_bitmap[indexB], sizeof(uint8_t), 1, f);
    //printf("\n%d", inode_bitmap[indexB]);
    //uvolneni pameti v data bitmap
    int sizeOfDataBitmap = sb.cluster_count / sizeof(uint8_t);
    uint8_t data_bitmap[sizeOfDataBitmap / sizeof(uint8_t)];
    fseek(f, sb.bitmap_start_address, SEEK_SET);
    fread(data_bitmap, sizeOfDataBitmap * sizeof(uint8_t), 1, f);
    //todo jak najdu svuj cluster?
    //celociselne deleni -> dostanu spravny byte
    int indexD = fileNode.direct1 / 8;
    //zbytek po deleni je urcity bit v bytu
    int multipD = 7 - (fileNode.direct1 % 8);
    data_bitmap[indexD] -= pow(2, multipD);//todo zkontrolovat
    fseek(f, sb.bitmap_start_address + indexD * sizeof(uint8_t), SEEK_SET);
    fwrite(&data_bitmap[indexD], sizeof(uint8_t), 1, f);

    //todo uvolnit misto v pameti a inode - pseudo_inode kde byla tato slozka a directory_item pro . a ..
    //inode
    fseek(f, sb.inode_start_address + fileNode.nodeid * sizeof(pseudo_inode), SEEK_SET);
    for (int i = 0; i < sizeof(pseudo_inode) / sizeof(uint8_t); i++) {
        fwrite(&a, sizeof(uint8_t), 1, f);
    }
    //data
    //direct1-5
    int fileSize = fileNode.file_size;
    if (fileSize / sb.cluster_size < 1) {
        printf("OK\n");
        fclose(f);
        return true;
    }
    fseek(f, sb.data_start_address + fileNode.direct1, SEEK_SET);
    for (int i = 0; i < sb.cluster_size / sizeof(uint8_t); i++) { fwrite(&a, sizeof(uint8_t), 1, f); }
    if (fileSize / sb.cluster_size < 2) {
        printf("OK\n");
        fclose(f);
        return true;
    }
    fseek(f, sb.data_start_address + fileNode.direct2, SEEK_SET);
    for (int i = 0; i < sb.cluster_size / sizeof(uint8_t); i++) { fwrite(&a, sizeof(uint8_t), 1, f); }
    if (fileSize / sb.cluster_size < 3) {
        printf("OK\n");
        fclose(f);
        return true;
    }
    fseek(f, sb.data_start_address + fileNode.direct3, SEEK_SET);
    for (int i = 0; i < sb.cluster_size / sizeof(uint8_t); i++) { fwrite(&a, sizeof(uint8_t), 1, f); }
    if (fileSize / sb.cluster_size < 4) {
        printf("OK\n");
        fclose(f);
        return true;
    }
    fseek(f, sb.data_start_address + fileNode.direct4, SEEK_SET);
    for (int i = 0; i < sb.cluster_size / sizeof(uint8_t); i++) { fwrite(&a, sizeof(uint8_t), 1, f); }
    if (fileSize / sb.cluster_size < 5) {
        printf("OK\n");
        fclose(f);
        return true;
    }
    //printf("\n\nHERE\n\n");
    fseek(f, sb.data_start_address + fileNode.direct5, SEEK_SET);
    for (int i = 0; i < sb.cluster_size / sizeof(uint8_t); i++) { fwrite(&a, sizeof(uint8_t), 1, f); }
    //vzhuru na indirect1
    uint32_t directs[sb.cluster_size / sizeof(uint32_t)];
    fseek(f, sb.data_start_address + fileNode.indirect1 * sb.cluster_size, SEEK_SET);
    fread(&directs, sizeof(uint32_t), sb.cluster_size / sizeof(uint32_t), f);
    //bool foundEnd = false;
    //mam seznam directu - postupne cist a odstranovat
    for (int i = 0; i < sb.cluster_size / sizeof(uint32_t); i++) {
        if (directs[i] == 0) {
            printf("OK\n");
            fclose(f);
            return true;
        }
        fseek(f, sb.data_start_address + directs[i] * sb.cluster_size, SEEK_SET);
        for (int j = 0; j < sb.cluster_size / sizeof(uint8_t); j++) { fwrite(&a, sizeof(uint8_t), 1, f); }
    }
    //a indirect2
    uint32_t indirects[sb.cluster_size / sizeof(uint32_t)];
    fseek(f, sb.data_start_address + fileNode.indirect2 * sb.cluster_size, SEEK_SET);
    fread(&indirects, sizeof(uint32_t), sb.cluster_size / sizeof(uint32_t), f);

    for (int k = 0; k < sb.cluster_size / sizeof(uint32_t); k++) {
        if (indirects[k] == 0) {
            printf("OK\n");
            fclose(f);
            return true;
        }
        memset(&directs, 0, sb.cluster_size);
        directs[sb.cluster_size / sizeof(uint32_t)];
        fseek(f, sb.data_start_address + indirects[k]*sb.cluster_size, SEEK_SET);
        fread(&directs, sizeof(uint32_t), sb.cluster_size / sizeof(uint32_t), f);
        //bool foundEnd = false;
        //mam seznam directu - postupne cist a odstranovat
        for (int i = 0; i < sb.cluster_size / sizeof(uint32_t); i++) {
            if (directs[i] == 0) {
                printf("OK\n");
                fclose(f);
                return true;
            }
            fseek(f, sb.data_start_address + directs[i] * sb.cluster_size, SEEK_SET);
            for (int j = 0; j < sb.cluster_size / sizeof(uint8_t); j++) { fwrite(&a, sizeof(uint8_t), 1, f); }
        }
    }
}

//create new directory a1
bool createDir(vector<string> v) {
    f = fopen(fileName.c_str(), "rb+");
    string a1 = v[1];
    //todo kontrola aby se dva adresare nejmenovali stejne

    //directory create
    struct pseudo_inode myAdr{};
    myAdr.isDirectory = true;
    myAdr.references = 0;
    myAdr.file_size = sb.cluster_size;
    vector<string> path = tokenize(a1);
    char dirName[8];
    strncpy(dirName, path[path.size() - 1].c_str(), 8);
    //printf("%s %s\n",path[path.size()].c_str(), path[path.size()-1].c_str());
    pseudo_inode currInode;
    pseudo_inode prevInode;
    if (a1[0] == '/' || a1[0] == '\\') {
        //absolute route
        //dostan se na root
        fseek(f, sb.inode_start_address, SEEK_SET);
        fread(&currInode, sizeof(pseudo_inode), 1, f);
    } else {
        //relative route
        //presun se do aktualni slozky ve ktere uzivatel je
        fseek(f, sb.inode_start_address + currentNodeId * sizeof(pseudo_inode), SEEK_SET);
        fread(&currInode, sizeof(pseudo_inode), 1, f);
        //zacni se posouvat podle relativni cesty k slozce ve ktere se bude dalsi slozka vytvaret
        //printf("path.size() = %d\n", path.size());
    }
    //postupne se zanoruj
    if (path.size() > 1) {
        for (int i = 0; i < path.size() - 1; i++) {
            prevInode = currInode;
            currInode = findInode(currInode, path[i]);
            if (prevInode.nodeid == currInode.nodeid) {
                printf("PATH NOT FOUND\n");
                return false;
            }
        }
    }
    //zkontroluj, zda takovy adresar uz neexistuje
    directory_item dirArr[sb.cluster_size / sizeof(directory_item)];
    fseek(f, sb.data_start_address + currInode.direct1, SEEK_SET);
    fread(&dirArr, sizeof(directory_item), sb.cluster_size / sizeof(directory_item), f);
    string str1;
    string str2 = dirName;
    for (int i = 0; i < sb.cluster_size / sizeof(directory_item); i++) {
        str1 = dirArr[i].item_name;
        //if(str1.compare("")!=0)printf("NAME: \"%s\" ?= \"%s\"\n",dirArr[i].item_name, dirName);
        if (str1.compare(str2)==0) {
            printf("EXIST\n");
            return false;
        }
    }

    uint8_t a = 0;
    int i = 1;
    int found8bit;
    //najit volné místo na bitmapě
    //printf("\nInode bitmapa:\n");
    uint8_t inode_bitmap[sb.bitmap_start_address - sb.bitmapi_start_address];
    fseek(f, sb.bitmapi_start_address, SEEK_SET);
    fread(&inode_bitmap, sizeof(inode_bitmap), 1, f);
    bool emptyFound = false;
    int x = 1;
    //projeď všechny skupiny dat po 8 v inode bitmap
    for (int i = 0; i < sizeof(inode_bitmap); i++) {
        //v každé skupině koukni na prvky
        for (int j = 7; j >= 0; j--) {
            //printf("%d", (inode_bitmap[i] >> j) & 0b1);
            //když najdeš 0, tak je to tam volné a zapiš tam
            if (((inode_bitmap[i] >> j) & 0b1) == 0) {
                //printf("here");
                //volné místo nalezeno
                emptyFound = true;
                fseek(f, sb.bitmapi_start_address + sizeof(inode_bitmap[i]) * i, SEEK_SET);
                inode_bitmap[i] += pow(2, j);
                //uloz 1 do inode bitmap na nalezene misto
                fwrite(&inode_bitmap[i], sizeof(inode_bitmap[i]), 1, f);
                myAdr.nodeid = i * 8 + (7 - j);


                struct directory_item tecka{myAdr.nodeid, "."};
                //printf("dvojtecka: %d\n",currInode.nodeid);
                struct directory_item dvojtecka{currInode.nodeid, ".."};
                struct directory_item child;
                child.inode = myAdr.nodeid;
                strncpy(child.item_name, dirName, 8);
                //ted je potřeba najit volný místo v datech pro directory_items
                int si = sb.inode_start_address - sb.bitmap_start_address;
                uint8_t data_bitmap[si];
                fseek(f, sb.bitmap_start_address, SEEK_SET);
                fread(&data_bitmap, sizeof(data_bitmap), 1, f);

                bool emptyFound = false;
                int x = 1;
                //printf("sizeofDataBitmap: %d - %d\n",sb.inode_start_address, sb.bitmap_start_address);
                //projeď všechny skupiny dat po 8 v data bitmap
                //printf("%d %d %d\n",(data_bitmap[0] >> 0) & 0b1, (data_bitmap[1] >> 0) & 0b1, (data_bitmap[2] >> 0) & 0b1);
                for (int k = 0; k < si; k++) {
                    for (int l = 7; l >= 0; l--) {
                        //printf("%d", (data_bitmap[k] >> l) & 0b1);
                        //když najdeš 0, tak je to tam volné a zapiš tam
                        if (((data_bitmap[k] >> l) & 0b1) == 0) {

                            data_bitmap[k] += pow(2, l);
                            //volné místo nalezeno
                            fseek(f, sb.bitmap_start_address + sizeof(data_bitmap[k]) * k, SEEK_SET);
                            fwrite(&data_bitmap[k], sizeof(data_bitmap[k]), 1, f);

                            //zapsat . a ..
                            fseek(f, sb.data_start_address + sb.cluster_size * (k * 8 + (7 - l)), SEEK_SET);
                            fwrite(&tecka, sizeof(directory_item), 1, f);
                            fwrite(&dvojtecka, sizeof(directory_item), 1, f);
                            //zapsat child do nadrazene slozky
                            //1) najit kolik podslozek a souboru pod sebou nadrazena slozka ma - kontrola zda ma slozka jeste misto
                            struct directory_item dirItems[sb.cluster_size / sizeof(directory_item)];
                            fseek(f, sb.data_start_address + sb.cluster_size * currInode.direct1, SEEK_SET);
                            fread(&dirItems, sb.cluster_size, 1, f);
                            int m;
                            //najdi v diritems me nadslozky volny misto pro child
                            //todo pokud se dostanu na konec foru, tak mam plnou pamet slozky... mozna by stalo za to to osetrit.... neeee!
                            for (m = 0; m < (sb.cluster_size / sizeof(directory_item)); m++) {
                                if (dirItems[m].inode == 0 && !strcmp(dirItems[m].item_name, ""))break;
                            }
                            //2) zapsat
                            fseek(f, sb.data_start_address + sb.cluster_size * currInode.direct1 +
                                     m * sizeof(directory_item), SEEK_SET);
                            fwrite(&child, sizeof(directory_item), 1, f);
                            emptyFound = true;
                            myAdr.direct1 = 8 * k + (7 - l);
                        }
                        if (emptyFound)break;
                    }
                    if (emptyFound)break;
                }
                //zapis samotneho inode
                fseek(f, sb.inode_start_address + myAdr.nodeid * sizeof(pseudo_inode), SEEK_SET);
                fwrite(&myAdr, sizeof(pseudo_inode), 1, f);
                //printf("%s created\n", path[path.size() - 1].c_str());
            }
            if (emptyFound)break;
        }
        if (emptyFound)break;
    }
    printf("OK\n");
    fclose(f);
    return true;
}

//Delete empty directory a1
bool delDir(vector<string> v) {
    //posun do odstranované slozky
    f = fopen(fileName.c_str(), "rb+");
    string a1 = v[1];
    std::vector<string> parsed = tokenize(a1);
    pseudo_inode tmpNode{};
    pseudo_inode tmpParent{};
    int dirCounter = 0;
    if (a1[0] == '/' || a1[0] == '\\') {
        //zacit od rootu
        fseek(f, sb.inode_start_address, SEEK_SET);
        fread(&tmpNode, sizeof(struct pseudo_inode), 1, f);
    } else {
        //zacni z aktualni inode
        fseek(f, sb.inode_start_address + (currentNodeId * sizeof(struct pseudo_inode)), SEEK_SET);
        fread(&tmpNode, sizeof(struct pseudo_inode), 1, f);
    }
    pseudo_inode prevInode;
    for (int i = 0; i < parsed.size(); i++) {
        if (i == parsed.size() - 1) {
            tmpParent = tmpNode;
        }
        //tmpNode = findInode(tmpNode, parsed[i]);
        prevInode = tmpNode;
        tmpNode = findInode(tmpNode, parsed[i]);
        if (prevInode.nodeid == tmpNode.nodeid) {
            printf("PATH NOT FOUND\n");
            return false;
        }
    }
    //printf("nodes nacteny: %d, %d", tmpNode.nodeid, tmpParent.nodeid);

    //kontrola ze je adresar prazdny
    //nactu obsah teto slozky
    int max_count = sb.cluster_size / sizeof(struct directory_item);
    struct directory_item dirArr[max_count];
    fseek(f, sb.data_start_address + tmpNode.direct1 * sb.cluster_size, SEEK_SET);
    fread(&dirArr, sizeof(struct directory_item), max_count, f);
    //mam vsechny podslozky me slozky, staci porovnat id vsech s idPotomka ze ktereho jsem prisel a zapsat jeho nazev
    for (int i = 0; i < max_count; i++) {
        if (strcmp(dirArr[i].item_name, ""))dirCounter++;
    }
    //je slozka prazdna?
    if (dirCounter > 2) {
        printf("NOT EMPTY\n");
        return false;
    }
    //printf("Directory is empty - can be deleted");

    //je - pokracuji teda jejim odstranenim
    //nactu obsah slozky ve ktere je odstranovana slozka a odstranim na ni odkaz
    //int newMax = sb.cluster_size / (int32_t) sizeof(struct directory_item);
    //struct directory_item newArr[newMax];
    memset(&dirArr, 0, max_count*sizeof(directory_item));
    fseek(f, sb.data_start_address + tmpParent.direct1 * sb.cluster_size, SEEK_SET);
    fread(&dirArr, sizeof(struct directory_item), max_count, f);
    uint8_t a = 0;
    //hledam dirItem
    //todo zkontrolovat
    vector<string> vec = {"ls","."};
    for (int i = 0; i < max_count; i++) {
        //pokud nazev dirItemu odpovida nazvu odstranovane slozky, odstranim ji
        string str1 = dirArr[i].item_name;
        string str2 = parsed[parsed.size() - 1];
        //printf("%s | %s\n", str1.c_str(), str2.c_str());
        //fakeSwitch(vec);
        if (str1.compare(str2)==0) {

            /*fakeSwitch(vec);*/
            //posun do dat -> direct odkud chci mazat -> na pozici i kde lezi directory_item ktery chci odebrat
            fseek(f, sb.data_start_address + (tmpParent.direct1 * sb.cluster_size) + (sizeof(directory_item) * i),
                  SEEK_SET);
            for (int j = 0; j < (sizeof(directory_item) / sizeof(uint8_t)); j++) {
                fwrite(&a, sizeof(uint8_t), 1, f);
            }
            fflush(f);
            //printf("");
            //fakeSwitch(vec);

            break;
            //printf("Odkaz odebran: (pro slozku s indexem) %d", i);
        }
    }
    fclose(f);
    f = fopen(fileName.c_str(), "rb+");
    memReset();
    //fakeSwitch(vec);

    //uvolneni pameti v inode bitmap
    int sizeOfInodeBitmap = (sb.disk_size / 10) / sizeof(struct pseudo_inode);
    uint8_t inode_bitmap[sizeOfInodeBitmap / sizeof(uint8_t)];
    fseek(f, sb.bitmapi_start_address, SEEK_SET);
    fread(&inode_bitmap, sizeOfInodeBitmap * sizeof(uint8_t), 1, f);
    //celociselne deleni -> dostanu spravny byte
    int indexB = tmpNode.nodeid / 8;
    //zbytek po deleni je urcity bit v bytu
    int multipB = 7 - (tmpNode.nodeid % 8);
    inode_bitmap[indexB] -= pow(2, multipB);
    fseek(f, sb.bitmapi_start_address + indexB * sizeof(uint8_t), SEEK_SET);
    fwrite(&inode_bitmap[indexB], sizeof(uint8_t), 1, f);

    //uvolneni pameti v data bitmap
    int sizeOfDataBitmap = sb.cluster_count / sizeof(uint8_t);
    uint8_t data_bitmap[sizeOfDataBitmap / sizeof(uint8_t)];
    fseek(f, sb.bitmap_start_address, SEEK_SET);
    fread(&data_bitmap, sizeOfDataBitmap * sizeof(uint8_t), 1, f);
    //celociselne deleni -> dostanu spravny byte
    int indexD = tmpNode.direct1 / 8;
    //zbytek po deleni je urcity bit v bytu
    int multipD = 7 - (tmpNode.direct1 % 8);
    data_bitmap[indexD] -= pow(2, multipD);
    fseek(f, sb.bitmap_start_address + indexD * sizeof(uint8_t), SEEK_SET);
    fwrite(&data_bitmap[indexD], sizeof(uint8_t), 1, f);

    //inode
    fseek(f, sb.inode_start_address + tmpNode.nodeid * sizeof(pseudo_inode), SEEK_SET);
    for (int i = 0; i < sizeof(pseudo_inode) / sizeof(uint8_t); i++) {
        fwrite(&a, sizeof(uint8_t), 1, f);
    }
    //data
    fseek(f, sb.data_start_address + tmpNode.direct1, SEEK_SET);
    for (int i = 0; i < sb.cluster_size / sizeof(uint8_t); i++) {
        fwrite(&a, sizeof(uint8_t), 1, f);
    }

    fclose(f);
    printf("OK\n");
    return true;
}

//Print what is in file s1
bool listF(vector<string> v) {
    string s1 = v[1];
    FILE *f = fopen(fileName.c_str(), "rb+");
    std::vector<string> parsed = tokenize(s1);
    pseudo_inode tmpInode;
    //posun do mista odkud kopiruji ven
    if (s1[0] == '/' || s1[0] == '\\') {//zacit od rootu - absolute route
        fseek(f, sb.inode_start_address, SEEK_SET);
        fread(&tmpInode, sizeof(struct pseudo_inode), 1, f);
    } else {//zacit v aktualni slozce - relative route
        fseek(f, sb.inode_start_address + (currentNodeId * sizeof(struct pseudo_inode)), SEEK_SET);
        fread(&tmpInode, sizeof(struct pseudo_inode), 1, f);

    }//presun
    pseudo_inode prevInode;
    for (int i = 0; i < parsed.size() - 1; i++) {
        //printf("%d/", tmpInode.nodeid);
        //tmpInode = findInode(tmpInode, parsed[i]);
        prevInode = tmpInode;
        tmpInode = findInode(tmpInode, parsed[i]);
        if (prevInode.nodeid == tmpInode.nodeid) {
            printf("PATH NOT FOUND\n");
            return false;
        }
    }
    pseudo_inode fileNode = findInode(tmpInode, parsed[parsed.size() - 1]);
    if (tmpInode.nodeid == fileNode.nodeid) {
        printf("PATH NOT FOUND\n");
        return false;
    }

    //zjistit koik budu odesilat clusteru
    //todo zkusit odebrat
    int clusterCount = fileNode.file_size / sb.cluster_size + 1;
    char lastCluster[fileNode.file_size % sb.cluster_size + 1];
    memset(&lastCluster, 0, fileNode.file_size % sb.cluster_size);

    /*if (fileNode.file_size % sb.cluster_size == 0) {
        clusterCount--;
        char lastCluster[sb.cluster_size];
    }*/

    //directy
    uint8_t a = 0;
    //ziskani inode souboru a postupne vypisovani
    char bfr[sb.cluster_size];
    memset(&bfr, 0, sb.cluster_size);
    fseek(f, sb.data_start_address + fileNode.direct1 * sb.cluster_size, SEEK_SET);
    if (clusterCount == 1) {
        memset(&lastCluster, 0, sizeof(lastCluster));
        fread(&lastCluster, fileNode.file_size % sb.cluster_size, 1, f);
        printf("%s", lastCluster);
        fclose(f);
        return true;
    }
    fread(&bfr, sb.cluster_size, 1, f);
    printf("%s", bfr);

    memset(&bfr, 0, sb.cluster_size);
    fseek(f, sb.data_start_address + fileNode.direct2 * sb.cluster_size, SEEK_SET);
    if (clusterCount == 2) {
        memset(&lastCluster, 0, sizeof(lastCluster));
        fread(&lastCluster, fileNode.file_size % sb.cluster_size, 1, f);
        printf("%s", lastCluster);
        fclose(f);
        return true;
    }
    fread(&bfr, sb.cluster_size, 1, f);
    printf("%s", bfr);

    memset(&bfr, 0, sb.cluster_size);
    fseek(f, sb.data_start_address + fileNode.direct3 * sb.cluster_size, SEEK_SET);
    if (clusterCount == 3) {
        memset(&lastCluster, 0, sizeof(lastCluster));
        fread(&lastCluster, fileNode.file_size % sb.cluster_size, 1, f);
        printf("%s", lastCluster);
        fclose(f);
        return true;
    }
    fread(&bfr, sb.cluster_size, 1, f);
    printf("%s", bfr);

    memset(&bfr, 0, sb.cluster_size);
    fseek(f, sb.data_start_address + fileNode.direct4 * sb.cluster_size, SEEK_SET);
    if (clusterCount == 4) {
        memset(&lastCluster, 0, sizeof(lastCluster));
        fread(&lastCluster, fileNode.file_size % sb.cluster_size, 1, f);
        printf("%s", lastCluster);
        fclose(f);
        return true;
    }
    fread(&bfr, sb.cluster_size, 1, f);
    printf("%s", bfr);

    memset(&bfr, 0, sb.cluster_size);
    fseek(f, sb.data_start_address + fileNode.direct5 * sb.cluster_size, SEEK_SET);
    if (clusterCount == 5) {
        memset(&lastCluster, 0, sizeof(lastCluster));
        fread(&lastCluster, fileNode.file_size % sb.cluster_size, 1, f);
        printf("%s", lastCluster);
        fclose(f);
        return true;
    }
    fread(&bfr, sb.cluster_size, 1, f);
    printf("%s", bfr);
    //vypis indirect1
    uint32_t odkazDal;
    for (int i = 0; i < sb.cluster_size / sizeof(uint32_t); i++) {
        memset(&bfr, 0, sb.cluster_size);
        fseek(f, sb.data_start_address + fileNode.indirect1 * sb.cluster_size + sizeof(uint32_t) * i, SEEK_SET);
        fread(&odkazDal, sizeof(uint32_t), 1, f);
        //pokud je direct 0, je prazdny a pro me konci soubor
        if (odkazDal == 0) {
            fclose(f);
            return true;
        }
        fseek(f, sb.data_start_address + odkazDal * sb.cluster_size, SEEK_SET);
        if (clusterCount == i + 6) {
            memset(&lastCluster, 0, sizeof(lastCluster));
            fread(&lastCluster, fileNode.file_size % sb.cluster_size, 1, f);
            //printf("%s %d", lastCluster, i);
            printf("%s", lastCluster);
            fclose(f);
            return true;
        }
        fread(&bfr, sb.cluster_size, 1, f);
        printf("%s", bfr);
        //printf("indirect na direct i = %d\n", i);

    }
    //printf("za indirecty1");
    if (fileNode.indirect2 == 0) {
        fclose(f);
        return true;
    }
    //vypis indirect2
    //obsahy vsech indirect1 uvnitr
    uint32_t indir;
    for (int i = 0; i < sb.cluster_size / sizeof(uint32_t); i++) {
        //obsahy vsech direct uvnitr
        fseek(f, sb.data_start_address + fileNode.indirect2 * sb.cluster_size + sizeof(uint32_t) * i, SEEK_SET);
        fread(&indir, sizeof(uint32_t), 1, f);
        for (int j = 0; j < sb.cluster_size / sizeof(uint32_t); j++) {
            fseek(f, sb.data_start_address + indir * sb.cluster_size + sizeof(uint32_t) * j, SEEK_SET);
            fread(&odkazDal, sizeof(uint32_t), 1, f);
            //pokud je direct 0, je prazdny a pro me konci soubor
            memset(&bfr, 0, sb.cluster_size);
            if (clusterCount == i + 6 + (sb.cluster_size / sizeof(uint32_t))) {
                memset(&lastCluster, 0, sizeof(lastCluster));
                fread(&lastCluster, fileNode.file_size % sb.cluster_size, 1, f);
                //printf("%s %d", lastCluster, i);
                printf("%s", lastCluster);
                fclose(f);
                return true;

            }
            fread(&bfr, sb.cluster_size, 1, f);
            printf("%s", bfr);
        }
    }
    printf("\n");
    fclose(f);
}

//Print what is in directory a1
bool listDir(vector<string> v) {
    f = fopen(fileName.c_str(), "rb+");
    string a1 = v[1];
    std::vector<string> parsed = tokenize(a1);
    pseudo_inode currentInode{};
    if (a1[0] == '/' || a1[0] == '\\') {
        //zacit od rootu
        fseek(f, sb.inode_start_address, SEEK_SET);
        fread(&currentInode, sizeof(struct pseudo_inode), 1, f);
    } else {
        //zacni z aktualni inode
        fseek(f, sb.inode_start_address + (currentNodeId * sizeof(struct pseudo_inode)), SEEK_SET);
        fread(&currentInode, sizeof(struct pseudo_inode), 1, f);
    }
    //presun hloubeji do adresarove struktury
    for (int i = 0; i < parsed.size(); i++) {
        currentInode = findInode(currentInode, parsed[i]);
    }
    //printf("%d \n %d\n",currentNodeId, currentInode.nodeid);
    int max_count = sb.cluster_size / (int32_t) sizeof(struct directory_item);
    struct directory_item dirArr[max_count];
    fseek(f, sb.data_start_address + currentInode.direct1 * sb.cluster_size, SEEK_SET);
    fread(&dirArr, sizeof(struct directory_item), max_count, f);
    //printf("%d", sb.data_start_address);
    pseudo_inode node;
    string s;
    for (int i = 0; i < max_count; i++) {
        if (strcmp(dirArr[i].item_name, "")){
            fseek(f, sb.inode_start_address + dirArr[i].inode*sizeof(pseudo_inode), SEEK_SET);
            fread(&node, sizeof(pseudo_inode), 1, f);
            if(node.isDirectory)s="+";
            else s="-";
            printf("%s%s\n", s.c_str(), dirArr[i].item_name, dirArr[i].inode);//printf("%s\n", dirArr[i].item_name);
        }
    }
    fclose(f);
    return true;
}

//Move in filesystem to new location a1
bool move(vector<string> v) {
    FILE *fptr = fopen(fileName.c_str(), "rb+");
    string a1 = v[1];
    std::vector<string> parsed = tokenize(a1);
    pseudo_inode tmpInode;
    if (a1[0] == '/' || a1[0] == '\\') {
        //zacit od rootu
        fseek(fptr, sb.inode_start_address, SEEK_SET);
        fread(&tmpInode, sizeof(struct pseudo_inode), 1, fptr);
    } else {
        //zacit v aktualni slozce
        fseek(fptr, sb.inode_start_address + (currentNodeId * sizeof(struct pseudo_inode)), SEEK_SET);
        fread(&tmpInode, sizeof(struct pseudo_inode), 1, fptr);

    }
    //presun
    pseudo_inode prevInode;
    for (int i = 0; i < parsed.size() - 1; i++) {
        //printf("%d/", tmpInode.nodeid);
        //tmpInode = findInode(tmpInode, parsed[i]);
        prevInode = tmpInode;
        tmpInode = findInode(tmpInode, parsed[i]);
        if (prevInode.nodeid == tmpInode.nodeid) {
            printf("PATH NOT FOUND\n");
            return false;
        }
    }
    //printf("%d", tmpInode.nodeid);
    //vypis + ulozeni id jako aktualni pozice
    directory_item tmpDir;
    fseek(fptr, sb.data_start_address + tmpInode.direct1 * sb.cluster_size, SEEK_SET);
    fread(&tmpDir, sizeof(directory_item), 1, fptr);
    //printf("You are in %s", tmpDir.item_name);
    currentNodeId = tmpInode.nodeid;

    printf("OK\n");
    fclose(fptr);
    return true;
}

//Print where am I - my path
bool path() {
    FILE *f = fopen(fileName.c_str(), "rb+");
    //todo do konzole vypsat cestu pomoci currentNodeId
    //printf("here1\n");
    vector<string> out;
    pseudo_inode tmpNode{};
    directory_item tmpDir{};
    int idPotomka = 0;
    //postupuj pres vsechny vrstvy nahoru
    //zacneme v currentNode
    fseek(f, sb.inode_start_address + currentNodeId * sizeof(pseudo_inode), SEEK_SET);
    fread(&tmpNode, sizeof(pseudo_inode), 1, f);
    idPotomka = currentNodeId;
    //printf("here2\n");
    //najdu si nadrazenou slozku
    fseek(f, sb.data_start_address + sb.cluster_size * tmpNode.direct1 + sizeof(directory_item), SEEK_SET);
    fread(&tmpDir, sizeof(directory_item), 1, f);
    //printf("here3 index ..: %d searching for %d\n", tmpDir.inode, idPotomka);
    while (tmpNode.nodeid != 0) {
        //presunu se do nalezene slozky
        fseek(f, sb.inode_start_address + tmpDir.inode * sizeof(pseudo_inode), SEEK_SET);
        fread(&tmpNode, sizeof(pseudo_inode), 1, f);

        //nactu obsah teto slozky
        int max_count = sb.cluster_size / (int32_t) sizeof(struct directory_item);
        struct directory_item dirArr[max_count];
        fseek(f, sb.data_start_address + tmpNode.direct1 * sb.cluster_size, SEEK_SET);
        fread(&dirArr, sizeof(struct directory_item), max_count, f);
        //mam vsechny podslozky me slozky, staci porovnat id vsech s idPotomka ze ktereho jsem prisel a zapsat jeho nazev
        for (int i = 0; i < max_count; i++) {

            if (dirArr[i].inode == idPotomka) {
                out.emplace_back(dirArr[i].item_name);
            } else if (!strcmp(dirArr[i].item_name, ""))continue;
        }
        //ulozim id pro zpetne dohledani vyssi slozky
        idPotomka = tmpNode.nodeid;
        //zjistim kam se mam pri pristi iteraci presunout
        fseek(f, sb.data_start_address + sb.cluster_size * tmpNode.direct1 + sizeof(directory_item), SEEK_SET);
        fread(&tmpDir, sizeof(directory_item), 1, f);
        if (idPotomka == 0)break;
    }
    //vypis
    for (int i = out.size() - 1; i >= 0; i--) {
        if (i == 0)printf("%s\n", out[i].c_str());
        else printf("%s/", out[i].c_str());
    }
    fclose(f);
    return true;
}

//Give me info about file or directory s1/a1 (and it's clusters)
bool info(vector<string> v) {
    string o1 = v[1];
    FILE *f = fopen(fileName.c_str(), "rb+");
    std::vector<string> parsed = tokenize(o1);
    pseudo_inode tmpInode;
    //posun do mista odkud kopiruji ven
    if (o1[0] == '/' || o1[0] == '\\') {//zacit od rootu - absolute route
        fseek(f, sb.inode_start_address, SEEK_SET);
        fread(&tmpInode, sizeof(struct pseudo_inode), 1, f);
    } else {//zacit v aktualni slozce - relative route
        fseek(f, sb.inode_start_address + (currentNodeId * sizeof(struct pseudo_inode)), SEEK_SET);
        fread(&tmpInode, sizeof(struct pseudo_inode), 1, f);

    }//presun
    pseudo_inode prevInode;
    for (int i = 0; i < parsed.size(); i++) {
        //printf("%d/", tmpInode.nodeid);
        //tmpInode = findInode(tmpInode, parsed[i]);
        prevInode = tmpInode;
        tmpInode = findInode(tmpInode, parsed[i]);
        if (prevInode.nodeid == tmpInode.nodeid) {
            printf("FILE NOT FOUND\n");
            return false;
        }
    }

    if (tmpInode.isDirectory) {
        printf("%s - %d - %d - ", parsed[parsed.size() - 1].c_str(), tmpInode.file_size, tmpInode.nodeid);
        //je to slozka - kouknu jen do direct1 a vypisu podslozky
        int max_count = sb.cluster_size / (int32_t) sizeof(struct directory_item);
        struct directory_item dirArr[max_count];
        fseek(f, sb.data_start_address + tmpInode.direct1 * sb.cluster_size, SEEK_SET);
        fread(&dirArr, sizeof(struct directory_item), max_count, f);
        printf("Direct links: \n");
        for (int i = 0; i < max_count; i++) {
            if (strcmp(dirArr[i].item_name, ""))
                printf("%s\t|\t%d\n", dirArr[i].item_name, dirArr[i].inode);
        }

    } else {
        printf("%s - %d - %d - ", parsed[parsed.size() - 1].c_str(), tmpInode.file_size, tmpInode.nodeid);
        //ziskani inode souboru a postupne vypisovani
        uint32_t bfr[sb.cluster_size / sizeof(uint32_t)];
        //pseudo_inode fileNode = findInode(tmpInode, parsed[parsed.size()-1]);
        pseudo_inode fileNode = tmpInode;
        printf("Direct links: \n");
        if (fileNode.direct1 != 0) {
            printf("direct1: %d\n", fileNode.direct1);
            /*fseek(f, sb.data_start_address + fileNode.direct1*sb.cluster_size, SEEK_SET);
            fread(&bfr, sizeof(directory_item), sb.cluster_size/sizeof(directory_item), f);
            for(int i = 0; i < sb.cluster_size/sizeof(directory_item), i++){
                printf("Name: %s\t|\tIndex: %d",)
            }*/
        }
        if (fileNode.direct2 != 0) printf("direct2: %d\n", fileNode.direct2);
        if (fileNode.direct3 != 0) printf("direct3: %d\n", fileNode.direct3);
        if (fileNode.direct4 != 0) printf("direct4: %d\n", fileNode.direct4);
        if (fileNode.direct5 != 0) printf("direct5: %d\n", fileNode.direct5);
        if (fileNode.indirect1 != 0) printf("indirect1: %d\n", fileNode.indirect1);
        fseek(f, sb.data_start_address + fileNode.indirect1 * sb.cluster_size, SEEK_SET);
        fread(&bfr, sizeof(uint32_t), sb.cluster_size / sizeof(uint32_t), f);
        //vypis indirect1
        if (fileNode.indirect1 <= 0) {
            fclose(f);
            return true;
        }
        for (int i = 0; i < sb.cluster_size / sizeof(uint32_t); i++) {
            if (bfr[i] == 0)break;
            printf("Link %d\n", bfr[i]);
        }
        if(fileNode.indirect2<=0 || fileNode.indirect2>10*bfr[sb.cluster_size / sizeof(uint32_t)]){
            fclose(f);
            return true;
        }
        printf("indirect2: %d\n", fileNode.indirect2);
        //vypis indirect2
        uint32_t indir[sb.cluster_size / sizeof(uint32_t)];
        fseek(f, sb.data_start_address + fileNode.indirect2 * sb.cluster_size, SEEK_SET);
        fread(&indir, sizeof(uint32_t), sb.cluster_size / sizeof(uint32_t), f);
        for (int i = 0; i < sb.cluster_size / sizeof(uint32_t); i++) {
            if (indir[i] == 0)break;
            printf("Indirect link %d\n", indir[i]);
            fseek(f, sb.data_start_address + indir[i] * sb.cluster_size, SEEK_SET);
            fread(&bfr, sizeof(uint32_t), sb.cluster_size / sizeof(uint32_t), f);
            for (int j = 0; j < sb.cluster_size / sizeof(uint32_t); j++) {
                if (bfr[j] == 0)break;
                printf("Link %d\n", bfr[j]);
            }
        }
    }
    fclose(f);
    return true;
}

// Upload s1 from pseudoNTFS to new location s2 on disc
bool outcp(vector<string> v) {
    FILE *f = fopen(fileName.c_str(), "rb+");
    string s1 = v[1];
    string s2 = v[2];
    std::vector<string> parsed = tokenize(s1);
    pseudo_inode tmpInode;
    //posun do mista odkud kopiruji ven
    if (s1[0] == '/' || s1[0] == '\\') {//zacit od rootu - absolute route
        fseek(f, sb.inode_start_address, SEEK_SET);
        fread(&tmpInode, sizeof(struct pseudo_inode), 1, f);
    } else {//zacit v aktualni slozce - relative route
        fseek(f, sb.inode_start_address + (currentNodeId * sizeof(struct pseudo_inode)), SEEK_SET);
        fread(&tmpInode, sizeof(struct pseudo_inode), 1, f);

    }//presun
    pseudo_inode prevInode;
    for (int i = 0; i < parsed.size() - 1; i++) {
        //printf("%d/", tmpInode.nodeid);
        //tmpInode = findInode(tmpInode, parsed[i]);
        prevInode = tmpInode;
        tmpInode = findInode(tmpInode, parsed[i]);
        if (prevInode.nodeid == tmpInode.nodeid) {
            printf("PATH NOT FOUND\n");
            fclose(f);
            return false;
        }
    }
    //vytvorit a otevrit soubor, do ktereho budu zapisovat
    FILE *fout = fopen(s2.c_str(), "wb+");
    if(fout == nullptr){
        printf("FILE NOT FOUND\n");
        fclose(f);
        return false;
    }

    //zacnu postupne cist a zapisovat data, dokud mi nedojdou
    //ziskam inode souboru
    pseudo_inode fileNode = findInode(tmpInode, parsed[parsed.size() - 1]);

    //zjistit koik budu odesilat clusteru
    int clusterCount = fileNode.file_size / sb.cluster_size + 1;
    char lastCluster[fileNode.file_size % sb.cluster_size];
    memset(&lastCluster, 0, fileNode.file_size % sb.cluster_size);

    /*if (fileNode.file_size % sb.cluster_size == 0) {
        clusterCount--;
        char lastCluster[sb.cluster_size];
    }*/

    //directy
    uint8_t a = 0;
    //ziskani inode souboru a postupne vypisovani
    char bfr[sb.cluster_size];
    memset(&bfr, 0, sb.cluster_size);
    fseek(f, sb.data_start_address + fileNode.direct1 * sb.cluster_size, SEEK_SET);
    if (clusterCount == 1) {
        fread(&lastCluster, fileNode.file_size % sb.cluster_size, 1, f);
        fwrite(lastCluster, fileNode.file_size % sb.cluster_size, 1, fout);
        printf("OK\n");
        fclose(f);
        fclose(fout);
        return true;
    }
    fread(&bfr, sb.cluster_size, 1, f);
    fwrite(bfr, sb.cluster_size, 1, fout);

    memset(&bfr, 0, sb.cluster_size);
    fseek(f, sb.data_start_address + fileNode.direct2 * sb.cluster_size, SEEK_SET);
    if (clusterCount == 2) {
        fread(&lastCluster, fileNode.file_size % sb.cluster_size, 1, f);
        fwrite(lastCluster, fileNode.file_size % sb.cluster_size, 1, fout);
        printf("OK\n");
        fclose(f);
        fclose(fout);
        return true;
    }
    fread(&bfr, sb.cluster_size, 1, f);
    fwrite(bfr, sb.cluster_size, 1, fout);

    memset(&bfr, 0, sb.cluster_size);
    fseek(f, sb.data_start_address + fileNode.direct3 * sb.cluster_size, SEEK_SET);
    if (clusterCount == 3) {
        fread(&lastCluster, fileNode.file_size % sb.cluster_size, 1, f);
        fwrite(lastCluster, fileNode.file_size % sb.cluster_size, 1, fout);
        printf("OK\n");
        fclose(f);
        fclose(fout);
        return true;
    }
    fread(&bfr, sb.cluster_size, 1, f);
    fwrite(bfr, sb.cluster_size, 1, fout);

    memset(&bfr, 0, sb.cluster_size);
    fseek(f, sb.data_start_address + fileNode.direct4 * sb.cluster_size, SEEK_SET);
    if (clusterCount == 4) {
        fread(&lastCluster, fileNode.file_size % sb.cluster_size, 1, f);
        fwrite(lastCluster, fileNode.file_size % sb.cluster_size, 1, fout);
        printf("OK\n");
        fclose(f);
        fclose(fout);
        return true;
    }
    fread(&bfr, sb.cluster_size, 1, f);
    fwrite(bfr, sb.cluster_size, 1, fout);

    memset(&bfr, 0, sb.cluster_size);
    fseek(f, sb.data_start_address + fileNode.direct5 * sb.cluster_size, SEEK_SET);
    if (clusterCount == 5) {
        fread(&lastCluster, fileNode.file_size % sb.cluster_size, 1, f);
        fwrite(lastCluster, fileNode.file_size % sb.cluster_size, 1, fout);
        printf("OK\n");
        fclose(f);
        fclose(fout);
        return true;
    }
    fread(&bfr, sb.cluster_size, 1, f);
    fwrite(bfr, sb.cluster_size, 1, fout);
    //printf("za directy\n");
    //vypis indirect1
    uint32_t odkazDal;
    for (int i = 0; i < sb.cluster_size / sizeof(uint32_t); i++) {
        memset(&bfr, 0, sb.cluster_size);
        fseek(f, sb.data_start_address + fileNode.indirect1 * sb.cluster_size + sizeof(uint32_t) * i, SEEK_SET);
        fread(&odkazDal, sizeof(uint32_t), 1, f);
        //pokud je direct 0, je prazdny a pro me konci soubor
        if (odkazDal == 0) {
            printf("OK\n");
            fclose(f);
            fclose(fout);
            return true;
        }
        fseek(f, sb.data_start_address + odkazDal * sb.cluster_size, SEEK_SET);
        if (clusterCount == i + 6) {
            fread(&lastCluster, fileNode.file_size % sb.cluster_size, 1, f);
            fwrite(lastCluster, fileNode.file_size % sb.cluster_size, 1, fout);
            printf("OK\n");
            fclose(f);
            fclose(fout);
            return true;
        }
        fread(&bfr, sb.cluster_size, 1, f);
        fwrite(bfr, sb.cluster_size, 1, fout);
        //printf("indirect na direct i = %d\n", i);

    }
    //printf("za indirecty1");
    if (fileNode.indirect2 == 0) {
        printf("OK\n");
        fclose(f);
        fclose(fout);
        return true;
    }
    //vypis indirect2
    //obsahy vsech indirect1 uvnitr
    uint32_t indir;
    for (int i = 0; i < sb.cluster_size / sizeof(uint32_t); i++) {
        //obsahy vsech direct uvnitr
        fseek(f, sb.data_start_address + fileNode.indirect2 * sb.cluster_size + sizeof(uint32_t) * i, SEEK_SET);
        fread(&indir, sizeof(uint32_t), 1, f);
        for (int j = 0; j < sb.cluster_size / sizeof(uint32_t); j++) {
            fseek(f, sb.data_start_address + indir * sb.cluster_size + sizeof(uint32_t) * j, SEEK_SET);
            fread(&odkazDal, sizeof(uint32_t), 1, f);
            //pokud je direct 0, je prazdny a pro me konci soubor
            /*if (odkazDal == 0) {
                fclose(f);
                fclose(fout);
                return true;
            }*/
            memset(&bfr, 0, sb.cluster_size);
            if (clusterCount == i + 6 + (sb.cluster_size / sizeof(uint32_t))) {
                fread(&lastCluster, fileNode.file_size % sb.cluster_size, 1, f);
                fwrite(lastCluster, fileNode.file_size % sb.cluster_size, 1, fout);
                printf("OK\n");
                fclose(f);
                fclose(fout);
                return true;
            }
            fread(&bfr, sb.cluster_size, 1, f);
            fwrite(bfr, sb.cluster_size, 1, fout);
        }
    }
    printf("OK\n");
    fclose(f);
    fclose(fout);
    return true;
}

//Nahraje soubor s1 z pevného disku do umístění s2 v pseudoNTFS
bool incp(vector<string> v) {
    //printf("CNID:%d\n",currentNodeId);
    string s1 = v[1];
    string s2 = v[2];
    FILE *fout = fopen(s1.c_str(), "rb");
    if(fout == nullptr){
        printf("FILE NOT FOUND\n");
        return false;
    }
    FILE *fin = fopen(fileName.c_str(), "rb+");
    int sizeFout = get_file_size(s1);
    //printf("%d", sizeFout);
    //vytvoreni inode souboru
    struct pseudo_inode newFile{};
    newFile.isDirectory = false;
    newFile.references = 0;
    newFile.file_size = sizeFout;

    //kouknu na cestu, zjistim jmeno souboru
    vector<string> pathS2 = tokenize(s2);
    //vector<string> pathS1 = tokenize(s1);
    char nameOfNewFile[12];
    memset(&nameOfNewFile, 0, 12);
    string s = pathS2[pathS2.size() - 1];
    string token = s.substr(0, s.find('.'));
    strncpy(nameOfNewFile, token.c_str(), min(7, (int) s.size()));
    string nazev = nameOfNewFile;
    s = pathS2[pathS2.size() - 1];
    //printf("%s", s.c_str());
    token = s.substr(s.rfind('.') + 1, s.size() - (s.rfind('.') + 1));
    string pripona = token;
    string fname = nazev+"."+pripona;
    //strncpy(nameOfNewFile + 8, pripona.c_str(), min(3, (int) pripona.size()));
    strncpy(nameOfNewFile, fname.c_str(), 12);
    nameOfNewFile[11] = '\0';
    //printf("mam jmeno");

    //zapsat do teto slozky, ze jsem zde vytvoril soubor
    pseudo_inode currInode;
    //printf("\n sb.inode_start_address : %d\n",sb.inode_start_address);
    if (s2[0] == '/' || s2[0] == '\\') {
        //absolute route
        //dostan se na root
        fseek(fin, sb.inode_start_address, SEEK_SET);
        fread(&currInode, sizeof(pseudo_inode), 1, fin);
    } else {
        //relative route
        //presun se do aktualni slozky ve ktere uzivatel je
        fseek(fin, sb.inode_start_address + currentNodeId * sizeof(pseudo_inode), SEEK_SET);
        fread(&currInode, sizeof(pseudo_inode), 1, fin);
    }
    //zacni se posouvat podle relativni cesty k slozce ve ktere se bude dalsi slozka vytvaret
    pseudo_inode prevInode;
    if (pathS2.size() > 1) {
        for (int i = 0; i < pathS2.size() - 1; i++) {
            prevInode = currInode;
            currInode = findInode(currInode, pathS2[i]);
            //printf("%s\n",pathOrigin[i].c_str());
            if (prevInode.nodeid == currInode.nodeid) {
                printf("PATH NOT FOUND\n");
                return false;
            }
        }
    }

    //zjistim jak velky prostor budu potrebovat
    int numOfDirects = sizeFout / sb.cluster_size + 1;
    if (sizeFout % sb.cluster_size == 0)numOfDirects--;

    //inode bitmap zapis nove polozky
    uint8_t inode_bitmap[sb.bitmap_start_address - sb.bitmapi_start_address];
    fseek(fin, sb.bitmapi_start_address, SEEK_SET);
    fread(&inode_bitmap, sizeof(inode_bitmap), 1, fin);
    bool emptyFound = false;
    //projeď všechny skupiny dat po 8 v inode bitmap
    //printf("517");
    for (int i = 0; i < sizeof(inode_bitmap); i++) {
        //v každé skupině koukni na prvky
        for (int j = 7; j >= 0; j--) {
            //když najdeš 0, tak je to tam volné a zapiš tam
            if (((inode_bitmap[i] >> j) & 0b1) == 0) {
                //volné místo nalezeno
                emptyFound = true;
                fseek(fin, sb.bitmapi_start_address + sizeof(inode_bitmap[i]) * i, SEEK_SET);
                inode_bitmap[i] += pow(2, j);
                //uloz 1 do inode bitmap na nalezene misto
                fwrite(&inode_bitmap[i], sizeof(inode_bitmap[i]), 1, fin);
                newFile.nodeid = i * 8 + (7 - j);
            }
            if (emptyFound)break;
        }
        if (emptyFound)break;
    }
    //printf("537");
    //zapis do data bitmap
    int si = sb.inode_start_address - sb.bitmap_start_address;
    uint8_t data_bitmap[si];
    fseek(fin, sb.bitmap_start_address, SEEK_SET);
    fread(&data_bitmap, sizeof(data_bitmap), 1, fin);
    emptyFound = false;
    vector<int> odkazyNaVolneBloky{};
    int numOfIndirects = 0;
    int cnst = sb.cluster_size / sizeof(uint32_t);
    //vypocet kolik je potreba indirect
    if (numOfDirects > 5)numOfIndirects++;
    if (numOfDirects > cnst + 5) {
        //vytvoreni indirect2
        numOfIndirects++;
        numOfIndirects += ((numOfDirects - 5 - cnst) / (cnst)) + 1;
        if ((numOfDirects - 5 - cnst) % (cnst) == 0)numOfIndirects--;
    }
    //printf("555");
    //priprav misto a odkaz pro kazdy cluster dat, ktery budu potrebovat
    for (int j = 0; j < numOfDirects + numOfIndirects; j++) {
        emptyFound = false;
        //projeď všechny skupiny dat po 8 v data bitmap
        for (int k = 0; k < si; k++) {
            for (int l = 7; l >= 0; l--) {
                //printf("%d", (data_bitmap[k] >> l) & 0b1);
                //když najdeš 0, tak je to tam volno a zapiš tam jednicku
                if (((data_bitmap[k] >> l) & 0b1) == 0) {
                    data_bitmap[k] += pow(2, l);
                    //volné místo nalezeno
                    fseek(fin, sb.bitmap_start_address + sizeof(data_bitmap[k]) * k, SEEK_SET);
                    fwrite(&data_bitmap[k], sizeof(data_bitmap[k]), 1, fin);
                    odkazyNaVolneBloky.push_back((8 * k + (7 - l)));
                    emptyFound = true;
                }
                if (emptyFound)break;
            }
            if (emptyFound)break;
        }
    }
    //printf("575");
    //mam misto v pameti, tak muzu postupne cist a zapisovat
    char bfr[sb.cluster_size / sizeof(char)];
    int indirectCounter = 2;
    uint32_t indirect;
    for (int i = 0; i < numOfDirects; i++) {
        //printf("for!!!");
        memset(bfr, 0, sb.cluster_size / sizeof(char));
        //nactu datovy blok z venku
        //printf("585");
        fread(&bfr, sb.cluster_size / sizeof(char), 1, fout);
        //printf("587");
        //zapisu jej dovnitr
        //printf("\n%d - %d\n",odkazyNaVolneBloky.size(), i);
        fseek(fin, sb.data_start_address + odkazyNaVolneBloky[i] * sb.cluster_size, SEEK_SET);
        //printf("588");
        fwrite(&bfr, sizeof(bfr), 1, fin);
        //printf("591");
        if (i == 0)newFile.direct1 = odkazyNaVolneBloky[i];
        else if (i == 1)newFile.direct2 = odkazyNaVolneBloky[i];
        else if (i == 2)newFile.direct3 = odkazyNaVolneBloky[i];
        else if (i == 3)newFile.direct4 = odkazyNaVolneBloky[i];
        else if (i == 4)newFile.direct5 = odkazyNaVolneBloky[i];
        else {
            //printf("592");
            if (i == 5)newFile.indirect1 = odkazyNaVolneBloky[numOfDirects];
            if (i < cnst + 5) {
                //zapis do indirect1
                fseek(fin, sb.data_start_address + newFile.indirect1 * sb.cluster_size + (i - 5) * sizeof(uint32_t),
                      SEEK_SET);
                fwrite(&odkazyNaVolneBloky[i], sizeof(uint32_t), 1, fin);
            } else {
                //printf("600");
                //zapis do indirect2
                if (i == cnst + 5)newFile.indirect2 = odkazyNaVolneBloky[numOfDirects + 1];
                if (i % cnst + 5 == 0) {
                    indirect = odkazyNaVolneBloky[numOfDirects + indirectCounter];
                    fseek(fin, sb.data_start_address + newFile.indirect2 * sb.cluster_size +
                               (indirectCounter - 2) * sizeof(uint32_t), SEEK_SET);
                    fwrite(&indirect, sizeof(uint32_t), 1, fin);
                    indirectCounter++;
                }
                fseek(fin, sb.data_start_address + indirect * sb.cluster_size + ((i - 5) % cnst) * sizeof(uint32_t),
                      SEEK_SET);
                fwrite(&odkazyNaVolneBloky[i], sizeof(uint32_t), 1, fin);
            }
        }
    }
    //printf("614");
    //zapis samotneho inode
    //printf("newFile.inodeid: %d\n",newFile.nodeid);
    fseek(fin, sb.inode_start_address + newFile.nodeid * sizeof(pseudo_inode), SEEK_SET);
    fwrite(&newFile, sizeof(pseudo_inode), 1, fin);


    //v teto slozce je soubor
    struct directory_item child;
    child.inode = newFile.nodeid;
    memcpy(child.item_name, nameOfNewFile, 12);

    //1) najit kolik podslozek a souboru pod sebou nadrazena slozka ma - kontrola zda ma slozka jeste misto
    struct directory_item dirItems[sb.cluster_size / sizeof(directory_item)];
    fseek(fin, sb.data_start_address + sb.cluster_size * currInode.direct1, SEEK_SET);
    fread(&dirItems, sb.cluster_size, 1, fin);
    int m;
    //najdi v diritems me nadslozky volny misto pro child
    //todo pokud se dostanu na konec foru, tak mam plnou pamet slozky... mozna by stalo za to to osetrit.... neeee!
    //printf("%d | %d | %d\n",(sb.cluster_size / sizeof(directory_item)),sb.cluster_size, currInode.direct1);
    for (m = 0; m < (sb.cluster_size / sizeof(directory_item)); m++) {
        //printf("for %s\n",dirItems[m].item_name);
        if (dirItems[m].inode == 0 && !strcmp(dirItems[m].item_name, ""))break;
    }
    //2) zapsat
    fseek(fin, sb.data_start_address + sb.cluster_size * currInode.direct1 +
               m * sizeof(directory_item), SEEK_SET);
    fwrite(&child, sizeof(directory_item), 1, fin);
    emptyFound = true;
    fclose(fin);
    fclose(fout);
    //printf("CNID:%d\n",currentNodeId);
    printf("OK\n");
}

//Načte soubor z pevného disku, ve kterém budou jednotlivé příkazy, a začne je sekvenčně vykonávat. Formát je 1 příkaz na 1 řádek
bool loadBuild(vector<string> v) {
    string file = v[1];
    std::ifstream in(file.c_str());
    if(in == 0){
        printf("FILE NOT FOUND\n");
        in.close();
        return false;
    }
    std::string str;
    vector<string> fileContent;
    // Read the next line from File untill it reaches the end.
    while (std::getline(in, str)) {
        // Line contains string of length > 0 then save it in vector
        if (str.size() > 0)
            fileContent.push_back(str);
    }
    for (int i = 0; i < fileContent.size(); i++) {
        string s = fileContent[i];
        stringstream ss(s);
        istream_iterator<string> begin(ss);
        istream_iterator<string> end;
        vector<string> input(begin, end);
        //std::copy(input.begin(), input.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
        //printf("->Loading... \"%s\"\n", fileContent[i].c_str());
        fakeSwitch(input);
    }
    printf("OK\n");
    in.close();
}

//Format disc. Delete old disc or create new one
bool format(vector<string> v) {
    //printf("Wait a second, it may take a while.\n");
    //FILE *f = f;
    string size = v[1];
    int c = atoi(size.c_str());
    //stringy pro porovnani
    int multiplier = 1;
    //pokud se najde v porovnani string, tak podle toho co uzivatel chce, tak zvetsime finalSize
    if (size.find("kB") != -1 || size.find("KB") != -1 || size.find("kb") != -1)multiplier = pow(2, 10);
    else if (size.find("MB") != -1 || size.find("mB") != -1 || size.find("mb") != -1)multiplier = pow(2, 20);
    else { /*printf("I didn\'t find kb or mb, so the size wasn\'t affected by any multiplier.\n"); */}
    //pozadovana velikost disku zapsana jako int
    int finalSize = c * multiplier;
    fill_sb(finalSize);
    FILE *f = fopen(fileName.c_str(), "wb+");
    if(f == nullptr){
        printf("CANNOT CREATE FILE");
        fclose(f);
        return false;
    }
    //printf("%d",f);
    //f = fopen(fileName.c_str(), "wb+");
    //printf("%d",sb.disk_size);
    // inode bitmapa / a soubor.txt
    int sizeOfInodeBitmap;
    sizeOfInodeBitmap = (sb.disk_size / 10) / sizeof(struct pseudo_inode);
    if (sizeOfInodeBitmap < 1)exit(2);
    uint8_t inode_bitmap[sizeOfInodeBitmap / sizeof(uint8_t)];
    memset(inode_bitmap, 0, sizeOfInodeBitmap / sizeof(uint8_t));
    inode_bitmap[0] = 0b10000000;
    // data bitmapa - první jsou data složky / pak rozdělený soubor.txt
    int sizeOfDataBitmap = 0;
    sizeOfDataBitmap = sb.cluster_count / sizeof(uint8_t);
    uint8_t data_bitmap[sizeOfDataBitmap];
    memset(data_bitmap, 0, sizeOfDataBitmap);
    data_bitmap[0] = 0b10000000;

    sb.bitmapi_start_address = sizeof(struct superblock); // konec sb
    sb.bitmap_start_address = sb.bitmapi_start_address + sizeOfInodeBitmap;//konec bitmap inode
    sb.inode_start_address = sb.bitmap_start_address + sizeOfDataBitmap;//konec bitmap clusteru
    sb.data_start_address = sb.inode_start_address + (sizeOfInodeBitmap * sizeof(pseudo_inode));//konec inode
    fwrite(&sb, sizeof(struct superblock), 1, f);

    // zapis bitmap
    fwrite(&inode_bitmap, sizeof(uint8_t), sizeOfInodeBitmap, f);
    fwrite(&data_bitmap, sizeof(uint8_t), sizeOfDataBitmap, f);
    //printf("creating root\n");
    //root create
    struct pseudo_inode root{};
    root.nodeid = 0;
    root.isDirectory = true;
    root.references = 0;
    root.file_size = sb.cluster_size;
    //printf("filling inode space\n");
    //fill inode space with 0
    uint8_t a = 0;
    fwrite(&root, sizeof(pseudo_inode), 1, f);
    for (int i = sizeof(pseudo_inode); i < sb.data_start_address - sb.inode_start_address; i++) {
        fwrite(&a, sizeof(uint8_t), 1, f);
    }
    //makce custom directory items for root and then fill the rest of dataspace with 0
    struct directory_item tecka{0, "."};
    struct directory_item dvojtecka{0, ".."};
    fwrite(&tecka, sizeof(directory_item), 1, f);
    fwrite(&dvojtecka, sizeof(directory_item), 1, f);
    //printf("filling data space\n");
    for (int i = 0; i < ((sb.cluster_size / sizeof(directory_item)) - 2); i++) {
        fwrite(&a, sizeof(uint8_t), 1, f);
    }
    for (int i = sb.cluster_size / sizeof(directory_item); i < sb.disk_size - sb.data_start_address; i++) {
        fwrite(&a, sizeof(uint8_t), 1, f);
    }
    currentNodeId = root.nodeid;
    fclose(f);
    printf("OK\n");
    return true;
    //inode index0 - diritemy . : na 0 zapsat inode a dat, zapsat spoustu nul o spravne velikosti dat a inode
}

//Vytvoří symbolický link na soubor s1 s názvem s2. Dále se s ním pracuje očekávaným způsobem, tedy např. cat s2 vypíše obsah souboru s1.
bool slink(vector<string> v) {
    f = fopen(fileName.c_str(), "rb+");
    string s1 = v[1];
    string s2 = v[2];
    pseudo_inode originInode;
    pseudo_inode destInode;
    pseudo_inode prevInode;
    pseudo_inode oldFileInode;
    vector<string> pathOrigin = tokenize(s1);
    vector<string> pathDest = tokenize(s2);
    //presun origin inode do mista odkud kopiruji
    if (s1[0] == '/' || s1[0] == '\\') {
        //absolute route - dostan se na root
        fseek(f, sb.inode_start_address, SEEK_SET);
        fread(&originInode, sizeof(pseudo_inode), 1, f);
    } else {
        //relative route - presun se do aktualni slozky ve ktere uzivatel je
        fseek(f, sb.inode_start_address + currentNodeId * sizeof(pseudo_inode), SEEK_SET);
        fread(&originInode, sizeof(pseudo_inode), 1, f);
    }
    //postupne se zanoruj
    if (pathOrigin.size() > 1) {
        for (int i = 0; i < pathOrigin.size() - 1; i++) {
            prevInode = originInode;
            originInode = findInode(originInode, pathOrigin[i]);
            //printf("%s\n",pathOrigin[i].c_str());
            if (prevInode.nodeid == originInode.nodeid) {
                printf("PATH NOT FOUND");
                return false;
            }
        }
    }
    //ziskam inode souboru
    oldFileInode = findInode(originInode, pathOrigin[pathOrigin.size() - 1]);
    if (oldFileInode.nodeid == originInode.nodeid) {
        printf("FILE NOT FOUND");
        return false;
    }
    //zopakovani pro misto kam kopiruji
    if (s2[0] == '/' || s2[0] == '\\') {
        fseek(f, sb.inode_start_address, SEEK_SET);
        fread(&destInode, sizeof(pseudo_inode), 1, f);
    } else {
        fseek(f, sb.inode_start_address + currentNodeId * sizeof(pseudo_inode), SEEK_SET);
        fread(&destInode, sizeof(pseudo_inode), 1, f);
    }
    if (pathDest.size() > 1) {
        for (int i = 0; i < pathDest.size() - 1; i++) {
            prevInode = destInode;
            destInode = findInode(destInode, pathDest[i]);
            if (prevInode.nodeid == destInode.nodeid) {
                printf("PATH NOT FOUND");
                return false;
            }
        }
    }
    //kouknu na cestu, zjistim jmeno noveho souboru
    char nameOfNewFile[12];
    memset(&nameOfNewFile, 0, 12);
    string s = pathDest[pathDest.size() - 1];
    string token = s.substr(0, s.find('.'));
    strncpy(nameOfNewFile, token.c_str(), min(7, (int) s.size()));
    string nazev = nameOfNewFile;
    s = pathDest[pathDest.size() - 1];
    token = s.substr(s.rfind('.') + 1, s.size() - (s.rfind('.') + 1));
    string pripona = token;
    string fname = nazev+"."+pripona;
    //strncpy(nameOfNewFile + 8, pripona.c_str(), min(3, (int) pripona.size()));
    strncpy(nameOfNewFile, fname.c_str(), 12);
    nameOfNewFile[11] = '\0';

    //chci zaridit aby inody dest a origin byli stejné
    pseudo_inode slink = oldFileInode;
    /*slink.nodeid = originInode.nodeid;
    slink.file_size = originInode.file_size;
    slink.isDirectory = originInode.isDirectory;
    slink.references = originInode.references;
    slink.direct1 = originInode.direct1;
    slink.direct2 = originInode.direct2;
    slink.direct3 = originInode.direct3;
    slink.direct4 = originInode.direct4;
    slink.direct5 = originInode.direct5;
    slink.indirect1 = originInode.indirect1;
    slink.indirect2 = originInode.indirect2;*/

    //inode bitmap zapis nove polozky
    uint8_t inode_bitmap[sb.bitmap_start_address - sb.bitmapi_start_address];
    fseek(f, sb.bitmapi_start_address, SEEK_SET);
    fread(&inode_bitmap, sizeof(inode_bitmap), 1, f);
    bool emptyFound = false;
    int pseudoId;
    //projeď všechny skupiny dat po 8 v inode bitmap
    for (int i = 0; i < sizeof(inode_bitmap); i++) {
        //v každé skupině koukni na prvky
        for (int j = 7; j >= 0; j--) {
            //když najdeš 0, tak je to tam volné a zapiš tam
            if (((inode_bitmap[i] >> j) & 0b1) == 0) {
                //volné místo nalezeno
                emptyFound = true;
                fseek(f, sb.bitmapi_start_address + sizeof(inode_bitmap[i]) * i, SEEK_SET);
                inode_bitmap[i] += pow(2, j);
                //uloz 1 do inode bitmap na nalezene misto
                fwrite(&inode_bitmap[i], sizeof(inode_bitmap[i]), 1, f);
                slink.nodeid = i * 8 + (7 - j);
                //pseudoId = i * 8 + (7 - j);
            }
            if (emptyFound)break;
        }
        if (emptyFound)break;
    }
    //zapsani samotne inode
    fseek(f, sb.inode_start_address + slink.nodeid* sizeof(pseudo_inode), SEEK_SET);
    fwrite(&slink, sizeof(pseudo_inode), 1, f);

    //zapsani inodu do nadrazene slozky
    struct directory_item child;
    child.inode = slink.nodeid;
    memcpy(child.item_name, nameOfNewFile, 12);

    //1) najit kolik podslozek a souboru pod sebou nadrazena slozka ma - kontrola zda ma slozka jeste misto
    struct directory_item dirItems[sb.cluster_size / sizeof(directory_item)];
    fseek(f, sb.data_start_address + sb.cluster_size * destInode.direct1, SEEK_SET);
    fread(&dirItems, sb.cluster_size, 1, f);
    int m;
    //najdi v diritems me nadslozky volny misto pro child
    //todo pokud se dostanu na konec foru, tak mam plnou pamet slozky... mozna by stalo za to to osetrit.... neeee!
    //printf("%d | %d | %d\n",(sb.cluster_size / sizeof(directory_item)),sb.cluster_size, currInode.direct1);
    for (m = 0; m < (sb.cluster_size / sizeof(directory_item)); m++) {
        //printf("for %s\n",dirItems[m].item_name);
        if (dirItems[m].inode == 0 && !strcmp(dirItems[m].item_name, ""))break;
    }
    //2) zapsat
    fseek(f, sb.data_start_address + sb.cluster_size * destInode.direct1 +
             m * sizeof(directory_item), SEEK_SET);
    fwrite(&child, sizeof(directory_item), 1, f);
    emptyFound = true;

    printf("OK\n");
    fclose(f);
    return true;
}