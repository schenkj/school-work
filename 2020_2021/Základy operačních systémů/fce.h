#include "main.h"
#include <string>
#include <vector>



using namespace std;

//Zkopíruje soubor s1 do umístění s2
bool copyF(vector<string> vector);

//Přesune soubor s1 do umístění s2, nebo přejmenuje s1 na s2
bool moveF(vector<string> vector);

//Smaže soubor s1
bool delF(vector<string> vector);

//Vytvoří adresář a1
bool createDir(vector<string> vector);

//Smaže prázdný adresář a1
bool delDir(vector<string> vector);

//Vypíše obsah adresáře a1
bool listDir(vector<string> vector);

//Vypíše obsah souboru s1
bool listF(vector<string> vector);

//Změní aktuální cestu do adresáře a1
bool move(vector<string> v);

//Vypíše aktuální cestu
bool path();

//Vypíše informace o souboru/adresáři s1/a1 (v jakých clusterech se nachází)
bool info(vector<string> vector);

// Nahraje soubor s1 z pevného disku do umístění s2 v pseudoNTFS
bool outcp(vector<string> vector);

//Nahraje soubor s1 z pseudoNTFS do umístění s2 na pevném disku
bool incp(vector<string> vector);

//Načte soubor z pevného disku, ve kterém budou jednotlivé příkazy, a začne je sekvenčně vykonávat. Formát je 1 příkaz/1řádek
bool loadBuild(vector<string> vector);

//Příkaz provede formát souboru, který byl zadán jako parametr při spuštení programu na souborový systém dané velikosti. Pokud už soubor nějaká data obsahoval, budou přemazána. Pokud soubor neexistoval, bude vytvořen.
bool format(vector<string> input);

//Vytvoří symbolický link na soubor s1 s názvem s2. Dále se s ním pracuje očekávaným způsobem, tedy např. cat s2 vypíše obsah souboru s1.
bool slink(vector<string> vector);

//rozdeleni stringu
vector<string> tokenize(string s);

//najde adresar podle jmena ve složce kde je
pseudo_inode findInode(pseudo_inode start, const string &findName);