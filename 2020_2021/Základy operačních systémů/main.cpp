#include <stdio.h>
#include "main.h"
#include "fce.h"
#include <iostream>
#include <cstring>
#include <sstream>
#include <iterator>
#include <fstream>
#include <string.h>


using namespace std;

FILE * f;
std::string fileName;
superblock sb;
int currentNodeId = 0;


void fill_sb(int size){
// add disk_size param
strcpy(sb.signature, "== ext ==");
strcpy(sb.volume_descriptor, "superblock podle ukazky");
sb.disk_size = size;
if(size>(10*pow(2,20)))sb.cluster_size = 10*pow(2,10);//10kB
else if(size<100*pow(2,10))sb.cluster_size = 128;//128B - 8x directory item
else sb.cluster_size = pow(2,10);//1kB
//sb->cluster_size = sb->disk_size/(100 * pow(2,10)); //100MB -> 1kB etc.
//if(sb->cluster_size < 128){sb->cluster_size=128;}   //not smaller then 8x directory_item
sb.cluster_count = (size*9/10)/sb.cluster_size;    //90% of disk

}
//zpracovani vstupu uzivatele
void fakeSwitch(vector<string> vector) {
    if(vector[0] == "cp" && vector.size() == 3){copyF(vector);}
    else if(vector[0] == "format" && vector.size() == 2){format(vector);}
    else if(vector[0] == "mv" && vector.size() == 3){moveF(vector);}
    else if(vector[0] == "rm" && vector.size() == 2){delF(vector);}
    else if(vector[0] == "mkdir" && vector.size() == 2){createDir(vector);}
    else if(vector[0] == "rmdir" && vector.size() == 2){delDir(vector);}
    else if(vector[0] == "ls" && vector.size() == 2){listDir(vector);}
    else if(vector[0] == "cat" && vector.size() == 2){listF(vector);}
    else if(vector[0] == "cd" && vector.size() == 2){ move(vector);}
    else if(vector[0] == "pwd" && vector.size() == 1){path();}
    else if(vector[0] == "info" && vector.size() == 2){info(vector);}
    else if(vector[0] == "incp" && vector.size() == 3){incp(vector);}
    else if(vector[0] == "outcp" && vector.size() == 3){outcp(vector);}
    else if(vector[0] == "load" && vector.size() == 2){loadBuild(vector);}
    else if(vector[0] == "slink" && vector.size() == 3){slink(vector);}
    else {
        cout << "Unknown command, check number of parametrs and spelling.\n";
    }
}

int main(int argc, char **argv) {
    if(argc<1){cout<<"Invalid number of arguments."<<endl;return 0;}
    string name = argv[1];
    sb = {};
    string fc;
    fileName = name;
    //nacti si filesystem, nebo napis uzivateli at zavola form
    FILE * f = fopen(fileName.c_str(),"rb+");
    if(f!=nullptr){
        //nacti superblock
        fread(&sb, sizeof(superblock), 1, f);
        printf("filesystem found - %d\n",sb.disk_size);
        //cout<<sb.disk_size<<endl;
        fclose(f);
    }
    else printf("Filesystem not found, begin with format command.\n");
    //smycka cekajici na vstupy od uzivatele
    while(true){
        string str;
        getline(cin, str);

        if(str.empty())break;

        string s = str;
        stringstream ss(s);
        istream_iterator<string> begin(ss);
        istream_iterator<string> end;
        vector<string> input(begin, end);
        //std::copy(input.begin(), input.end(), std::ostream_iterator<std::string>(std::cout, "\n"));
        fakeSwitch(input);
    }
    return 0;
}

