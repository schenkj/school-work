import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

class writeThread extends Thread {
    public BlockingQueue<String> bq = new ArrayBlockingQueue<>(1024);
    public void run(){
        String s;
        while(true){

            try {
                s = bq.take();
                client.oos.write(s);
                client.oos.flush();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }


        }
    }

}

class readThread extends Thread{
    public void run(){
        String in = "";

        while(true){
            boolean validMessage = false;
            try {in = client.ois.readLine();}
            catch (IOException e) {System.err.println("Server connection lost.");}

            System.out.println("server send: \""+in+"\"");


            //name read
            if (in.contains("Hi ")) {
                validMessage = true;
                if (in.contains("(")) {
                    //jmeno bylo zmeneno a prijato
                    //nacti dostupne mistnosti

                    System.out.println(in);
                } else {
                    //jmeno nebylo zmeneno a bylo prijato
                    System.out.println(in);
                }
                client.wt.bq.add("list\n");
                String lenS = "Hi ";
                client.myName = in.substring(lenS.length(),in.length());
                //System.out.println(client.myName);
            }
            if(in.contains("This username is ")){
                validMessage = true;
                System.out.println("here");
                client.frame1 = client.loginF();
                client.frame2.dispose();
                client.frame3.dispose();
                client.frame1.setVisible(true);
            }

            //list read
            if (in.contains("list")) {
                validMessage = true;
                in = in.substring(4,in.length());
                String sa[] = in.split("\n");
                String s;
                client.rooms.clear();
                for(int i = 0; i < sa.length; i++) {
                    s = sa[i];
                    if(s.contains("list"))s=s.substring(4,s.length());
                    System.out.println("reading list: " + s);
                    if (s.equals("0")) break;
                    JLabel label = new JLabel(s);
                    String finalS = s;
                    label.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            //System.out.println("mouse clicked string: "+finalS);
                            System.out.println(finalS);
                            int n = Integer.parseInt(finalS.substring(5,6));
                            System.out.println(n);
                            System.out.println("joining room "+n);
                            System.out.println("String: "+finalS);
                            client.chosenRoom = n;
                        }
                    });
                    client.rooms.add(label);
                    System.out.println("label added");
                }
                client.list = new JPanel();

                for(int i = 0; i < client.rooms.size(); i++){
                    client.list.add(client.rooms.get(i));
                }
                client.frame2.getContentPane().add(BorderLayout.CENTER, client.list);
                client.frame2.setVisible(true);
                //System.out.println("end of list block here");

            }

            //create
            if(in.contains("Room created")){
                validMessage = true;
                JOptionPane.showMessageDialog(client.frame2, "Room succesfully created. Wait for the second player to start.");
                JPanel p = new JPanel();
                client.frame2.getContentPane().add(p,BorderLayout.CENTER);
            }

            //join
            if(in.contains("Joined")){
                validMessage = true;
                JOptionPane.showMessageDialog(client.frame2, "You have joined the room. Start button enabled.");
                JPanel p = new JPanel();
                client.frame2.getContentPane().add(p,BorderLayout.CENTER);
            }
            if(in.contains("Player joined")){
                validMessage = true;
                JOptionPane.showMessageDialog(client.frame2, "Second player joined, you can start the game");
                client.start.setEnabled(true);
            }

            //start
            if(in.contains("Starting")){
                validMessage = true;
                client.frame3.dispose();
                client.frame3 = client.gameF();
                client.frame3.setVisible(true);
            }

            //move
            if(in.contains("Move result")){
                validMessage = true;
                //"Move result (jmeno): " <- vypsat jmeno
                String name = in.substring(13,in.length()-13);
                int move = -1;
                try{
                    move = Integer.parseInt(in.substring(in.length()-1,in.length()));
                }
                catch(NumberFormatException e){
                    System.out.println("not valid move");
                }
                //System.out.println(in+" | "+name+" | "+move);


                if(!name.equals(client.myName) && move!=-1){
                    System.out.println(name+"|"+client.myName);
                    client.move(move, 2);
                }

            }

            //leave
            if(in.contains("You have")){
                validMessage = true;
                JOptionPane.showMessageDialog(client.frame2, "You have left the room.");
                //list
                in = in.substring(4,in.length());
                String[] sa = in.split("\n");
                String s;
                client.rooms.clear();
                for(int i = 0; i < sa.length; i++) {
                    s = sa[i];
                    System.out.println("reading list: " + s);
                    if (s.equals("0")) break;
                    JLabel label = new JLabel(s);
                    String finalS = s;
                    label.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            int n = Integer.parseInt(finalS.substring(4,5));
                            System.out.println("joining room "+n);
                            //System.out.println("String: "+finalS);
                            client.chosenRoom = n;
                        }
                    });
                    client.rooms.add(label);
                    System.out.println("label added");
                }
                for(int i = 0; i < client.rooms.size(); i++){
                    client.list.add(client.rooms.get(i));
                }
                client.frame2.repaint();
                if(client.frame3.isActive())client.frame3.dispose();
            }

            //not your turn (in game)
            if(in.contains("Not your")){
                validMessage = true;
                JOptionPane.showMessageDialog(client.frame3, "Not your turn, wait for the other player\'s respond.");
                int l = in.length();

                int n = Integer.parseInt(in.substring(l-2,l-1));
                int p = Integer.parseInt(in.substring(l-1,l));
                //System.out.println("Error "+in);
                client.move(n,p);
            }

            //join into non selected room
            if(in.contains("Room wasn")){
                validMessage = true;
                JOptionPane.showMessageDialog(client.frame2, "You didn\'t select room. Click on room and then press join.");
                client.join.setEnabled(true);
                client.start.setEnabled(false);
                client.leave.setEnabled(false);
                client.refresh.setEnabled(true);
                client.create.setEnabled(false);
            }

            //draw
            if(in.contains("Last turn done")){
                validMessage = true;
                JOptionPane.showMessageDialog(client.frame2, "Draw!");
                client.frame3.dispose();
                client.frame3 = client.gameF();
                //client.frame3.setVisible(false);
                client.frame3.setVisible(true);
            }

            //win or loss
            if(in.contains("Winning move done, player ")){
                validMessage = true;
                String lenS = "Winning move done, player";
                String lenS2 = " has won.";
                String name = in.substring(lenS.length(), in.length()-lenS2.length());
                JOptionPane.showMessageDialog(client.frame2, "Player "+name+" has won!");
                client.frame3.dispose();
                client.frame3 = client.gameF();
                //client.frame3.setVisible(false);
                client.frame3.setVisible(true);
            }

            //join not-existing room
            if(in.contains("Room unvalid, ")){
                validMessage = true;
                JOptionPane.showMessageDialog(client.frame2, "This room doesn\'t exist, refresh and try again");
                client.join.setEnabled(true);
                client.start.setEnabled(false);
                client.leave.setEnabled(false);
                client.refresh.setEnabled(true);
                client.create.setEnabled(false);
            }

            //reconnect
            if(in.contains("Reconnecting ")){
                validMessage = true;
                int l = "Reconnecting ".length();
                JOptionPane.showMessageDialog(client.frame2, "Reconnecting...");
                String gs = in.substring(in.length()-9,in.length());
                client.myName = in.substring(0,in.length()-l-10);
                boolean converted = false;
                if(in.substring(in.length()-l-10,in.length()-l-9).equals("2"))converted=true;
                //System.out.println(gs+"|"+gs.length());
                client.frame3 = client.gameF();
                for(int i = 0; i < gs.length(); i++){
                    int p = Integer.parseInt(gs.substring(i,i+1));
                    if(converted && p==1)p=2;
                    else if(converted && p==2)p=1;
                    client.move(i,p);
                }
                client.frame3.setVisible(true);
            }

            //join not-existing room
            if(in.contains("Your opponent")){
                validMessage = true;
                JOptionPane.showMessageDialog(client.frame2, in);
            }
            if(in.contains("Didnt recognise")){
                validMessage = true;
                JOptionPane.showMessageDialog(null, in);
            }
            if(in.contains("Give me your ")){
                validMessage = true;
                System.out.println("Server asked for nickname");
            }
            if(!validMessage){
                JOptionPane.showMessageDialog(null,"Server send unvalid message - shutting down!");
                System.exit(3);
            }

        }
    }
}

public class client {
    public static String name;
    public static boolean showGame = false;
    public static ArrayList<JButton> gameboard = new ArrayList<JButton>();
    public static final Icon x = new ImageIcon("./x.png");
    public static final Icon o = new ImageIcon("./o.png");
    public static final Icon d = new ImageIcon("./d.png");
    public static int chosenRoom=-1;

    public static ArrayList<JLabel> rooms = new ArrayList<JLabel>();

    public static Socket socket = null;

    public static JButton start;
    public static JButton join;
    public static JButton create;
    public static JButton refresh;
    public static JButton leave;

    public static JPanel list;

    public static JButton n0;
    public static JButton n1;
    public static JButton n2;
    public static JButton n3;
    public static JButton n4;
    public static JButton n5;
    public static JButton n6;
    public static JButton n7;
    public static JButton n8;

    public static String myName;



    public static BufferedReader ois = null;
    public static PrintWriter oos = null;

    public static writeThread wt;
    public static readThread rt;

    public static JFrame frame1;
    public static JFrame frame3;
    public static JFrame frame2;

    public static void main(String argv[]) throws Exception {

        String ip;
        String p;
        if(argv.length<2){
            ip="192.168.56.102";
            p="1234";
            System.out.println("IPv4 or port not entered - trying default.");
        }
        else{
            ip = argv[0];
            p = argv[1];
        }

        if(!validIP(ip) && !ip.equals("localhost")){
            System.err.println("Faulty ipv4 address\n");
            return;
        }
        String IPv4 = ip;
        String port = p;
        int po = 1234;
        for(int i = 0; i < p.length(); i++){
            try{po = Integer.parseInt(port);}
            catch(NumberFormatException e){
                System.err.println("Port is not a digit\n");
                return;
            }
        if(po>65535)po = 1234;

        }
        System.out.println(IPv4+"|"+po);
        try{
            socket = new Socket(IPv4, po);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Cannot reach the server - closing");
            return;
        }
        /*try {
            socket = new Socket(IPv4, po);
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        InetAddress adresa = socket.getInetAddress();
        System.out.print("Pripojuju se na : " + adresa.getHostAddress() + " se jmenem : " + adresa.getHostName() + "\n");



        //inicializace globalnich promennych
        try {
            oos = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()));
            ois = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        rt = new readThread();
        wt = new writeThread();
        rt.start();
        wt.start();
        /*os.writeObject("HalloXXXX");
        String message = (String) ois.readObject();*/

        frame1 = loginF();
        frame1.setVisible(true);


    }

    public static boolean validIP (String ip) {
        try {
            if ( ip == null || ip.isEmpty() ) {
                return false;
            }

            String[] parts = ip.split( "\\." );
            if ( parts.length != 4 ) {
                return false;
            }

            for ( String s : parts ) {
                int i = Integer.parseInt( s );
                if ( (i < 0) || (i > 255) ) {
                    return false;
                }
            }
            if ( ip.endsWith(".") ) {
                return false;
            }

            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    static void move(int move, int player) {
        //System.out.println("move: "+move+"|-| player: "+player);
        if (player == 1) {
            gameboard.get(move).setIcon(x);
        } else if(player == 0){
            gameboard.get(move).setIcon(d);
        } else {
            gameboard.get(move).setIcon(o);
        }
    }
    static JFrame gameF() {

        JFrame frame = new JFrame("TicTacToe");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.setSize(600, 600);
        gameboard.clear();
        frame.setLayout(new GridLayout(3, 3, 25, 25));
        n0 = new JButton("");
        n0.setIcon(d);
        n0.setPreferredSize(new Dimension(150, 150));
        n1 = new JButton("");
        n1.setIcon(d);
        n1.setPreferredSize(new Dimension(150, 150));
        n2 = new JButton("");
        n2.setIcon(d);
        n2.setPreferredSize(new Dimension(150, 150));
        n3 = new JButton("");
        n3.setIcon(d);
        n3.setPreferredSize(new Dimension(150, 150));
        n4 = new JButton("");
        n4.setIcon(d);
        n4.setPreferredSize(new Dimension(150, 150));
        n5 = new JButton("");
        n5.setIcon(d);
        n5.setPreferredSize(new Dimension(150, 150));
        n6 = new JButton("");
        n6.setIcon(d);
        n6.setPreferredSize(new Dimension(150, 150));
        n7 = new JButton("");
        n7.setIcon(d);
        n7.setPreferredSize(new Dimension(150, 150));
        n8 = new JButton("");
        n8.setIcon(d);
        n8.setPreferredSize(new Dimension(150, 150));

        gameboard.add(n0);
        gameboard.add(n1);
        gameboard.add(n2);
        gameboard.add(n3);
        gameboard.add(n4);
        gameboard.add(n5);
        gameboard.add(n6);
        gameboard.add(n7);
        gameboard.add(n8);
        int i;
        for (i = 0; i < gameboard.size(); i++) {

            //System.out.println("hodnota na tlacitku: "+i+" ------------------ gameboardsize: "+gameboard.size());
            int finalI = i;
            //funkcnost tlacitek
            gameboard.get(i).addActionListener(e -> {
                wt.bq.add("move/" + finalI+"\n");
                System.out.println("move/" + finalI);
                move(finalI, 1);
            });
        }

        frame.add(n0);
        frame.add(n1);
        frame.add(n2);
        frame.add(n3);
        frame.add(n4);
        frame.add(n5);
        frame.add(n6);
        frame.add(n7);
        frame.add(n8);

        frame.pack();
        System.out.println("game frame created");
        return frame;
    }

    private static JFrame lobbyF(JFrame f3) throws IOException, ClassNotFoundException {
        //lobby

        JFrame frame2 = new JFrame("Lobby");
        frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame2.setSize(600, 400);

        frame2.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                oos.close();
                try {
                    ois.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                try {
                    socket.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
                frame2.dispose();
            }
        });


        //Creating the panel at bottom and adding components
        JPanel panel = new JPanel(); // the panel is not visible in output
        join = new JButton("Join");
        create  = new JButton("CREATE");
        leave  = new JButton("leave");
        start  = new JButton("START");
        refresh  = new JButton("refresh");

        join.addActionListener(e -> {
            wt.bq.add("join/"+chosenRoom+"\n");

            System.out.println("join");
            join.setEnabled(false);
            start.setEnabled(true);
            leave.setEnabled(true);
            refresh.setEnabled(false);
            create.setEnabled(false);
        });

        refresh.addActionListener(e -> {
            //resetuj seznam mistnosti a znovu jej nacti

            wt.bq.add("list\n");

            System.out.println("here");


            System.out.println("list");
            join.setEnabled(true);
            start.setEnabled(false);
            leave.setEnabled(false);
            refresh.setEnabled(true);
            create.setEnabled(true);
        });

        start.addActionListener(e -> {
            wt.bq.add("start\n");

            System.out.println("start");
            f3.setVisible(true);
            join.setEnabled(false);
            start.setEnabled(false);
            leave.setEnabled(true);
            refresh.setEnabled(false);
            create.setEnabled(false);
        });
        start.setEnabled(false);

        leave.addActionListener(e -> {
            wt.bq.add("leave\n");

            System.out.println("leave");
            leave.setEnabled(false);
            start.setEnabled(false);
            create.setEnabled(true);
            refresh.setEnabled(true);
            join.setEnabled(true);
        });
        leave.setEnabled(false);


        create.addActionListener(e -> {
            wt.bq.add("create\n");

            System.out.println("create");
            //schovani nemoznych akci a odhaleni nove moznych
            refresh.setEnabled(false);
            join.setEnabled(false);
            leave.setEnabled(true);
            start.setEnabled(false);
            create.setEnabled(false);
        });

        panel.add(create);
        panel.add(join);
        panel.add(refresh);

        panel.add(start);

        panel.add(leave);

        // List of rooms at the Center


        System.out.println(rooms.size());
        list = new JPanel();
        for (int i = 0; i < rooms.size(); i++) {
            list.add(rooms.get(i));
        }


        //Adding Components to the frame2.
        frame2.getContentPane().add(BorderLayout.SOUTH, panel);
        frame2.getContentPane().add(BorderLayout.CENTER, list);

        System.out.println("lobby frame created");
        return frame2;
    }

    public static JFrame loginF() {
        //login
        JFrame frame1 = new JFrame("Login");
        frame1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame1.setSize(400, 150);

        JPanel panel1 = new JPanel();
        JLabel label1 = new JLabel("Username:");
        JTextField nameField = new JTextField(32);

        JButton submit = new JButton("Submit");
        submit.addActionListener(e -> {
            System.out.println(nameField.getText());
            name = nameField.getText();

            frame3 = gameF();
            try {
                frame2 = lobbyF(frame3);
            } catch (IOException | ClassNotFoundException e1) {
                e1.printStackTrace();
            }

            wt.bq.add(name+"\n");


            //nezobrazuj frame2 dokud se nevyridi frame1
            frame1.dispose();

            frame2.setVisible(true);
        });
        panel1.add(label1); // Components Added using Flow Layout
        panel1.add(nameField);
        panel1.add(submit);
        frame1.getContentPane().add(BorderLayout.CENTER, panel1);

        System.out.println("login frame created");
        return frame1;
    }

}

