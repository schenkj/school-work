#include <cstdio> // perror, printf
#include <cstdlib> // exit, atoi
#include <pthread.h>
#include <unistd.h> // read, write, close

#ifdef WIN32
#include <winsock2.h>
#else

#include <arpa/inet.h> // sockaddr_in, AF_INET, SOCK_STREAM, INADDR_ANY, socket etc...

#endif

#include <cstring> // memset
#include <string>
#include <vector>
#include <list>
#include <iostream>
#include "main.h"


using namespace std;

list<room *> roomList;
list<string> nameList;
list<string> onlineList;

//todo list jmen klientu -> menit id v roomce po reconnectu, zakazat duplitu jmen, ...


//boolean jestli je hra ve stavu vhry jednoho z hracu
bool vyhra(room *r) {
    int s[9];
    for (int i = 0; i < 9; i++) {
        s[i] = r->gameboard[i];
        cout << s[i];
    }
    cout<<endl;
    //v nejake rade, sloupci, nebo uhlopricce jsou vsechny symboly shodne a nejsou to prazdna pole
    return (//rady
            (s[0] == s[1] && s[0] == s[2] && s[0] != 0) ||
            (s[3] == s[4] && s[3] == s[5] && s[3] != 0) ||
            (s[6] == s[7] && s[6] == s[8] && s[6] != 0) ||
            //sloupce
            (s[0] == s[3] && s[0] == s[6] && s[0] != 0) ||
            (s[1] == s[4] && s[1] == s[7] && s[1] != 0) ||
            (s[2] == s[5] && s[2] == s[8] && s[2] != 0) ||
            //diagonaly
            (s[0] == s[4] && s[0] == s[8] && s[0] != 0) ||
            (s[2] == s[4] && s[2] == s[6] && s[2] != 0)
            );
}

//return 0 pokud se jednalo o regulerni tah, 1 pokud se jednalo o vyherni tah, 2 pokud se jednalo o posledni tah a 3 v pripade chyby
//game* zkusTah(game *gameState) {
void zkusTah(room *gameState) {
    int hrac = gameState->hrac;
    int tah = gameState->move;
    //je validni? jestli ne, return 1;
    //tah je hran na herni desce a bylo zvoleno pole, ktere je prazdne
    if (tah < 9 && gameState->gameboard[tah] == 0) {
        //zmena stavu hry -> pridani vysledku tahu hrace (na pozici kam chtel byl zapsan jeho znak)
        gameState->gameboard[tah] = hrac;
        //je vyherni?
        if (vyhra(gameState)) {
            cout << "vyherni tah" << endl;
            gameState->ret = 1;
        }
            //je posledni?
        else {
            gameState->ret = 2;
            for (int i : gameState->gameboard) {
                if (i == 0) {
                    //neni posledni
                    cout << "regulerni tah" << endl;
                    gameState->ret = 0;
                    break;
                }
            }
            if (gameState->ret == 2) { cout << "remizujici tah" << endl; }
        }
    } else {
        cout << "tah neni validni, zkus to prosim znovu" << endl;
        gameState->ret = 3;
    }
    //return gameState;
}

room* klientNecoPoslal(string s, int id, string name, room *r) {
    //odstraneni ze stringu posledni znak "\n"
    if (!s.empty())s.pop_back();
    //cout << "start of message ||" << s << "|| end of message" << endl;
    string pName = name;
    r->ret = -1;

    //split incomming string
    std::vector<std::string> vec{};
    string delimetr = "/";
    string str;
    size_t pos;
    string token;
    bool var = false;

    while ((pos = s.find(delimetr)) != string::npos) {
        token = s.substr(0, pos);
        vec.push_back(token);
        s.erase(0, pos + delimetr.length());
        var = true;
    }
    vec.push_back(s);
    if(!var){
        vec.push_back("");
    }
    if (vec[0] == "create") {
        cout << "creating room" << endl;
        //vytvor room
        r = new struct room;//(room *)(malloc(sizeof(room)));
        r->id1 = id;
        r->id2 = -1;
        r->h1 = pName;
        r->h2 = "";
        if(roomList.size()>0){r->idRoom = roomList.back()->idRoom+1;}
        else {r->idRoom = 0;}
        r->ready = false;


        //vytvor instance hry pred zapnutim
        //reset hry
        memset(r->gameboard, 0, sizeof(r->gameboard));
        r->ret = -1;
        r->move = -1;
        r->hrac = 0;
        roomList.push_back(r);

        cout << "Room created " << r->idRoom << endl;
        string printString = "Room created " + to_string(r->idRoom)+"\n";
        if (write(id, printString.c_str(), printString.size()) < 0) {
            perror("write error");
            r->ret = -6;
        }
    }

    else if (vec[0] == "list") {
        //print info about listing rooms
        string printString = "list";
        /*if (write(id, printString.c_str(), printString.size()) < 0) {
            perror("write error");
            r->ret = -6;
        }*/
        cout << "listing rooms "<<roomList.size() << endl;
        if(roomList.size()<1){
            cout<<"empty list"<<endl;
            printString += "0\n";
            if (write(id, printString.c_str(), printString.size()) < 0) {
                perror("write error");
                r->ret = -6;
            }
            r->ret = -10;
            return r;
        }
        //list all available rooms
        for (const auto &v : roomList) {
            if (v->h2.empty() || v->id2 < 0) {
                cout << "Room " << v->idRoom << " with waiting player " << v->h1 << endl;

                printString += "listRoom " + to_string(v->idRoom);
                printString += " with waiting player " + v->h1 + "\n";
                r->ret-2;
            }
        }
        if(printString == "list"){
            printString += "0\n";
        }
        if (write(id, printString.c_str(), printString.size()) < 0) {
            perror("write error");
            r->ret = -6;
        }
    }

    else if (vec[0] == "join") {
        if(vec[1]==""){
            r->ret = -7;
            string printString = "Room wasn\'t selected, try again.";
            cout << printString << endl;
            if (write(id, printString.c_str(), printString.size()) < 0) {
                perror("write error");
                r->ret = -6;
            }
            return r;
        }
        cout << "trying to connect to room" << endl;
        //pripojit se ke hre
        int desiredRoom = atoi(vec[1].c_str());
        for (auto v : roomList) {
            if (v->idRoom != desiredRoom) { continue; }
            if(v->h1 == "" || v->id1<0){
                string printString = "Room unvalid, refresh and try again please.";
                cout << printString << endl;
                if (write(id, printString.c_str(), printString.size()) < 0) {
                    perror("write error");
                    r->ret = -6;
                }
                continue;
            }
            if (v->h2.empty() || v->id2 < 0) {
                v->h2 = pName;
                v->id2 = id;
                v->ready = true;
                r = v;

                cout << "joined " << r->idRoom << "... press Start" << r->idRoom << r->ready << endl;

                string printString = "Joined ";
                if (write(v->id2, printString.c_str(), printString.size()) < 0) {
                    perror("write error");
                    r->ret = -6;
                }
                printString = "Player joined your room: " + pName + "\n";
                if (write(v->id1, printString.c_str(), printString.size()) < 0) {
                    perror("write error");
                    r->ret = -6;
                }
                break;
            }
        }
    }

    else if (vec[0] == "start") {
        //cout << "start called" << r->idRoom << " ingame: "<<r->inGame<<" ready: " << r->ready <<endl;

        if (r->idRoom > -1 && r->inGame==0 && r->ready) {
            //mam room ktera zavolala start
            r->inGame = 1;
            r->ready = false;
            cout << "starting" + to_string(r->idRoom) << endl;

            string printString = "Starting\n";
            if (write(r->id1, printString.c_str(), printString.size()) < 0) {
                perror("write error");
                r->ret = -6;
            }
            if (write(r->id2, printString.c_str(), printString.size()) < 0) {
                perror("write error");
                r->ret = -6;
            }

            //reset hry
            memset(r->gameboard, 0, sizeof(r->gameboard));
            r->ret = -1;
            r->move = -1;
            r->hrac = 0;


        }
    }

    else if (vec[0] == "move") {
        // nelze tahnout pokud nejsi na tahu
        int kdoJeNaTahu = 0;
        for (int i : r->gameboard) {
            if (i == 1)kdoJeNaTahu++;
            else if (i == 2)kdoJeNaTahu--;
        }
        //cout<<"here1"<<endl;
        //jsem na tahu?
        if ((kdoJeNaTahu == 0 && id == r->id1) || (kdoJeNaTahu == 1 && id == r->id2)) {
            int move = atoi(vec[1].c_str());
            int hrac;
            string jmenoHrace;
            if (id == r->id1) {
                hrac = 1;
                jmenoHrace = r->h1;
            }
            else {
                hrac = 2;
                jmenoHrace = r->h2;
            }
            r->move = move;
            r->hrac = hrac;
            r->ret = -1;
            //pokus o provedeni tahu i s validaci
            //v resultu je novy gamestate a podle hodnoty .ret lze zjistit jak tah dopadl
            //0 - regulerni tah, 1 - vyherni tah, 2 - posledni remizujici tah, 3 - nevalidni tah
            //gs = *zkusTah(&gs);
            zkusTah(r);
            cout << "gamestate return: "<<r->ret << endl;

            string printString;
            printString = "Move result ("+jmenoHrace+"): ";

            if (r->ret == 0 || r->ret == 1 || r->ret == 2){
                printString += "turn done"+to_string(move)+"\n";
            }
            cout<< printString <<endl;
            if (write(r->id1, printString.c_str(), printString.size()) < 0) {
                perror("write error");
                r->ret = -6;
            }
            if (write(r->id2, printString.c_str(), printString.size()) < 0) {
                perror("write error");
                r->ret = -6;
            }
            //secondary doplnujici zprava
            cout<<"r->ret = "<<to_string(r->ret) <<endl;
            if(r->ret == 0)printString="";
            else if (r->ret == 1){
                printString = "Winning move done, player "+jmenoHrace+" has won.\n";
                //reset hry
                memset(r->gameboard, 0, sizeof(r->gameboard));
                r->ret = -1;
                r->move = -1;
                r->hrac = 0;
            }
            else if (r->ret == 2){
                printString = "Last turn done - draw\n";
                //reset hry
                memset(r->gameboard, 0, sizeof(r->gameboard));
                r->ret = -1;
                r->move = -1;
                r->hrac = 0;
            }

            else if (r->ret == 3)printString = "Turn unvalid\n";
            else printString += "Your opponent probably disconnected. Waiting for reconnect.\n";
            cout<< printString <<endl;
            if (write(r->id1, printString.c_str(), printString.size()) < 0) {
                perror("write error");
                r->ret = -6;
            }
            if (write(r->id2, printString.c_str(), printString.size()) < 0) {
                perror("write error");
                r->ret = -6;
            }
        }
        else{
            //not your turn + pozice kde byl proveden tah pro zpetne vráceni tahu
            int m = atoi(vec[1].c_str());
            string printString = "Not your turn:"+to_string(m)+to_string(r->gameboard[m])+"\n";
            cout<< printString <<endl;
            if (write(id, printString.c_str(), printString.size()) < 0) {
                perror("write error");
                r->ret = -6;
            }

        }
    }
        //hrac opousti mistnost
    else if (vec[0] == "leave" && r->idRoom >= 0) {
        //pokud se odpojil hrac 1
        if (r->id1 == id) {
            //poud hrac dva zustal v mistnosti
            if(r->id2 != -1){
                r->id1 = r->id2;
                r->id2 = -1;
                r->h1 = r->h2;
                r->h2 = "";
                r->inGame = 0;
                r->ready=false;
            }
            //mistnost je prazda - odstranit ze seznamu
            else {
                roomList.remove(r);
            }
        } else if (r->id2 == id) {
            r->id2 = -1;
            r->h2 = "";
        }
        string printString = "You have left the room.\n";
        if (write(id, printString.c_str(), printString.size()) < 0) {
            perror("write error");
            r->ret = -6;
        }
        r->ret = -2;
    } else {
        string printString = "Didnt recognise command, shutting down\n";
        if (write(id, printString.c_str(), printString.size()) < 0) {
            perror("write error");
            r->ret = -6;
        }
        cout << "Didnt recognise command, shutting down" << endl;
        r->ret = -50;

    }
    return r;
}


void *obsluha(void *id) {

    int clientID = *(int *) id;
    string clientName;
    char buffer[1024];
    struct room *r = new struct room;



    //ziskani jmena hrace
    while (true) {
        string jmCmd = "Give me your nickname please: \n";
        cout<<jmCmd<<endl;
        if (write(clientID, jmCmd.c_str(), jmCmd.size()) < 0) {
            perror("write error");
            //exit(6);
        }
        //nacteni jmena
        memset(buffer, 0, sizeof(buffer));
        int size = read(clientID, buffer, sizeof(buffer));
        cout << size << endl;
        if (size < 0) {
            perror("read error");
            //exit(5);
        }//klient se odpojil - odpojit ho
        else if (size == 0) {
            close(clientID);
        }
        string s = buffer;
        //osetreni jmena
        if (!s.empty())s.pop_back();
        if (s.empty()) { continue; }

        //osetreni znaku ve jmene
        int len = s.length();
        int i = 0;
        while (i < len) {
            if (!isalnum(s[i]) || s[i] == ' ') {
                s.erase(i, 1);
                len--;
            } else i++;
        }

        //overeni jedinecnosti jmena
        string v;
        bool kontrola = false;
        bool obsazeno = false;
        //cout<<"here"<<endl;
        for (const auto v : nameList){
            //cout<<"namelist for"<<endl;
            //jmeno je obsazene - kontrola jestli je ingame
            if(v==s){
                obsazeno = true;
                cout<<"jmeno je uzivano"<<endl;
                for (auto v : roomList){
                    //jmeno souhlasi a hra ceka na reconnect
                    cout<<v->h1 << v->h2 << s << v->inGame <<endl;
                    if((v->h1 == s || v->h2 == s) && v->inGame==2){
                        //cout<<"here in reconnect"<<endl;
                        v->inGame = 1;
                        int p;
                        if(v->h1==s)p=1;
                        else p=2;

                        //cout<<p<<v->h1<<r->h2<<endl;

                        string printString =s+to_string(p)+"Reconnecting ";
                        r = v;
                        for(int i = 0; i < 9; i++){
                            printString+=to_string(r->gameboard[i]);
                        }
                        if(v->h1==s)v->id1=clientID;
                        else v->id2 = clientID;

                        printString+="\n";
                        cout<<printString<<endl;
                        if (write(clientID, printString.c_str(), printString.size()) < 0) {
                            perror("write error");
                            //exit(6);
                        }
                        kontrola = true;
                        cout<<"kontrola = true"<<endl;
                        onlineList.push_back(s);
                        clientName = s;
                        break;
                    }
                }
                //novy nick prosim
                if(!kontrola) {
                    string printString = "This username is taken, choose another please.\n";
                    cout << printString << endl;
                    if (write(clientID, printString.c_str(), printString.size()) < 0) {
                        perror("write error");
                        //exit(6);
                    }
                    else cout<<"send"<<endl;

                }
            }
        }
        //cout<<"here3"<<endl;
        if(!obsazeno){
            clientName = s;
            nameList.push_back(s);
            onlineList.push_back(s);
            //cout<<"here2"<<endl;
            string printString ="Hi " + s +"\n";
            cout<<printString<<endl;
            if (write(clientID, printString.c_str(), printString.size()) < 0) {
                perror("write error");
                //exit(6);
            }
            break;
        }
        else if(obsazeno && kontrola)break;

    }

    //nekonecna smycka s cekanim na prikazy
    while (true) {
        //cout << "---------------------------------- CEKANI ---------------------------------------------"<<endl;

        memset(buffer, 0, sizeof(buffer));
        int sizeOfBuffer = read(clientID, buffer, sizeof(buffer));

        //cout<<"cekam tady 1:"<<buffer<<endl;
        if (sizeOfBuffer < 0) {
            perror("read error");
            //exit(5);
        }
            //klient se odpojil
        else if (sizeOfBuffer == 0) {
            onlineList.remove(clientName);
            for (auto v : roomList){
                //jmeno souhlasi a hra ceka na reconnect
                //cout<<"disconnect loop - ingame state: "<<v->inGame<<v->h1<<v->h2<<clientName<<endl;
                if((v->h1 == clientName || v->h2 == clientName) && v->inGame==1){
                    v->inGame = 2;
                    //cout<<"here in disconnect"<<endl;
                }
                else if((v->h1 == clientName || v->h2 == clientName) && v->inGame==2){
                    v->inGame =0;
                    roomList.remove(v);
                }
            }
            cout << "bad read - closing client" <<endl;
            close(clientID);
            break;
        }
        //cout<<"cekam tady 2"<<endl;
        printf("%d|%s send: %s", clientID, clientName.c_str(), buffer);
        //zpracovani zpravy od uzivatele
        string sendString = buffer;
        r = klientNecoPoslal(sendString, clientID, clientName, r);
        //cout << "-------------------------------- ZPRACOVANO -------------------------------------------"<<endl;
        if(r->ret==-6){
            //cout << "Got code -6 from string handler (write error) - client is probably disconnected." <<endl;
            //write error - klient je pravdepodobne ztracen
        }
        else if(r->ret == -10){
            //cout << "Got code -10 from string handler (empty list) - client is probably disconnected." <<endl;
            //write error - klient je pravdepodobne ztracen
        }
        else if(r->ret == -7){
            //attempt join to none room
            //cout<< "Got code -7 from string handler (joining not existing room) - try again"<<endl;
        }
        else if(r->ret == -50){
            onlineList.remove(clientName);
            for (auto v : roomList){
                //jmeno souhlasi a hra ceka na reconnect
                //cout<<"disconnect loop - ingame state: "<<v->inGame<<v->h1<<v->h2<<clientName<<endl;
                if((v->h1 == clientName || v->h2 == clientName) && v->inGame==1){
                    v->inGame = 2;
                    //cout<<"here in disconnect"<<endl;
                }
                else if((v->h1 == clientName || v->h2 == clientName) && v->inGame==2){
                    v->inGame =0;
                    roomList.remove(v);
                }
            }
            
            close(clientID);
            return nullptr;
        }
        /*if (ret >= 0) {
            roomId = ret;
        } else if (ret == -2) roomId = -1;*/
    }
    return nullptr;
}


int main(int argc, char const *argv[]) {

    //clientFd predelat na dvojrozmerne pole jmen [string][string] - pokud se mi nekdo pripoji, kouknu jestli ho znam... pokud ->
    //-> jo, tak kouknu jestli mi spadnul a kdyztak ho pripojim, pokud ne, tak ho vyhodim at si da jine jmeno
    int serverFd, clientFd;
    struct sockaddr_in server, client;
    int len;
    int port = 1234;
    //jestli spustim program s parametrem - spusti se na urcitem portu (predpokladam, ze uzivatel vi)

    int isIP = -1;
    int isPort = -1;
    isIP = inet_pton(AF_INET, argv[1], &(server.sin_addr));

    if(!isIP && strcmp(argv[1], "localhost")!=0){
        perror("Not an IP address (IPv4)!");
        exit(1);
    }
    for(int i = 0; i <strlen(argv[2]); i++){
        if(!isdigit(argv[2][i])){
            perror("Not valid port!");
            exit(1);
        }
    }
    port = atoi(argv[2]);
    if(port>65535){
        perror("Not valid port!");
        exit(1);
    }

    serverFd = socket(AF_INET, SOCK_STREAM, 0);
    if (serverFd < 0) {
        perror("Cannot create socket");
        exit(1);
    }
    server.sin_family = AF_INET;

    if(!strcmp(argv[1],"localost"))server.sin_addr.s_addr = inet_addr("127.0.0.1");
    else server.sin_addr.s_addr = inet_addr(argv[1]);

    server.sin_port = htons(port);
    len = sizeof(server);

    int reuse = 1;
    if (setsockopt(serverFd, SOL_SOCKET, SO_REUSEADDR, (const char*)&reuse, sizeof(reuse)) < 0)
        perror("setsockopt(SO_REUSEADDR) failed");

    #ifdef SO_REUSEPORT
    if (setsockopt(serverFd, SOL_SOCKET, SO_REUSEPORT, (const char*)&reuse, sizeof(reuse)) < 0)
        perror("setsockopt(SO_REUSEPORT) failed");
    #endif

    if (bind(serverFd, (struct sockaddr *) &server, len) < 0) {
        perror("Cannot bind sokcet");
        exit(2);
    }
    if (listen(serverFd, 10) < 0) {
        perror("listen error");
        exit(3);
    }

    cout<<"server start...." <<endl;
    while (true) {
        len = sizeof(client);
        //printf("waiting for clients\n");

        if ((clientFd = accept(serverFd, (struct sockaddr *) &client, (socklen_t *) &len)) < 0) {
            perror("accept error");
            exit(4);
        }
        cout<<clientFd<<endl;
        //klient se odebral do vlastniho vlakna obsluha
        pthread_t tmp;
        //clientFd pokud budu chtit nahradit, musim poslat jen jeden parametr -> zmena poctu parametru=vytvorit strukturu a posilat si ji
        pthread_create(&tmp, nullptr, obsluha, &clientFd);

    }/*
    close(serverFd);
    return 0;*/
}
