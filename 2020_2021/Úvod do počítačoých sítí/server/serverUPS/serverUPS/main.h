//
// Created by schenkj on 03.01.21.
//

#ifndef UNTITLED_MAIN_H
#define UNTITLED_MAIN_H

#endif //UNTITLED_MAIN_H

using namespace std;

//zakladni info mistnosti s hraci
struct room{
    int idRoom = -1;
    int id1 = -1;
    int id2 = -1;
    std::string h1 = "";
    std::string h2 = "";
    int inGame = 0;
    bool ready = false;
    //game
    int gameboard[9] = {0,0,0,0,0,0,0,0,0};
    int move{-1};
    int hrac{0};
    int ret{-1};
};
//hrac - jaky hrac hral; move - kde se snazi provest tah; gameboard - momentalni stav hry; ret - validace tahu



int klientNecoPoslal(string s, int id, const string& name, room r);
void *obsluha(void * id);
int main(int argc, char const *argv[]);
//game* zkusTah(game gs);
void zkusTah(room r);
bool vyhra(room r);
