﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zk1.Model
{
    public class Chanel
    {
        public string title { get; set; }
        public string description { get; set; }
        public string link { get; set; }
        public List<Item> messages { get; set; }


        public Chanel()
        {
            messages = new List<Item>();
        }
    }
}
