﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.ServiceModel.Syndication;
using System.Xml;
using System.Windows.Data;

namespace Zk1.View
{
    public class Functions : INotifyPropertyChanged
    {
        ObservableCollection<Model.Chanel> chanels { get; set; } = new();
        public ICollectionView ChanelsView { get; private set; }

        public Model.Chanel SelectedItem { get; set; }

        public Model.Item SelectedItemMessage { get; set; }

        public event System.EventHandler ChanelChanged;

        ObservableCollection<Model.Item> messages { get; set; } = new();
        public ICollectionView MessagesView { get; private set; }

        public event PropertyChangedEventHandler? PropertyChanged;

        public ICollectionViewLiveShaping liveShape;

        public RelayCommand ExportXML { get; private set; }


        public Model.Chanel MyProperty
        {
            get { return SelectedItem; }
            set
            {
                SelectedItem = value;
                if (true)
                {
                    Console.WriteLine();
                }
            }
        }

        protected virtual void OnChanelChanged()
        {
            if (ChanelChanged != null) ChanelChanged(this, EventArgs.Empty);
            Console.WriteLine();
        }
        public Model.Chanel SelectedChanel
        {
            get
            {
                return SelectedItem;
            }

            set
            {
                SelectedItem = value;
                OnChanelChanged();
            }
        }

        public Functions()
        {
            string[] paths = { "http://home.zcu.cz/~pvanecek/net/rss/", "http://home.zcu.cz/~pvanecek/net/rss/?timeout=5", "http://servis.idnes.cz/rss.aspx?c=bonusweb", " http://servis.idnes.cz/rss.aspx?c=technet" };

            foreach(string path in paths)
            {
                chanels.Add(ReadRSS(path));
            }
            
            foreach(var m in chanels[0].messages)
            {
                messages.Add(m);
            }
            SelectedItem = chanels[0];

            ChanelsView = CollectionViewSource.GetDefaultView(chanels);
            ChanelsView.SortDescriptions.Add(new SortDescription("title", ListSortDirection.Ascending));
            liveShape = (ChanelsView as ICollectionViewLiveShaping);
            liveShape.LiveSortingProperties.Add("title");
            liveShape.IsLiveSorting = true;


            MessagesView = CollectionViewSource.GetDefaultView(messages);
            MessagesView.SortDescriptions.Add(new SortDescription("title", ListSortDirection.Ascending));
            liveShape = (MessagesView as ICollectionViewLiveShaping);
            liveShape.LiveSortingProperties.Add("title");
            liveShape.IsLiveSorting = true;


            ExportXML = new RelayCommand(ExportXml);

        }



        public Model.Chanel ImportChanel(string path)
        {
            /* Model.Chanel chanel = new Model.Chanel();
             List<Model.Item> items = new List<Model.Item>();


             var serializer = new XmlSerializer(typeof(Model.Chanel));
             // response.Data return is a List<XElement>
             foreach (var result in Response.FromJson())
             {
                 var ch = (Model.Chanel)serializer.Deserialize(result.CreateReader());
                 chanel = ch;
             }
             return chanel;*/
            return new Model.Chanel();
        }


        public Model.Chanel ReadRSS(string path)
        {
            var url = path;
            using var reader = XmlReader.Create(url);
            var feed = SyndicationFeed.Load(reader);

            Model.Chanel c = new Model.Chanel();
            c.title = feed.Title.Text;
            c.link = feed.Links.FirstOrDefault().GetAbsoluteUri().AbsolutePath;
            c.description = feed.Description.Text;

            int counter = 0;
            
            var posts = feed.Items;
            foreach (var post in posts)
            {
                Model.Item m = new Model.Item();
                m.guid = post.Id;
                m.title = post.Title.Text;
                m.link = post.Links.FirstOrDefault().GetAbsoluteUri().AbsolutePath;
                m.description = post.Summary.Text;
                c.messages.Add(m);
                counter++;
                if (counter > 100) break;
            }

            return c;
        }



        void ExportXml()
        {
            System.Xml.Serialization.XmlSerializer writer =
                new System.Xml.Serialization.XmlSerializer(typeof(Model.Chanel));

            var path = "./dss.xml";
            System.IO.FileStream file = System.IO.File.Create(path);

            foreach (var s in chanels)
            {
                writer.Serialize(file, s);
            }

            file.Close();
        }
    }
}
