﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace GUI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static string filePath = "";
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            if (e.Args.Length > 0 && (e.Args[0] != null || e.Args[0] != ""))
            {
                //Console.WriteLine("Pokud bylo do aplikace nahráno velké množství souborů, je možné, že se bude aplikace zapínat i několik minut.");
                //Console.WriteLine("Poskytnuté json soubory (81785 souborů) se čtou a indexují cca 4 minuty.");
                filePath = e.Args[0];
            }
            else
            {
                filePath = "WholeDoc.txt";
            }
            MainWindow wnd = new MainWindow();
            wnd.Show();
        }
    }
}
