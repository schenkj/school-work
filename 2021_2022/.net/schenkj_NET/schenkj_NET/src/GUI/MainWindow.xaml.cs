﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string querry;
        public Preprocessing preprocess;
        public AdvancedTokenizer tokenizer;
        public TfIdf tfidf;
        public LogicalSearch logicalSearch;
        public DatabaseHandler db;
        public int HistoryStopper;
        public int numberOfHistoryShown = 10;

        public MainWindow()
        {
            //inicializace a spusteni aplikace

            preprocess = new Preprocessing();
            tokenizer = new AdvancedTokenizer();
            tfidf = new TfIdf(preprocess);
            logicalSearch = new LogicalSearch(tokenizer, tfidf, preprocess);
            db = new DatabaseHandler();
            HistoryStopper = 0;

            //spusteni nacitani a indexovani dokumentu
            SearchEngine.Program(App.filePath, preprocess, tfidf);

            InitializeComponent();
            LoadHistory();
        }

       
        //metoda pro nacteni poslednich  nekolika zaznamu
        private void LoadHistory(int id = 0)
        {
            History.Text = "";
            var dbContent = db.Read();
            if (dbContent.Count > numberOfHistoryShown)
            {
                for (int i = dbContent.Count - numberOfHistoryShown; i < dbContent.Count; i++)
                {
                    if (dbContent[i].Id > id)
                        History.Text += dbContent[i].ToString();
                }
            }
            else
            {
                for (int i = 0; i < dbContent.Count; i++)
                {
                    if (dbContent[i].Id > id)
                        History.Text += dbContent[i].ToString();
                }
            }

        }

        //obsluha tlacitka pro zahajeni vyhledavani
        private void SearchButtonClicked(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(Querry.Text))
            {
                DateTime startTime = DateTime.Now;

                bool logical = false;
                string input = Querry.Text;
                if (input == null) return;
                string tmp = input;
                tmp = tmp.Replace("AND", "");
                tmp = tmp.Replace("OR", "");
                tmp = tmp.Replace("NOT", "");
                tmp = tmp.Trim();
                if (tmp.Length == 0) return;
                querry = input;

                //osetreni vstupu pro pocet pozadovanych dokumentu
                int numberOfDocs;
                try
                {
                    input = NumberOfWantedDocs.Text;
                    if (input == null)
                    {
                        return;
                    }
                    numberOfDocs = int.Parse(input);
                }
                catch (Exception)
                {
                    Results.Text += "You didn´t enter whole number for number of wanted documents.\n Your result will consist top 5 answers.";
                    numberOfDocs = 5;
                }
                string results = "";

                //binarni hledani (booleovsky model)
                if (querry.Contains("AND") || querry.Contains("OR") || querry.Contains("NOT"))
                {
                    logical = true;
                    results = logicalSearch.advancedSearch(querry, numberOfDocs, SearchEngine.invertedIndex);
                }
                //else normalni vektorov vyhledavani
                else results = tfidf.EnterQuerry(querry, numberOfDocs);

                DateTime endTime = DateTime.Now;
                TimeSpan difference = endTime - startTime;

                //ulozeni vysledku do db (historie vyhledavani)
                db.Update();
                var m = new Member();
                if (db.MemberList.Count > 0) m.Id = db.MemberList.Last().Id + 1;
                else m.Id = 1;
                m.Querry = querry;
                int counter = 0;
                m.Results = "";
                foreach (var winner in tfidf.winnerDocumentIndex)
                {
                    if (winner == -1) break;
                    if (counter % 5 == 0) m.Results += "\n";
                    if (winner == tfidf.winnerDocumentIndex[tfidf.winnerDocumentIndex.Length - 1]) m.Results += winner;
                    else m.Results += winner + ", ";
                    counter++;
                }
                m.NumberOfResultsWanted = tfidf.winnerDocumentIndex.Length;
                m.NumberOfFoundResults = tfidf.numberOfFoundDocuments;
                m.TimeInMilis = difference.TotalMilliseconds;
                db.Add(m);

                //obslouženi vypisu na obrazovku + do okna s historii
                Results.Text += "------------------------------------------------------------------------------------------------------------\n(" + difference.TotalMilliseconds + " ms)\n";
                Results.Text += results;
                Results.Text += "\n\n\n";
                Results.ScrollToEnd();
                LoadHistory(HistoryStopper);
                History.ScrollToEnd();

            }
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            History.Text = "";
            HistoryStopper = db.MemberList[db.MemberList.Count - 1].Id+1;
        }
    }

}

