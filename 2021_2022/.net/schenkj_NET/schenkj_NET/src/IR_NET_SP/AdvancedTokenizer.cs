﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;


public class AdvancedTokenizer
{

    public string defaultRegex;

    public AdvancedTokenizer()
    {
        defaultRegex = "@([<]?http(s)?://[a-zA-z0-9+\\-&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|])|([\\s\\p{P}])|(\\S*)";
    }

    //metoda pro tokenizaci
    public string[] tokenize(string text, string regex)
    {
        List<string> words = new List<string>();

        string myString = text;
        Regex rx = new Regex(regex);
        MatchCollection matches = rx.Matches(myString);
        foreach (Match match in matches)
        {
            if (match.Value.Length > 0 && match.Value != " ")
            {
                words.Add(match.Value);
            }
        }

        string[] ws = new string[words.Count];
        ws = words.ToArray<string>();

        return ws;
    }

    //odebrani akcentu
    public string removeAccents(string text)
    {
        StringBuilder sbReturn = new StringBuilder();
        var arrayText = text.Normalize(NormalizationForm.FormD).ToCharArray();
        foreach (char letter in arrayText)
        {
            if (CharUnicodeInfo.GetUnicodeCategory(letter) != UnicodeCategory.NonSpacingMark)
                sbReturn.Append(letter);
        }
        return sbReturn.ToString();
    }

    //spusteni tokenizace
    public string[] tokenize(string text)
    {
        return tokenize(text, defaultRegex);
    }
}

