﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class CzechStemmerLight
{
    /**
    * buffer na stemmovana slova
    */
    private string sb = "";


    /**
     * Zakladni constructor
     */
    public CzechStemmerLight() { } // constructor

    public string stem(string input)
    {

        //
        input = input.ToLower();

        //reset bufferu
        sb.Remove(0, sb.Length);
        sb = input;

        // stemming...
        //pridavna jmena a podstatna jmena - odebrani koncovek
        removeCase(sb);

        //odebrani koncu jmen -ov- a -in-
        removePossessives(sb);

        string result = sb;

        return result;
    }
    private void palatalise(string buffer)
    {
        int len = buffer.Length;

        if (buffer.Substring(len - 2, 2).Equals("ci") ||
                buffer.Substring(len - 2, 2).Equals("ce") ||
                buffer.Substring(len - 2, 2).Equals("\u010di") ||  //-či
                buffer.Substring(len - 2, 2).Equals("\u010de"))
        {  //-č

            //buffer.Replace(len - 2, len, "k");
            buffer = buffer.Remove(len - 2, 2);
            buffer += "k";
            sb = buffer;
            return;
        }
        if (buffer.Substring(len - 2, 2).Equals("zi") ||
                buffer.Substring(len - 2, 2).Equals("ze") ||
                buffer.Substring(len - 2, 2).Equals("\u017ei") ||  //-ži
                buffer.Substring(len - 2, 2).Equals("\u017ee"))
        {  //-že

            //buffer.replace(len - 2, len, "h");
            buffer = buffer.Remove(len - 2, 2);
            buffer += "h";
            sb = buffer;
            return;
        }
        if (buffer.Substring(len - 3, 3).Equals("\u010dt\u011b") ||  //-čtě
                buffer.Substring(len - 3, 3).Equals("\u010dti") ||       //-čti
                buffer.Substring(len - 3, 3).Equals("\u010dt\u00ed"))
        {  //-čté

            //buffer.replace(len - 3, len, "ck");
            buffer = buffer.Remove(len - 3, 3);
            buffer += "ck";
            sb = buffer;
            return;
        }
        if (buffer.Substring(len - 2, 2).Equals("\u0161t\u011b") ||  //-ště
                buffer.Substring(len - 2, 2).Equals("\u0161ti") ||       //-šti
                buffer.Substring(len - 2, 2).Equals("\u0161t\u00ed"))
        {  //-šté

            //buffer.replace(len - 2, len, "sk");
            buffer = buffer.Remove(len - 2, 2);
            buffer += "sk";
            sb = buffer;
            return;
        }
        buffer = buffer.Remove(len - 1, 1);
        sb = buffer;
        return;
    }//palatalise

    private void removePossessives(string buffer)
    {
        int len = buffer.Length;

        if (len > 5)
        {
            if (buffer.Substring(len - 2, 2).Equals("ov"))
            {
                buffer = buffer.Remove(len - 2, 2);
                sb = buffer;
                //buffer.Delete(len - 2, len);
                return;
            }
            if (buffer.Substring(len - 2, 2).Equals("\u016fv"))
            { //-ův

                buffer = buffer.Remove(len - 2, 2);
                sb = buffer;
                return;
            }
            if (buffer.Substring(len - 2, 2).Equals("in"))
            {

                buffer = buffer.Remove(len - 1, 1);
                sb = buffer;
                palatalise(buffer);
                return;
            }
        }
        return;
    }//removePossessives

    private void removeCase(string buffer)
    {
        int len = buffer.Length;
        //
        if ((len > 7) &&
                buffer.Substring(len - 5, 5).Equals("atech"))
        {

            buffer = buffer.Remove(len - 5, 5);
            sb = buffer;
            return;
        }//len>7
        if (len > 6)
        {
            if (buffer.Substring(len - 4, 4).Equals("\u011btem"))
            { //-ětem

                buffer = buffer.Remove(len - 3, 3);
                sb = buffer;
                palatalise(buffer);
                return;
            }
            if (buffer.Substring(len - 4, 4).Equals("at\u016fm"))
            {  //-atům
                buffer = buffer.Remove(len - 4, 4);
                sb = buffer;
                return;
            }

        }
        if (len > 5)
        {
            if (buffer.Substring(len - 3, 3).Equals("ech") ||
                    buffer.Substring(len - 3, 3).Equals("ich") ||
                    buffer.Substring(len - 3, 3).Equals("\u00edch"))
            { //-ích

                buffer = buffer.Remove(len - 2, 2);
                sb = buffer;
                palatalise(buffer);
                return;
            }
            if (buffer.Substring(len - 3, 3).Equals("\u00e9ho") || //-ého
                    buffer.Substring(len - 3, 3).Equals("\u011bmi") ||  //-ěmi
                    buffer.Substring(len - 3, 3).Equals("emi") ||
                    buffer.Substring(len - 3, 3).Equals("\u00e9mu") ||  //ému
                    buffer.Substring(len - 3, 3).Equals("\u011bte") ||  //-ěte
                    buffer.Substring(len - 3, 3).Equals("\u011bti") ||  //-ěti
                    buffer.Substring(len - 3, 3).Equals("iho") ||
                    buffer.Substring(len - 3, 3).Equals("\u00edho") ||  //-ího
                    buffer.Substring(len - 3, 3).Equals("\u00edmi") ||  //-ími
                    buffer.Substring(len - 3, 3).Equals("imu"))
            {

                buffer = buffer.Remove(len - 2, 2);
                sb = buffer;
                palatalise(buffer);
                return;
            }
            if (buffer.Substring(len - 3, 3).Equals("\u00e1ch") || //-ách
                    buffer.Substring(len - 3, 3).Equals("ata") ||
                    buffer.Substring(len - 3, 3).Equals("aty") ||
                    buffer.Substring(len - 3, 3).Equals("\u00fdch") ||   //-ých
                    buffer.Substring(len - 3, 3).Equals("ama") ||
                    buffer.Substring(len - 3, 3).Equals("ami") ||
                    buffer.Substring(len - 3, 3).Equals("ov\u00e9") ||   //-ové
                    buffer.Substring(len - 3, 3).Equals("ovi") ||
                    buffer.Substring(len - 3, 3).Equals("\u00fdmi"))
            {  //-ými

                buffer = buffer.Remove(len - 3, 3);
                sb = buffer;
                return;
            }
        }
        if (len > 4)
        {
            if (buffer.Substring(len - 2, 2).Equals("em"))
            {

                buffer = buffer.Remove(len - 1, 1);
                sb = buffer;
                palatalise(buffer);
                return;

            }
            if (buffer.Substring(len - 2, 2).Equals("es") ||
                    buffer.Substring(len - 2, 2).Equals("\u00e9m") ||    //-ém
                    buffer.Substring(len - 2, 2).Equals("\u00edm"))
            {   //-ím

                buffer = buffer.Remove(len - 2, 2);
                sb = buffer;
                palatalise(buffer);
                return;
            }
            if (buffer.Substring(len - 2, 2).Equals("\u016fm"))
            {  //-ům

                buffer = buffer.Remove(len - 2, 2);
                sb = buffer;
                return;

            }
            if (buffer.Substring(len - 2, 2).Equals("at") ||
                    buffer.Substring(len - 2, 2).Equals("\u00e1m") ||    //-ám
                    buffer.Substring(len - 2, 2).Equals("os") ||
                    buffer.Substring(len - 2, 2).Equals("us") ||
                    buffer.Substring(len - 2, 2).Equals("\u00fdm") ||     //-ým
                    buffer.Substring(len - 2, 2).Equals("mi") ||
                    buffer.Substring(len - 2, 2).Equals("ou"))
            {

                buffer = buffer.Remove(len - 2, 2);
                sb = buffer;
                return;
            }
        }//len>4
        if (len > 3)
        {
            if (buffer.Substring(len - 1, 1).Equals("e") ||
                    buffer.Substring(len - 1, 1).Equals("i"))
            {

                palatalise(buffer);
                return;

            }
            if (buffer.Substring(len - 1, 1).Equals("\u00ed") ||  //-í
                    buffer.Substring(len - 1, 1).Equals("\u011b"))
            { //-ě

                palatalise(buffer);
                return;
            }
            if (buffer.Substring(len - 1, 1).Equals("u") ||
                    buffer.Substring(len - 1, 1).Equals("y") ||
                    buffer.Substring(len - 1, 1).Equals("\u016f"))
            {  //-ů

                buffer = buffer.Remove(len - 1, 1);
                sb = buffer;
                return;

            }
            if (buffer.Substring(len - 1, 1).Equals("a") ||
                    buffer.Substring(len - 1, 1).Equals("o") ||
                    buffer.Substring(len - 1, 1).Equals("\u00e1") ||  // -á
                    buffer.Substring(len - 1, 1).Equals("\u00e9") ||  //-é
                    buffer.Substring(len - 1, 1).Equals("\u00fd"))
            {   //-ý

                buffer = buffer.Remove(len - 1, 1);
                sb = buffer;
                return;
            }
        }//len>3
    }
}

