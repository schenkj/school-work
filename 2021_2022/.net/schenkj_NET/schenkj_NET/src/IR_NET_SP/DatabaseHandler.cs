﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//trida pro reprezentaci zaznamu v databazi
public class Member : IEnumerable
{
    [Key]
    [Column("Id")]
    public int Id { get; set; }
    public string Querry { get; set; }
    public string Results { get; set; }
    public int NumberOfResultsWanted { get; set; }
    public int NumberOfFoundResults { get; set; }
    public double TimeInMilis { get; set; }

    public IEnumerator GetEnumerator()
    {
        return Results.GetEnumerator();
    }


    //vlastni tostring pro vypis zaznamu do okna s historii vyhledavani
    override
    public string ToString()
    {
        var ret = "";
        ret += "Querry: " + Querry;
        ret += "\nNumber of wanted results: " + NumberOfResultsWanted;
        ret += "\nFound results (from best): " + Results;
        ret += "\nNumber of documents found: " + NumberOfFoundResults;
        ret += "\nSearch time: " + TimeInMilis;
        ret += "\n-----------------------------------------\n";
        return ret;
    }

    //pro predchazeni chybam inicializace promennych
    public Member()
    {
        Id = 0;
        Querry = "";
        Results = "";
        NumberOfResultsWanted = -1;
        TimeInMilis = -1;
    }
}

//trida pro manipulaci s db
public class DatabaseHandler
{
    public List<Member> MemberList;

    //pri inicializaci nacteni seznamu se zaznamy
    public DatabaseHandler()
    {
        try
        {
            MemberList = Read();
        }
        catch (Exception ex)
        {
            Console.Error.WriteLine(ex.Message);
            CreateNew();
            MemberList = new();
        }
    }

    //pomoci precteni vsech zaznamu se provede aktualizace seznamu
    public void Update()
    {
        MemberList = Read();
    }

    //vytvoreni a premazani databaze
    public void CreateNew()
    {
        //scrap old db and create new and empty
        using (var context = new Context())
        {
            context.Database.EnsureDeleted();
            context.Database.EnsureCreated();
        }
        MemberList = new List<Member>();
    }

    //pridani noveho zaznamu
    public void Add(Member m)
    {
        //add new member to db
        using (var context = new Context())
        {
            context.Content.Add(m);
            context.SaveChanges();
        }
    }

    //precteni vsech zaznamu - premazani starych
    public List<Member> Read()
    {
        //read
        List<Member> MemberList = new();
        using (var context = new Context())
        {
            foreach (var member in context.Content)
            {
                MemberList.Add(member);
            }
        }
        return MemberList;
    }

}

//entity framework context pro manipulaci s databazi
public class Context : DbContext
{
    public DbSet<Member>? Content { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data source=history.sqlite");
    }
}
