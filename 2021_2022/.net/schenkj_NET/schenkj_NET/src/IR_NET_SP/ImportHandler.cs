﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json;


//prida pro nacteni dat
public class ImportHandler
{
    //struktura json souboru pro indexaci souboru poskytnutych KIV/IR
    public struct jsonFile
    {
        public string text { get; set; }
        public string id { get; set; }
        public string title { get; set; }
        public string date { get; set; }
    }

    //metoda pro nacteni novych dokumentu
    public static List<string> readNewDocuments(string v)
    {
        List<string> documents = new List<string>();
        if (v.EndsWith(".txt"))     //read .txt - cteni dokumentu se specifickou podobou ziskanou vlastnim crawlovanim
        {
            try
            {
                //precist dokumenty do seznamu                
                documents = ImportHandler.ReadDoc(v);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                return documents;
            }
        }
        else
        {   //nacteni json souboru v dane slozce
            try
            {
                int counter = 0;
                string fileName = v + "/" + counter + ".json";
                string json;
                while (File.Exists(fileName))
                {
                    using (StreamReader r = new StreamReader(fileName))
                    {

                        json = File.ReadAllText(fileName);
                        var jsonContent = JsonSerializer.Deserialize<jsonFile>(json)!;
                        documents.Add(jsonContent.text);
                    }
                    counter++;
                    fileName = v + "/" + counter + ".json";
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"There was error in reading json files. {ex}");
            }
        }
        Console.WriteLine("All documents were read into memory... Processing...");
        return documents;
    }



    //precte dokumenty obsazene v jednom .txt souboru oddelene "\n----------------------------------------------------\n" oddělovacem
    public static List<string> ReadDoc(string path)
    {
        try
        {
            if (!File.Exists(path))
            {
                Console.WriteLine("Not valid file path.");
                return new List<string>();
            }
            using (StreamReader sr = new StreamReader(path))
            {
                List<string> documents = new List<string>();
                string line = "";
                string document = "";
                while (sr.Peek() >= 0)
                {
                    line = "";
                    line += sr.ReadLine();
                    if (line.Contains("----------------------------------------------------"))
                    {
                        //konec dokumentu
                        documents.Add(document);
                        document = "";

                    }
                    else if (line.Length < 2) continue;     //preskoceni prazdnych radek
                    else
                    {
                        document += line;
                    }
                }
                documents.Add(document);
                return documents;
            }
        }
        catch (Exception e)
        {
            Console.WriteLine($"Couldnt read the file: {e}");
            return new List<string>();
        }
    }
}
