﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static TfIdf;

public class LogicalSearch
{

    public AdvancedTokenizer tokenizer;
    public TfIdf tfidf;
    public Preprocessing preprocessing;

    public int numberOfDocumentsWanted;

    public int[] winnerDocumentIndex;


    public LogicalSearch(AdvancedTokenizer tok, TfIdf tf, Preprocessing prep)
    {
        tokenizer = tok;
        tfidf = tf;
        preprocessing = prep;
    }
    public string advancedSearch(string input, int numberOfDocs, Dictionary<string, InvertedIndex> invertedIndex)
    {
        if (input.Contains("AND NOT")) input = input.Replace("AND NOT", "NOT");
        numberOfDocumentsWanted = numberOfDocs;
        string ret = "";
        //string je string mezi zavorkami a int je uroven zanoreni (priorita vykonani) a priorita zpracovani
        Dictionary<string, KeyValuePair<int, int>> parts = new Dictionary<string, KeyValuePair<int, int>>();
        string tmp = "";
        int layer = 1, i = 0;
        for (i = 0; i < input.Length; i++)
        {
            if (input[i] == '(' && !parts.Keys.Contains(tmp.Trim()))
            {
                //nalezena zavorka - nahrani tmp do parts s hodnotou layer a zvyseni hodnoty layer a vynulovani tmp
                parts.Add(tmp.Trim(), new KeyValuePair<int, int>(layer, (layer * (input.Length + 1) - i)));
                tmp = "";
                layer++;
            }
            else if (input[i] == ')' && !parts.Keys.Contains(tmp.Trim()))
            {
                //nalezena uzaviraci zavorka - nahrani tmp do parts s hodnotou layer a snizeni hodnoty layer a vynulovani tmp
                parts.Add(tmp.Trim(), new KeyValuePair<int, int>(layer, (layer * (input.Length + 1) - i)));
                tmp = "";
                layer--;
            }
            else tmp += input[i];
        }
        if (tmp.Trim().Length > 0 && !parts.Keys.Contains(tmp.Trim())) parts.Add(tmp.Trim(), new KeyValuePair<int, int>(layer, (layer * (input.Length + 1) - i)));
        //potreba rozdelit jeste podle AND/OR/NOT
        List<string> lilQuerries = new List<string>();
        List<string> unchangedLilQuerries = new List<string>();
        List<int> operations = new List<int>();//AND=0 OR=1 NOT=2
        char c = ' ', c1 = ' ', c2 = ' ';
        tmp = "";
        for (i = 0; i < input.Length; i++)
        {
            c = input[i];
            if (i < input.Length - 1) c1 = input[i + 1];
            if (i < input.Length - 2) c2 = input[i + 2];

            if (c == '(' || c == ')')
            {
                if (tmp.Trim().Length > 0)
                {
                    lilQuerries.Add(preprocessing.PreprocessQuerry(tmp));
                    unchangedLilQuerries.Add(tmp.Trim());
                }
                tmp = "";
            }
            else if (c == 'A' && c1 == 'N' && c2 == 'D')
            {
                if (tmp.Trim().Length > 0)
                {
                    lilQuerries.Add(preprocessing.PreprocessQuerry(tmp));
                    unchangedLilQuerries.Add(tmp.Trim());
                }
                tmp = "";
                i += 2;
                operations.Add(0);
            }
            else if (c == 'N' && c1 == 'O' && c2 == 'T')
            {
                if (tmp.Trim().Length > 0)
                {
                    lilQuerries.Add(preprocessing.PreprocessQuerry(tmp));
                    unchangedLilQuerries.Add(tmp.Trim());
                }
                tmp = "";
                i += 2;
                operations.Add(2);
            }
            else if (c == 'O' && c1 == 'R')
            {
                if (tmp.Trim().Length > 0)
                {
                    lilQuerries.Add(preprocessing.PreprocessQuerry(tmp));
                    unchangedLilQuerries.Add(tmp.Trim());
                }
                tmp = "";
                i++;
                operations.Add(1);
            }
            else tmp += c;
        }
        if (tmp.Length > 0)
        {
            lilQuerries.Add(preprocessing.PreprocessQuerry(tmp));
            unchangedLilQuerries.Add(tmp.Trim());
        }
        //vectory vsech casti querry
        string tmpQuerry = "";

        //po provedeni logickych operaci bude vector (inverted index) a bude potreba ziska vector tfidf pro ziskani vysledku

        Dictionary<string, InvertedIndex> querryList = new();
        var littleParts = new List<string>();
        var vecArray = new bool[tfidf.docLen];
        List<bool[]> boolVecsOfQuerries = new();
        foreach (string lilQuerry in lilQuerries)
        {
            tmpQuerry = lilQuerry;
            littleParts = tmpQuerry.Split(" ").ToList();
            foreach (string littlePart in littleParts)
            {
                if (littlePart.Trim().Length < 2) continue;
                if (invertedIndex.ContainsKey(littlePart))
                {
                    foreach (int docId in invertedIndex[littlePart].docHits.Keys)
                    {
                        vecArray[docId] = true;
                    }

                }
            }
            boolVecsOfQuerries.Add((bool[])vecArray.Clone());
            littleParts.Clear();
            vecArray = new bool[tfidf.docLen];
        }

        /*
        podle klicovych slov pak kombinovat vector a ten použít jako primitivum v další vrstvě

        mam vysledny vector - staci cosinove porovnat
        ziskat querry vector vsech casti dotazu a pak podle AND/OR/NOT logických operací výsledek zkombinovat do vysledneho vektoru - ten porovnat s dokumenty jakožto vektor querry
        */


        winnerDocumentIndex = GetResultVec(parts, operations, boolVecsOfQuerries, unchangedLilQuerries);

        //vypis (s osetrenim, kdyby se nic nenaslo apod.)
        string retStr = "";
        retStr += "Results:\n\n";

        for (i = 0; i < winnerDocumentIndex.Length && i < numberOfDocumentsWanted; i++)
        {
            retStr += $"{i + 1}. document found for your querry has index {winnerDocumentIndex[i]}\n";
        }
        retStr += "\n";
        if (winnerDocumentIndex.Length < numberOfDocumentsWanted) retStr += "If there are none or there are less results then you were asking,\n none or not enough matching documents were found.";

        int foundDocs = 0;
        foreach (int isGood in winnerDocumentIndex)
        {
            if (isGood != 0) foundDocs++;
        }
        retStr += $"\n\nFor querry \"{input}\" there are {foundDocs} dokuments.";
        return retStr;
    }

    //metoda ktera spousti vypocet a ziskava vysledek
    private static int[] GetResultVec(Dictionary<string, KeyValuePair<int, int>> parts, List<int> operations, List<bool[]> boolVecsOfQuerries, List<string> lilQuerries)
    {
        var sortedParts = from entry in parts orderby entry.Value.Value descending select entry;
        string tmp = "";
        List<bool[]> prevResults = new List<bool[]>();
        bool[] vectorHolder = new bool[boolVecsOfQuerries[0].Length];
        bool vecHolderEmpty = true;
        bool prazdnyPrikazMeziZavorkami = true;
        int tmpOperation = -1;

        //serazene pole zavorek v poradi ve kterem maji byt vyhodnoceny po slozkach prochazim
        foreach (var part in sortedParts)
        {
            if (part.Key.Trim().Length < 2) continue;
            prazdnyPrikazMeziZavorkami = true;
            //osetreni pripadu casti typu ") OR ("
            if (part.Key.Contains("AND"))
            {
                tmpOperation = 0;
                var pokus = part.Key.Split("AND");
                foreach (var kousek in pokus)
                {
                    if (kousek.Trim().Length > 2) prazdnyPrikazMeziZavorkami = false;
                }
            }
            if (part.Key.Contains("OR") && prazdnyPrikazMeziZavorkami)
            {
                tmpOperation = 1;
                var pokus = part.Key.Split("OR");
                foreach (var kousek in pokus)
                {
                    if (kousek.Trim().Length > 2) prazdnyPrikazMeziZavorkami = false;
                }
            }
            if (part.Key.Contains("NOT") && prazdnyPrikazMeziZavorkami)
            {
                tmpOperation = 2;
                var pokus = part.Key.Split("NOT");
                foreach (var kousek in pokus)
                {
                    if (kousek.Trim().Length > 2) prazdnyPrikazMeziZavorkami = false;
                }
            }

            if (prazdnyPrikazMeziZavorkami)
            {
                vectorHolder = CountOperation(tmpOperation, prevResults[prevResults.Count - 2], prevResults.Last());
                prevResults.Add(vectorHolder);
            }
            else
            {
                //cast prochazim po znacich a kdyz narazim na operaci
                for (int i = 0; i < part.Key.Length - 2; i++)
                {

                    //pokud narazim na AND
                    if (part.Key[i] == 'A' && part.Key[i + 1] == 'N' && part.Key[i + 2] == 'D')
                    {
                        //jestlize se jedna o prvni operaci, je vectorHolder null a ulozim tedy do nej vector ktery jsem docetl a ulozim si operaci, ktera se ma stat
                        if (vecHolderEmpty)
                        {
                            if (tmp.Trim().Length < 2)
                            {//cast prikazu kterou prave zpracovavam zacala zavorkou - nahraji vyhodnoceni zavorky sem
                                vectorHolder = prevResults[prevResults.Count - 1];
                                vecHolderEmpty = false;
                                tmpOperation = 0;
                                tmp = "";
                            }
                            else
                            {
                                vectorHolder = boolVecsOfQuerries[lilQuerries.IndexOf(tmp.Trim())];
                                vecHolderEmpty = false;
                                tmpOperation = 0;
                                tmp = "";
                            }
                        }
                        //vector holder není null, tedy je tam vector a ma se provest nejaka operace ktera je ulozena v tmpOperation nad vectorem v tmp a vectorHolderem
                        //vysledek je ulozen do vectorHolderu
                        else
                        {
                            vectorHolder = CountOperation(tmpOperation, vectorHolder, boolVecsOfQuerries[lilQuerries.IndexOf(tmp.Trim())]);
                            vecHolderEmpty = false;
                            tmpOperation = 0;
                            tmp = "";
                        }
                        i += 3;
                    }

                    //pokud narazim na OR

                    else if (part.Key[i] == 'O' && part.Key[i + 1] == 'R')
                    {
                        //jestlize se jedna o prvni operaci, je vectorHolder null a ulozim tedy do nej vector ktery jsem docetl a ulozim si operaci, ktera se ma stat
                        if (vecHolderEmpty)
                        {
                            if (tmp.Trim().Length < 2)
                            {//cast prikazu kterou prave zpracovavam zacala zavorkou - nahraji vyhodnoceni zavorky sem
                                vectorHolder = prevResults[prevResults.Count - 1];
                                vecHolderEmpty = false;
                                tmpOperation = 1;
                                tmp = "";
                            }
                            else
                            {
                                vectorHolder = boolVecsOfQuerries[lilQuerries.IndexOf(tmp.Trim())];
                                vecHolderEmpty = false;
                                tmpOperation = 1;
                                tmp = "";
                            }
                        }
                        //vector holder není null, tedy je tam vector a ma se provest nejaka operace ktera je ulozena v tmpOperation nad vectorem v tmp a vectorHolderem
                        //vysledek je ulozen do vectorHolderu
                        else
                        {
                            vectorHolder = CountOperation(tmpOperation, vectorHolder, boolVecsOfQuerries[lilQuerries.IndexOf(tmp.Trim())]);
                            vecHolderEmpty = false;
                            tmpOperation = 1;
                            tmp = "";
                        }
                        i += 2;
                    }

                    //pokud narazim na NOT

                    else if (part.Key[i] == 'N' && part.Key[i + 1] == 'O' && part.Key[i + 2] == 'T')
                    {
                        //jestlize se jedna o prvni operaci, je vectorHolder null a ulozim tedy do nej vector ktery jsem docetl a ulozim si operaci, ktera se ma stat
                        if (vecHolderEmpty)
                        {
                            if (tmp.Trim().Length < 2)
                            {//cast prikazu kterou prave zpracovavam zacala zavorkou - nahraji vyhodnoceni zavorky sem
                                vectorHolder = prevResults[prevResults.Count - 1];
                                vecHolderEmpty = false;
                                tmpOperation = 2;
                                tmp = "";
                            }
                            else
                            {
                                vectorHolder = boolVecsOfQuerries[lilQuerries.IndexOf(tmp.Trim())];
                                vecHolderEmpty = false;
                                tmpOperation = 2;
                                tmp = "";
                            }
                        }
                        //vector holder není null, tedy je tam vector a ma se provest nejaka operace ktera je ulozena v tmpOperation nad vectorem v tmp a vectorHolderem
                        //vysledek je ulozen do vectorHolderu
                        else
                        {
                            vectorHolder = CountOperation(tmpOperation, vectorHolder, boolVecsOfQuerries[lilQuerries.IndexOf(tmp.Trim())]);
                            vecHolderEmpty = false;
                            tmpOperation = 2;
                            tmp = "";
                        }
                        i += 3;
                    }

                    else
                    {
                        if (part.Key[i] != '(' || part.Key[i] != ')' || part.Key[i] != ':' || part.Key[i] != '.' || part.Key[i] != ',' || part.Key[i] != '?' || part.Key[i] != '!' ||
                            part.Key[i] != '[' || part.Key[i] != ']' || part.Key[i] != '{' || part.Key[i] != '}' || part.Key[i] != '%')
                            tmp += part.Key[i];
                    }
                }//pokud posledni cast "part" neni klicove slovo - nacti ho do tmp
                if (part.Key.Length < 2) continue;
                if (!((part.Key[part.Key.Length - 2] == 'N' && part.Key[part.Key.Length - 1] == 'D') ||
                    (part.Key[part.Key.Length - 2] == 'O' && part.Key[part.Key.Length - 1] == 'R') ||
                    (part.Key[part.Key.Length - 2] == 'O' && part.Key[part.Key.Length - 1] == 'T')))
                {
                    tmp += part.Key[part.Key.Length - 2];
                    tmp += part.Key[part.Key.Length - 1];
                }
                else if (part.Key.EndsWith("AND")) tmpOperation = 0;
                else if (part.Key.EndsWith("OR")) tmpOperation = 1;
                else if (part.Key.EndsWith("NOT")) tmpOperation = 2;


                //na konci mam bud posledni operaci nad necim co je v tmp (pokud je tmp prazdny mam vector z predchozi zavorky nekde ulozeny a musim pouzit ten)
                if (tmp.Trim().Length < 1)
                {// v predchozi zavorce jsem ziskal vysledek, ktery se tady musi zpracovat, jako posledni cast
                    vectorHolder = CountOperation(tmpOperation, vectorHolder, prevResults[prevResults.Count - 1]);
                    vecHolderEmpty = false;
                }
                else if (vecHolderEmpty)
                {//pokud v tmp je normalni cast querry, staci udelat to same jako vzdy pred tim
                    vectorHolder = CountOperation(tmpOperation, boolVecsOfQuerries[lilQuerries.IndexOf(tmp.Trim())], prevResults[prevResults.Count - 1]);
                    vecHolderEmpty = false;
                }
                else
                {//pokud v tmp je normalni cast querry, staci udelat to same jako vzdy pred tim
                    vectorHolder = CountOperation(tmpOperation, vectorHolder, boolVecsOfQuerries[lilQuerries.IndexOf(tmp.Trim())]);
                    vecHolderEmpty = false;
                }

                prevResults.Add(vectorHolder);


                vectorHolder = new bool[prevResults.Last().Length];
                vecHolderEmpty = true;
                tmp = "";
            }
        }
        //v prevResult je vysledek, ktery je potreba prevest na vector
        //potreba ziskat vectory jednotlivych casti querry


        return BuildVector(prevResults.Last());
    }

    //metoda pro vytvoreni vysledneho vectoru
    private static int[] BuildVector(bool[] prevResult)
    {
        List<int> result = new();
        for (int i = 0; i < prevResult.Length; i++)
        {
            if (prevResult[i])
            {
                result.Add(i);
            }
        }
        return result.ToArray();
    }


    //metoda pro vypocitani jednoduche operace nad vektory
    private static bool[] CountOperation(int tmpOperation, bool[] vec1, bool[] vec2)
    {
        bool[] result = new bool[vec1.Length];
        if (tmpOperation == 0)//AND
        {
            for (int i = 0; i < vec1.Length; i++)
            {
                if (vec1[i] == vec2[i] && vec1[i]) result[i] = true;
                else result[i] = false;
            }
            return result;
        }
        else if (tmpOperation == 1)//OR
        {
            for (int i = 0; i < vec1.Length; i++)
            {
                if (vec1[i] || vec2[i]) result[i] = true;
                else result[i] = false;
            }
            return result;
        }
        else if (tmpOperation == 2)//NOT
        {
            for (int i = 0; i < vec1.Length; i++)
            {
                if (vec1[i] && !vec2[i]) result[i] = true;
                else result[i] = false;
            }
            return result;
        }
        else return null;
    }
}

