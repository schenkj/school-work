﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class Preprocessing
{
    //inicializace potrebnych kompomnent
    public AdvancedTokenizer tokenizer = new AdvancedTokenizer();
    public BasicPreprocessingExample basicPreprocessing = new BasicPreprocessingExample();
    public CzechStemmerLight stemmer = new CzechStemmerLight();
    HashSet<string> stopwords;
    Dictionary<string, int> wordFrequencies = new Dictionary<string, int>();
    public bool toLowercase, removeAccentsBeforeStemming, removeAccentsAfterStemming;
    public Preprocessing()
    {
        //inicializace preprocessingu
        string fileName = @"stopwords.txt";
        string[] lines = File.ReadAllLines(fileName);
        stopwords = lines.ToHashSet();
        toLowercase = true;
        removeAccentsBeforeStemming = false;
        removeAccentsAfterStemming = false;
    }

    //zakladni metoda pro zahajeni predzpracovani
    public List<string> Preprocess(List<string> docs)
    {
        List<string> preprocessedDocs = new List<string>();
        foreach (string doc in docs)
        {
            string preprocessed = basicPreprocessing.BasicPreprocessing(doc);
            preprocessedDocs.Add(preprocessed);
        }
        return preprocessedDocs;
    }

    //podobna jako predchozi metoda, prizpusobena pro predzpracovani dotazu - neuklada data
    public string PreprocessQuerry(string v)
    {
        return basicPreprocessing.BasicPreprocessing(v);
    }

    //metoda pro indexaci dat
    public void index(string document)
    {
        document = document.ToLower();
        //odebrani \n
        document = document.Replace("\n", "");
        //odebrat stopwords
        foreach (string stop in stopwords)
        {
            string regex = "((^[ ]?)" + stop.Trim() + " )|( " + stop.Trim() + " )";
            document = document.Replace(regex, " ");
        }
        document = removeAccents(document);

        //zavedeni tokenu do pameti
        string tmpToken;
        foreach (string token in tokenizer.tokenize(document))
        {
            tmpToken = stemmer.stem(token);
            tmpToken = removeAccents(tmpToken);

            if (!wordFrequencies.ContainsKey(token))
            {
                wordFrequencies.Add(token, 0);
            }

            wordFrequencies[token]++;
        }
    }

    //metoda pro predzpracovani obdrzeneho textu
    public string getProcessedForm(string text)
    {
        if (toLowercase)
        {
            text = text.ToLower();
        }
        if (removeAccentsBeforeStemming)
        {
            text = removeAccents(text);
        }
        if (stemmer != null)
        {
            text = stemmer.stem(text);
        }
        if (removeAccentsAfterStemming)
        {
            text = removeAccents(text);
        }
        return text;
    }

    //text pro vyhozeni hacku carek apod.
    string withDiacritics = "áÁčćČďĎéÉěĚíÍňŇóÓřŘšŠťŤúÚůŮýÝžŽ";
    string withoutDiacritics = "aAccCdDeEeEiInNoOrRsStTuUuUyYzZ";

    //odebrani akcentu - pomoci textu vyse
    private string removeAccents(string text)
    {
        for (int i = 0; i < withDiacritics.Length; i++)
        {
            text = text.Replace("" + withDiacritics[i], "" + withoutDiacritics[i]);
        }
        return text;
    }

    //getter
    public Dictionary<string, int> getWordFrequencies()
    {
        return wordFrequencies;
    }

}

//trida co predzpracovani spousti - legacy nazev
public class BasicPreprocessingExample
{
    public AdvancedTokenizer tokenizer = new AdvancedTokenizer();
    public CzechStemmerLight stemmer = new CzechStemmerLight();

    //metoda pro spusteni procesu
    public string BasicPreprocessing(string doc)
    {

        string preprocessedDoc = "";
        string text = "";
        if (doc != null) text = doc;
        List<string> documents = new List<string>();
        documents = split(text, "\n").ToList();
        string fileName = @"stopwords.txt";
        string[] lines = File.ReadAllLines(fileName);
        HashSet<string> stopwords = lines.ToHashSet();
        preprocessedDoc = wordsStatistics(documents, tokenizer.defaultRegex, true, stopwords, false, true, true);

        return preprocessedDoc;
    }


    //metoda spoustici tokenizaci a predzpracovani - vraci vysledny predzpracovany string
    public string wordsStatistics(List<string> lines, string tokenizeRegex, bool stemm, HashSet<string> stopwords, bool removeAccentsBeforeStemming, bool removeAccentsAfterStemming, bool toLowercase)
    {
        HashSet<string> words = new HashSet<string>();
        long numberOfWords = 0;
        long numberOfChars = 0;
        long numberOfDocuments = 0;
        string? doc;
        string tkn = "";
        //postupne predzpracovani po radcich
        foreach (string line in lines)
        {
            if (line.Length < 2) continue;
            numberOfDocuments++;
            doc = line;
            //vse na lowercase
            if (toLowercase)
            {
                doc = line.ToLower();
            }
            //odebrani stopslov
            if (stopwords != null || stopwords.Count > 0)
            {
                foreach (var stopword in stopwords)
                {
                    if (stopword != null)
                    {
                        string lowered = stopword.ToLowerInvariant();
                        doc = doc.Replace(@" " + lowered + " ", " ");
                    }
                }
            }
            //odebrani akcentu
            if (removeAccentsBeforeStemming)
            {
                doc = tokenizer.removeAccents(line);
            }
            //tokenizace
            var tokens = tokenize(doc, tokenizeRegex);
            foreach (string token in tokens)
            {
                //kazdy token ostemovat
                if (stemm)
                {
                    tkn = stemmer.stem(token);
                }
                else tkn = token;
                //pripadne odebrani akcentu po stemmingu
                if (removeAccentsAfterStemming)
                {
                    tkn = tokenizer.removeAccents(tkn);
                }
                numberOfWords++;
                numberOfChars += tkn.Length;
                words.Add(tkn);
            }
        }
        //ulozeni vysledku
        string preprocessedWords = "";
        foreach (string o in words)
        {
            preprocessedWords += o;
            preprocessedWords += " ";
        }
        return preprocessedWords;
    }

    //metoda rozdelujici string pomoci regexu
    private string[] split(string line, string regex)
    {
        return line.Split(regex);
    }

    //metoda spoustejici tokenizaci
    private string[] tokenize(string line, string regex)
    {
        return tokenizer.tokenize(line, regex);
    }
}
