﻿
using static TfIdf;

public class SearchEngine
{
    public static string querry;

    //pokud je spusteno s parametry, bude se nacitat novy soubor, pokud ne, spusti se pouze vyhledavac
    public static Dictionary<string, InvertedIndex> invertedIndex = new Dictionary<string, InvertedIndex>();

    //spustenim programu je zahajeno nacteni dokumentu a jejich indexace
    public static void Program(string docsPath, Preprocessing preprocess, TfIdf tfidf)
    {
        List<string> docs = new List<string>();
        docs = ImportHandler.readNewDocuments(docsPath);
        docs = preprocess.Preprocess(docs);
        invertedIndex = tfidf.NewDocs(docs);
    }


}