﻿using System.Collections.Generic;
using System.Linq;

public class TfIdf
{
    public int numberOfReturnedDocuments;
    //invertovany seznam obsahujici term a id dokumentu s cetnosti vyskytu (byly problémy s ulozenim mapy do mapy, takze se jedna jen o obalovy prvek)
    public struct InvertedIndex
    {
        //key = docId   value = #hits
        public Dictionary<int, int> docHits;
        public InvertedIndex(int id)
        {
            docHits = new Dictionary<int, int>();
            docHits[id] = 1;
        }
    }
    //id dokumentu je index v listu, trida drzi pojem a jeho vahu v tomto dokumentu - jedna se o vektor dokumentu, ktery nedrzi zadne nulove slozky
    public class docVec
    {
        public List<double> weight;
        public List<string> term;
        public docVec(int termCount)
        {
            term = new List<string>();
            weight = new List<double>();
        }
        public KeyValuePair<string, double> GetPair(int index)
        {
            string key = term[index];
            double value = weight[index];
            KeyValuePair<string, double> pair = new KeyValuePair<string, double>(key, value);
            return pair;
        }
        public void Grow(docVec v)
        {
            KeyValuePair<string, double> pair;
            for (int i = 0; i < v.weight.Count; i++)
            {
                pair = v.GetPair(i);
                this.term.Add(pair.Key);
                this.weight.Add(pair.Value);
            }
        }
    }
    //seznam obsahujici dokumenty (lze vycistit pred generovanim vektoru, ale je zde, abych mohl do vysledku vypsat obsah nejlepsiho dokumentu)
    public List<string> documents;
    //invertovany seznam - klic=term, value = dvojice int (id dokumentu | pocet vyskytu v dokumentu)
    public Dictionary<string, InvertedIndex> fixedInvertedIndex;
    //list vah - kazdy item je pro dokument a predstavuje seznam vah a indexy na termy k vaham
    public List<docVec> documentVectors;
    //seznam idfs namapovanych na sve termy
    public Dictionary<string, double> idfs;
    //vysledky hledani - nejlepsi cosinus similarity
    public double[] maxSim;
    //pocet dokumentu
    public int docLen;
    //pocet dokumentu, ktere odpovidaji querry
    public int numberOfFoundDocuments;
    //dokumenty odpovidajici vyhledavani
    public int[] winnerDocumentIndex;



    public Preprocessing preprocessing;

    //trida spoustejici postupne vsechny ukony pro nahrani a zaindexovani dokumentu
    public Dictionary<string, InvertedIndex> NewDocs(List<string> docs)
    {
        if (docs.Count == 0) return new Dictionary<string, InvertedIndex>();
        documents = docs;
        //zpracovat dokumenty, vytvorit invertovany seznam ktery si nese info o tom kolik vyskytu v jakem dokumentu
        GenerateInvertedIndex();
        docLen = documents.Count;
        //tady lze vycistit seznam dokumentu pokud by byl problem s pameti
        documents.Clear();

        //generovat vectory dokumentu
        GenerateDocumentVectors(docLen, fixedInvertedIndex.Count);

        return fixedInvertedIndex;
    }

    public TfIdf(Preprocessing prep)
    {
        documents = new List<string>();
        fixedInvertedIndex = new Dictionary<string, InvertedIndex>();
        documentVectors = new List<docVec>();
        idfs = new Dictionary<string, double>();
        preprocessing = prep;
    }
    public string EnterQuerry(string querry, int numberOfWantedDocuments)
    {
        numberOfReturnedDocuments = numberOfWantedDocuments;
        maxSim = new double[numberOfReturnedDocuments];
        //vypocitat vector pro querry

        docVec querryVec = QuerryVector(preprocessing.PreprocessQuerry(querry));

        //spocitat cosine similarity mezi vektorem querry a vektory dokumentu
        winnerDocumentIndex = AllCosineSim(querryVec, docLen);

        //ziskani statistik


        //vypis (s osetrenim, kdyby se nic nenaslo apod.)
        string ret = "";
        ret += "Results:\n\n";
        for (int i = 0; i < numberOfReturnedDocuments; i++)
        {
            if (maxSim[i] > 0)
            {
                ret += $"{i + 1}. best document with cosine similarity ranking {maxSim[i]} has index {winnerDocumentIndex[i]}\n";
            }
            else winnerDocumentIndex[i] = -1;
        }
        ret += "\n";
        try
        {
            if (maxSim[0] > 0)
            {
                ret += $"Document with greatest ranking has index {winnerDocumentIndex[0]} with cosine similarity {maxSim[0]}";
                ret += $"\n\n For querry \"{querry}\" there are {numberOfFoundDocuments} dokuments.";
            }
            else
            {
                ret += "There are no finds of any part of querry... \n Try different document collection or querry.";
            }
        }
        catch (Exception ex)
        {
            ret += "There are no finds of any part of querry, or something went horribly wrong.";
        }
        return ret;
    }

    //pocitani vsech kosinovych podobnosti s tim, ze uvnitr se volaji jednotlive vypocty 'CosineSimilarity' a radi se podle uspesnosti do vysledku
    public int[] AllCosineSim(docVec querryVec, int docLen)
    {
        numberOfFoundDocuments = 0;
        double res = 0;
        double[] results = new double[docLen];
        int[] maxIndexes = new int[numberOfReturnedDocuments];
        for (int i = 0; i < numberOfReturnedDocuments; i++) { maxIndexes[i] = -1; }
        for (int i = 0; i < docLen; i++)
        {
            //pro kazdy dokument kosinovou vzdalenost od querry
            docVec dv = documentVectors[i];
            res = CosineSimilarity(querryVec, dv);
            if (res > 0)
            {
                numberOfFoundDocuments++;
            }
            results[i] = res;
        }

        //ziskani nejlepsich
        double max = 0;
        int index = -1;
        for (int i = 0; i < numberOfReturnedDocuments; i++)
        {
            max = 0;
            for (int j = 0; j < docLen; j++)
            {
                if (!maxIndexes.Contains(j) && max < results[j])
                {
                    max = results[j];
                    index = j;
                }
            }
            maxIndexes[i] = index;
            maxSim[i] = max;
        }
        return maxIndexes;
    }

    //vypocet kosinove vzdalenosti mezi dvema zadanymi vektory
    private double CosineSimilarity(docVec querryVec, docVec dv)
    {
        //kazdy s kazdym termem, pokud jsou stejne se vynasobi a prictou do sumy
        double scalarOfVectors = 0;
        double normalQ = 0;
        double normalD = 0;
        //pocitani citatele - skalarni soucin techto vectoru
        int i = 0, j = 0;
        foreach (string querryTerm in querryVec.term)
        {
            j = 0;
            foreach (string docTerm in dv.term)
            {
                if (querryTerm.Equals(docTerm))
                {
                    int indexQ = i;
                    int indexD = j;
                    scalarOfVectors += querryVec.weight[indexQ] * dv.weight[indexD];
                    break;
                }
                j++;
            }
            i++;
        }

        //pocitani jmenovatele - normala vektoru
        foreach (double weight in querryVec.weight)
        {
            normalQ += weight * weight;
        }
        normalQ = Math.Sqrt(normalQ);
        //to same pro vec dokumentu
        foreach (double weight in dv.weight)
        {
            normalD += weight * weight;
        }
        normalD = Math.Sqrt(normalD);

        return scalarOfVectors / (normalQ * normalD);
    }

    //ziskani vektoru z dotazu
    public docVec QuerryVector(string querry)
    {
        //querry rozdelim podle slov
        string[] querrySplit = querry.Split(' ');
        Dictionary<string, int> querryDict = new Dictionary<string, int>();
        //neresim bigramy apod. -> seradim querry kvuli duplicitam
        Array.Sort(querrySplit);
        int counter = 1;
        //udelam si dvojice term-pocet_vyskytu
        for (int i = 0; i < querrySplit.Length - 1; i++)
        {
            if (querrySplit[i] != querrySplit[i + 1])
            {
                querryDict.Add(querrySplit[i], counter);
                counter = 1;
            }
            else
            {
                counter++;
            }
        }
        querryDict.Add(querrySplit[querrySplit.Length - 1], counter);
        docVec querryDocVec = new docVec(querryDict.Count);
        //pro kazdy term
        foreach (string querryPart in querryDict.Keys)
        {
            //najdu-li slovo, ktere mam v querry, ziskam jeho idf
            double idf;
            try { idf = idfs[querryPart]; }
            catch (Exception e)
            {
                //nenasel jsem dotazovane slovo ve svych dokumentech - nema vahu
                idf = 0;
            }
            int tf = querryDict[querryPart];
            double weight = (1 + Math.Log10(tf)) * idf;
            querryDocVec.weight.Add(weight);
            querryDocVec.term.Add(querryPart);
        }
        //mam vector querry ulozeny v querryDocVec
        return querryDocVec;
    }

    //ziskani vektoru vsech dokumentu - prochazeni po termech - pri pruchodu termem se zapisuji vahy do ruznych vektoru (postupne se skladaji vsechny najednou)
    //tato metoda jen obaluje volani dvou jinych metod - vypocet idf a nasledne vypocet tf-idf
    private void GenerateDocumentVectors(int docLen, int termCount)
    {
        //potrebuji vzit vsechny prvky invertovaneho seznamu a pro kazdy term vzit jeho dokumenty,
        //vypocitat tuto jednu slozku vectoru a ulozit ji do documentVectors

        //potrebuju idf - numberOfDocs/documentFeq
        CountIdfs(docLen);

        //zalozit seznamy
        for (int i = 0; i < docLen; i++)
        {
            documentVectors.Add(new docVec(termCount));
        }
        //spocitani vah
        CountWeights();
    }

    //pocitani vah (tf-idf) pro vsechny dokumenty (s jiz znamymi idf)
    //ukladani do seznamu - jen nenulove hodnoty spojene s termem
    private void CountWeights()
    {
        //weight (jedno cislo - slozka vectoru) = (1+LOG10(tf))*idf
        //tf je ve fixedII, jedna o docHits.value
        //idf najdu podle klice fixedII.term
        //a pujde to do vectoru dokumentu fixedII.key
        //docVec potrebuje termId a to bude 'i' pres ktere budu iterovat termy fixedInvertedIndex

        //for(int i = 0; i < fixedInvertedIndex.Count; i++)//kazdy term
        foreach (string indexName in fixedInvertedIndex.Keys)
        {
            foreach (int docId in fixedInvertedIndex[indexName].docHits.Keys)//pres zaznamy dokumentId u termu
            {
                int tf = fixedInvertedIndex[indexName].docHits[docId];       //get tf v tomto dokumentu tohoto termu
                double idf = idfs[indexName];        //get idf termu
                double weight = (1 + Math.Log10((double)tf)) * idf;
                //pridani zaznamu do vektoru pro urcity dokument spolecne s termem ke kteremu se zaznam vztahuje
                documentVectors[docId].term.Add(indexName);
                documentVectors[docId].weight.Add(weight);
            }
        }
    }

    //vypocitani idf pro vsechny termy
    private void CountIdfs(int numberOfDocuments)
    {
        foreach (string indexName in fixedInvertedIndex.Keys)
        {
            int df = fixedInvertedIndex[indexName].docHits.Count;
            idfs.Add(indexName, Math.Log10((double)numberOfDocuments / (double)df));
        }
    }

    //generovani invertovanych indexu - klic = term, value = dvojice (id dokumentu | pocet vyskytu termu v dokumentu)
    private void GenerateInvertedIndex()
    {
        int id = 0;
        foreach (string document in documents)//pro kazdy dokument
        {
            List<string> terms = document.Split(null).ToList<string>();
            foreach (string term in terms)//pro kazdy term
            {
                if (fixedInvertedIndex.ContainsKey(term)) //pokud uz term 
                {
                    if (fixedInvertedIndex[term].docHits.ContainsKey(id))
                    {   //znam term, ale nevim jestli uz mam zaznam u tohoto dokumentu, pokud ano zvednu pocet vyskytu v dokumentu o 1
                        fixedInvertedIndex[term].docHits[id] = fixedInvertedIndex[term].docHits[id] + 1;
                    }
                    else
                    {   //pokud znam term, ale neznam dokument, nastavim novy dokument s vyskyty s 1. vyskytem
                        fixedInvertedIndex[term].docHits.Add(id, 1);
                    }
                }
                else
                {   //neznam term - musim zalozit (v konstruktoru se setne vyskyt i tohoto id na 1)
                    fixedInvertedIndex.Add(term, new InvertedIndex(id));
                }
            }
            id++;
        }
    }
}