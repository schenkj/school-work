# School Work

Nejedná se o všechny školní projekty, které jsem vypracoval. Většina školních projektů byla nahrána na školní server, odkud jsem tyto získal zpět, ale některé práce se mi nepodařilo takto dohledat. Pokusím se ještě nějaké projekty dohledat a doplnit.

Všechno co zde je jsou školní projekty. Vybíral jsem projekty, které jsou dostatečně zajímavé a nejedná se o domácí úkol na půl hodiny do jedné java třídy. Zároveň jsem ale nevyhazoval ani projekty, které bych dnes nepovažoval za reprezentativní, protože by to ubíralo smysl tomuto náhledu do toho, jak programuji.

Jsem si vědom, že mé schopnosti nejsou nijak obdivuhodné a spíše než finanční ohodnocení hledám místo, kde bych své schopnosti mohl zlepšit, a kde bych se mohl lépe naučit pracovat v týmu.

## Mezi zajímavé projekty patří:

school-work/2021_2022/Information retrieval (C# projekt na vytvoření offline vyhledávače)

school-work/2020_2021/Bakalářská práce (práce s body, java backend na k-means a shlukování prostorových bodů podle různých metrik, python zobrazení)

school-work/2019_2020/Programové struktury/PGS1_oprava/PGS1_oprava (čistá java práce s vlákny)

school-work/2020_2021/Úvod do počítačových sítí (client - server jednoduchá aplikace v javě)

school-work/2020_2021/Základy operačních systémů (java implementace souborového systému založeného na i-uzlech)